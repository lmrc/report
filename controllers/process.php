<?php

// define('WWW', $_SERVER['DOCUMENT_ROOT']);


// require_once(WWW.'/report/models/Fetch.class.php');
// require_once(WWW.'/report/models/Clean.class.php');
// require_once(WWW.'/report/models/Section.class.php');
// require_once(WWW.'/report/models/SetInfo.class.php');
// require_once(WWW.'/report/models/GetInfo.class.php');
// require_once(WWW.'/report/models/Report.class.php');

require_once('../models/Fetch.class.php');
require_once('../models/Clean.class.php');
require_once('../models/SetInfo.class.php');
require_once('../models/Section.class.php');
require_once('../models/GetInfo.class.php');
require_once('../models/Report.class.php');

date_default_timezone_set('Asia/Shanghai'); 

file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 加载所需类文件成功',FILE_APPEND);	

//获取基本信息，并写入Session
if (!empty($_POST['reportId'])&&!empty($_POST['subType'])&&!empty($_POST['subName'])&&!empty($_POST['beginDate'])&&!empty($_POST['finishDate'])) {

	//手动生成报告
	$reportId=$_POST["reportId"];
	$subType=$_POST["subType"];
	$subName=$_POST["subName"];
	$subOffice=$_POST["subOffice"];
	$beginDate=$_POST["beginDate"];
	$finishDate=$_POST["finishDate"];

	session_start();
	$_SESSION['reportId']=$reportId;
	$_SESSION['subType']=$subType;
	$_SESSION['subName']=$subName;
	$_SESSION['subOffice']=$subOffice;
	$_SESSION['beginDate']=$beginDate;
	$_SESSION['finishDate']=$finishDate;

	$timeStamp=strtotime(date("Y-m-d H:i:s",time()));
	$_SESSION['reportId']=$timeStamp;

	$_SESSION['dbName']='report';
	// $_SESSION['collName']=$timeStamp;
	$_SESSION['collName']=$reportId;

	file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 获取报告基本信息成功',FILE_APPEND);
	echo $reportId;
	exit();
}

//对裁判文书进行预处理
if (!empty($_POST['pretreat'])) {

	$pretreat=$_POST["pretreat"];
	// $subType=$_POST["subType"];
	// $dataId=$_POST["dataId"];

	// session_start();
	// $_SESSION['collName']=$dataId;
	// $_SESSION['subType']=$subType;
	
	foreach ($pretreat as $key => $value) {
		if ($value=='fetch') {
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 搜集裁判文书成功',FILE_APPEND);	
			$myFetch = new Fetch();
			$case=$myFetch->fetchCase();
		}else if ($value=='clean') {
			$myClean = new Clean($case);
			$case=$myClean->cleanCase();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 清洗裁判文书成功',FILE_APPEND);	
		}else if ($value=='devide') {
			$mySection = new Section($case);
			$case=$mySection->setDevide();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 裁判文书分段成功',FILE_APPEND);	

		}else if ($value=='write') {
			$myFetch = new Fetch();
			$myFetch->writeCase($case);
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 写入数据库成功',FILE_APPEND);	
		}
	}
	echo 'done';
	exit();
}


//提取重要分析维度
if (!empty($_POST['setinfo'])&&!empty($_POST['subType'])&&!empty($_POST['dataId'])) {
	$setinfo=$_POST["setinfo"];
	$dataId=$_POST["dataId"];
	$subType=$_POST["subType"];
	
	session_start();
	$_SESSION['collName']=$dataId;
	$_SESSION['subType']=$subType;


	$myFetch = new Fetch();
	$case=$myFetch->getCase();

	$myInfo=new SetInfo($case);
	

	foreach ($setinfo as $key => $value) {
		if ($value=='setdate') {
			$myInfo->setDate();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置日期成功',FILE_APPEND);	
		}else if ($value=='setsite') {
			$myInfo->setSite();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置地域成功',FILE_APPEND);	
		}else if ($value=='setdoctype') {
			$myInfo->setDoctype();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置文书类型成功',FILE_APPEND);	
		}else if ($value=='setcasetype') {
			$myInfo->setCasetype();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置案件类型成功',FILE_APPEND);	
		}else if ($value=='setlevel') {
			$myInfo->setLevel();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置审级成功',FILE_APPEND);	
		}else if ($value=='setrank') {
			$myInfo->setRank();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置层级成功',FILE_APPEND);	
		}else if ($value=='setsecondtag') {
			$myInfo->setSecondtag();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置二级案由成功',FILE_APPEND);	
		}else if ($value=='setoutcome') {
			$myInfo->setOutcome();
			$myInfo->setWinner();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置裁判结果成功',FILE_APPEND);	
		}else if ($value=='setpartyandagent') {
			$myInfo->setPartyAndAgent();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置当事人和代理人成功',FILE_APPEND);	
		}else if ($value=='setjustice') {
			$myInfo->setJustice();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置审判人员成功',FILE_APPEND);	
		}else if ($value=='setstatus') {
			$myInfo->setStatus();
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 设置诉讼地位成功',FILE_APPEND);	
		}
	}
	$myFetch->updateCase($myInfo->case);
	file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 数据更新成功',FILE_APPEND);	
	echo 'done';
	exit();
}


//获取重要分析维度
if (!empty($_POST['getinfo'])&&!empty($_POST['subType'])&&!empty($_POST['dataId'])) {

	$getinfo=$_POST["getinfo"];
	$dataId=$_POST["dataId"];
	$subType=$_POST["subType"];

	session_start();
	$dbName=$_SESSION['dbName'];
	$_SESSION['collName']=$dataId;
	$collName=$_SESSION['collName'];
	$_SESSION['subType']=$subType;

	$myInfo=new GetInfo($dbName,$collname);
	
	foreach ($getinfo as $key => $value) {
		if ($value=='getmonth') {
			$month=$myInfo->getMonth();
			// header('Location:../views/report_lawyer.html');

		}else if ($value=='getlevel') {
			$level=$myInfo->getLevel();
			// header('Location:../views/report_lawfirm.html');
		
		}else if ($value=='getdoctype') {
			$doctype=$myInfo->getDoctype();
			// header('Location:../views/report_company.html');
		
		}else if ($value=='getcasetype') {
			$casetype=$myInfo->getCasetype();
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getsecondtag') {
			$secondtag=$myInfo->getSecondtag();
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getparty') {
			$party=$myInfo->getParty();
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getlawyer') {
			$lawyer=$myInfo->getLawyer();
			// print_r($lawyer[0]);
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getlawfirm') {
			$lawfirm=$myInfo->getLawfirm();
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getjudge') {
			$judge=$myInfo->getJudge();
			// header('Location:../views/report_case.html');
		
		}else if ($value=='getcourt') {
			$court=$myInfo->getCourt();
			// header('Location:../views/report_case.html');
		
		}
		
	}
	exit();
}

if (!empty($_POST['getreport'])&&!empty($_POST['subType'])&&!empty($_POST['dataId'])) {
	$dataId=$_POST["dataId"];
	file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 数据库名称：'.$dataId,FILE_APPEND);

	$subType=$_POST["subType"];
	file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).' 报告类型：'.$subType,FILE_APPEND);

	@session_start();
	$_SESSION['collName']=$dataId;

	$myFetch = new Fetch();
	$case=$myFetch->getCase();
	$myReport = new Report($case);
	$reportId=$_SESSION['reportId'];
	$_SESSION['subType']=$subType;

	switch ($subType) {
		case 'lawyer':
			$url='../views/report_lawyer.html?id='.$reportId;
			echo $url;
			$myReport->lawyerReport();
			break;
		case 'company':
			$url='../views/report_company.html?id='.$reportId;
			echo $url;
			$myReport->companyReport();
			break;
		default:
			# code...
			break;
	}
	exit();
}