<?php
// define('WWW', $_SERVER['DOCUMENT_ROOT']);
// require_once(WWW.'/report/models/Download.class.php');
// require_once(WWW.'/report/models/Clean.class.php');
// require_once(WWW.'/report/models/Section.class.php');
// require_once(WWW.'/report/models/SetInfo.class.php');
// require_once(WWW.'/report/models/GetInfo.class.php');
// require_once(WWW.'/report/models/Report.class.php');

require_once('../models/Download.class.php');
require_once('../models/Clean.class.php');
require_once('../models/Section.class.php');
require_once('../models/Setinfo.class.php');
require_once('../models/GetInfo.class.php');
require_once('../models/Report.class.php');

if (!empty($_GET['subType'])&&!empty($_GET['subName'])&&!empty($_GET['beginDate'])&&!empty($_GET['finishDate'])&&!empty($_GET['reportId'])) {

	//系统自动生成报告
	$reportId=$_GET["reportId"];
	$subType=$_GET["subType"];
	$subName=$_GET["subName"];
	$subOffice=$_GET["subOffice"];
	$beginDate=urldecode($_GET["beginDate"]);
	$finishDate=urldecode($_GET["finishDate"]);

	session_start();
	$_SESSION['reportId']=$reportId;
	$_SESSION['subType']=$subType;
	$_SESSION['subName']=$subName;
	$_SESSION['subOffice']=$subOffice;
	$_SESSION['beginDate']=$beginDate;
	$_SESSION['finishDate']=$finishDate;

	// date_default_timezone_set('Asia/Shanghai'); 
	// $timeStamp=strtotime(date("Y-m-d H:i:s",time()));
	// $_SESSION['reportId']=$timeStamp;

	$myDownload=new Download();
	$case=$myDownload->getCase();

	$myClean = new Clean($case);
	$case=$myClean->cleanCase();

	$mySection = new Section($case);
	$case=$mySection->setDevide();

	$mySetinfo=new Setinfo($case);
	$mySetinfo->setDate();
	$mySetinfo->setSite();
	$mySetinfo->setDoctype();
	$mySetinfo->setCasetype();
	$mySetinfo->setLevel();
	$mySetinfo->setRank();
	$mySetinfo->setSecondtag();
	$mySetinfo->setOutcome();
	$mySetinfo->setWinner();
	$mySetinfo->setPartyAndAgent();
	$mySetinfo->setJustice();
	$case=$mySetinfo->case;
	
	$myReport = new Report($case);

	switch ($subType) {
		case 'lawyer':
			$myReport->lawyerReport();
			header('Location:../views/report_lawyer.html?id='.$reportId);
			exit();
		case 'company':
			$myReport->companyReport();
			header('Location:../views/report_company.html?id='.$reportId);
			exit();
		default:
			header('Location:../index.php');
			exit();
	}
	
}else{
	header('Location:../index.php');
	exit();
}

