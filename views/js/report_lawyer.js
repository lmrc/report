// 基于准备好的dom，初始化echarts实例
var consumer1 = echarts.init(document.getElementById('consumer1'));
// var consumer2 = echarts.init(document.getElementById('consumer2'));
var opposite1 = echarts.init(document.getElementById('opposite-party1'));
// var opposite2 = echarts.init(document.getElementById('opposite-party2'));
var cause1 = echarts.init(document.getElementById('cause1'));
var cause2 = echarts.init(document.getElementById('cause2'));
var cause3 = echarts.init(document.getElementById('cause3'));
var cause4 = echarts.init(document.getElementById('cause4'));
var cause5 = echarts.init(document.getElementById('cause5'));
var court1 = echarts.init(document.getElementById('court1'));
var level1 = echarts.init(document.getElementById('level1'));
var status1 = echarts.init(document.getElementById('status1'));
var status2 = echarts.init(document.getElementById('status2'));
var status3 = echarts.init(document.getElementById('status3'));
var status4 = echarts.init(document.getElementById('status4'));
var result1 = echarts.init(document.getElementById('result1'));
var result2 = echarts.init(document.getElementById('result2'));
var judgement1 = echarts.init(document.getElementById('judgement1'));
var judgement2 = echarts.init(document.getElementById('judgement2'));
var judgement3 = echarts.init(document.getElementById('judgement3'));
var judgement4 = echarts.init(document.getElementById('judgement4'));
var judgement5 = echarts.init(document.getElementById('judgement5'));
var judgement6 = echarts.init(document.getElementById('judgement6'));


// 指定图表的配置项和数据
// 指定图表的配置项和数据
var consumer_1 = {
    title : {
        text: '客户类型',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '客户类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
// var consumer_2 = {
//     title : {
//         text: '',
//         subtext: '',
//         x:'center'
//     },
//     color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
//     tooltip : {
//         trigger: 'item',
//         formatter: "{a} <br/>{b} : {c} ({d}%)"
//     },
//     // legend: {
//     //     orient: 'vertical',
//     //     left: 'left',
//     //     data: ['个人','机构']
//     // },
//     series : [
//         {
//             name: '机构客户类型',
//             type: 'pie',
//             radius : '55%',
//             center: ['50%', '60%'],
//             data:[
//                 {value:5, name:'个人'},
//                 {value:19, name:'机构'}
//             ],
//             itemStyle: {
//                 emphasis: {
//                     shadowBlur: 10,
//                     shadowOffsetX: 0,
//                     shadowColor: 'rgba(0, 0, 0, 0.5)'
//                 }
//             },
//             label:{
//                 normal:{
//                     formatter: "{b}：{c} ({d}%)"
//                 }
//             }
//         }
//     ]
// };
var opposite_1 = {
    title : {
        text: '对方当事人类型',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '对方当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
// var opposite_2 = {
//     title : {
//         text: '',
//         subtext: '',
//         x:'center'
//     },
//     color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
//     tooltip : {
//         trigger: 'item',
//         formatter: "{a} <br/>{b} : {c} ({d}%)"
//     },
//     // legend: {
//     //     orient: 'vertical',
//     //     left: 'left',
//     //     data: ['个人','机构']
//     // },
//     series : [
//         {
//             name: '对方机构当事人类型',
//             type: 'pie',
//             radius : '55%',
//             center: ['50%', '60%'],
//             data:[
//                 {value:5, name:'个人'},
//                 {value:19, name:'机构'}
//             ],
//             itemStyle: {
//                 emphasis: {
//                     shadowBlur: 10,
//                     shadowOffsetX: 0,
//                     shadowColor: 'rgba(0, 0, 0, 0.5)'
//                 }
//             },
//             label:{
//                 normal:{
//                     formatter: "{b}：{c} ({d}%)"
//                 }
//             }
//         }
//     ]
// };
var cause_1 = {
    title : {
        text: '总体概览',
        //textStyle:{
            //fontSize:18,
            //color:'#666',s
            //fontFamily:'微软雅黑'
        //},
        padding:0,
        itemGap:1,
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var cause_2 = {
    title : {
        text: '民事类案件',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var cause_3 = {
    title : {
        text: '刑事类案件',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var cause_4 = {
    title : {
        text: '行政类案件',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var cause_5 = {
    title : {
        text: '执行类案件',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var court_1 = {
    title : {
        text: '地域分布',
        // subtext: '纯属虚构',
        left: 'center'
    },

    tooltip : {
        trigger: 'item'
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data:['iphone3','iphone4','iphone5']
    // },
    // visualMap: {
    //     min: 0,
    //     max: 2500,
    //     left: 'left',
    //     top: 'bottom',
    //     text:['高','低'],           // 文本，默认为数值文本
    //     calculable : true
    // },
    // toolbox: {
    //     show: true,
    //     orient : 'vertical',
    //     left: 'right',
    //     top: 'center',
    //     feature : {
    //         mark : {show: true},
    //         dataView : {show: true, readOnly: false},
    //         restore : {show: true},
    //         saveAsImage : {show: true}
    //     }
    // },
    series : [
        {
            name: '案件地域分布',
            type: 'map',
            // mapType: 'china',
            map: 'china',
            roam: false,
            selectedMode : 'multiple',
            itemStyle:{
                normal:{
                    label:{show:false},
                    areaColor:'#d7ffff',
                    borderColor:'#b0f8f8',
                    borderWidth:1
                    // areaColor:'#1e2f4c'
                },
                emphasis:{
                    label:{show:true},
                    areaColor:'#07d1d1'
                }
            },

            // label:{
            //     normal:{
            //         formatter: "{b}：{c} ({d}%)"
            //     }
            // },
            data:[
                {name:'广东', selected:true}
            ]
            // markPoint : {
            //     symbol: 'circle',
            //     data : [
            //         {name : '打酱油的标注', value : 100, x:'5%', y:'50%', symbolSize:32},
            //         {name : '打酱油的标注', value : 100, x:'95%', y:'50%', symbolSize:32}
            //     ]
            // }
        }
    ]
};

var level_1 = {
    title : {
        text: '层级分布',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院层级分布',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var status_1 = {
    title : {
        text: '总体概览',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '客户诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var status_2 = {
    title : {
        text: '攻方',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方客户诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var status_3 = {
    title : {
        text: '第三方',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '第三方客户诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var status_4 = {
    title : {
        text: '守方',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方客户诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var result_1 = {
    title : {
        text: '裁判文书类型',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁判文书类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var result_2 = {
    title : {
        text: '裁定案件裁判结果',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁定案件裁判结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_1 = {
    title : {
        text: '判决案件裁判结果',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '判决案件裁判结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_2 = {
    title : {
        text: '判决案件审理程序',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '判决案件审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_3 = {
    title : {
        text: '代表攻方判决结果',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代表攻方判决结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_4 = {
    title : {
        text: '代理攻方审理程序',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理攻方审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_5 = {
    title : {
        text: '代理守方判决结果',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理守方判决结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
var judgement_6 = {
    title : {
        text: '代理守方审理程序',
        subtext: '',
        x:'center'
    },
    color:['#07d1d1','#50dcdc','#62e9e9','#73f3f3','#93f6f6','#b0f8f8','#c4fbfb'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理守方审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};  

// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);

var NUM=11;
var now=0;
$(function(){
    $('#lawyer>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#lawyer>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#lawyer>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#lawyer>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#lawyer>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});


// $(function(){
//    $('#multicompany>div').each(function(j){
//            $("#page"+j).hide();
//        });
//    $("#page0").show();

//    $("#button_firstpage").click(function(){
//        $('#multicompany>div').each(function(j){
//            $("#page"+j).hide();
//        });
//        $("#page0").show();
//        now=0;
//    });

//    $("#button_prevpage").click(function(){
//        if (now!==0) {
//            $('#multicompany>div').each(function(j){
//                $("#page"+j).hide();
//            });
//            $("#page"+now+" .p").html(now);
//            $("#page"+(--now)).show();
//        }
//    });

//    $("#button_nextpage").click(function(){
//        if (now!==NUM) {
//            $('#multicompany>div').each(function(j){
//                $("#page"+j).hide();
//            });
//            $("#page"+now+" .p").html(now);
//            $("#page"+(++now)).show();
//        }
//    });
   
//    $("#button_lastpage").click(function(){
//        $('#multicompany>div').each(function(j){
//            $("#page"+j).hide();
//        });
//        $("#page"+NUM+" .p").html(now);
//        $("#page"+NUM).show();
//        now=NUM;
//    });
// });
