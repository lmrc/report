// 基于准备好的dom，初始化echarts实例


var lawyersite = echarts.init(document.getElementById('lawyersite'));
var clienttype1 = echarts.init(document.getElementById('clienttype1'));
var clienttype2 = echarts.init(document.getElementById('clienttype2'));
var clientnum1 = echarts.init(document.getElementById('clientnum1'));
var clientnum2 = echarts.init(document.getElementById('clientnum2'));
var clientnum3 = echarts.init(document.getElementById('clientnum3'));
var oppositetype1 = echarts.init(document.getElementById('oppositetype1'));
var oppositetype2 = echarts.init(document.getElementById('oppositetype2'));
var oppositelawfirm1 = echarts.init(document.getElementById('oppositelawfirm1'));
var oppositelawfirm2 = echarts.init(document.getElementById('oppositelawfirm2'));
var oppositelawfirm3 = echarts.init(document.getElementById('oppositelawfirm3'));
var casetype = echarts.init(document.getElementById('casetype'));
var casetype1 = echarts.init(document.getElementById('casetype1'));
var casetype2 = echarts.init(document.getElementById('casetype2'));
var casetype3 = echarts.init(document.getElementById('casetype3'));
var casetype4 = echarts.init(document.getElementById('casetype4'));
var casetype5 = echarts.init(document.getElementById('casetype5'));
var rank = echarts.init(document.getElementById('rank'));
var level = echarts.init(document.getElementById('level'));
var casesite = echarts.init(document.getElementById('casesite'));
var status1 = echarts.init(document.getElementById('status1'));
var status2 = echarts.init(document.getElementById('status2'));
var status3 = echarts.init(document.getElementById('status3'));
var doctype1 = echarts.init(document.getElementById('doctype1'));
var doctype2 = echarts.init(document.getElementById('doctype2'));
var doctype3 = echarts.init(document.getElementById('doctype3'));
var gfpanjue1 = echarts.init(document.getElementById('gfpanjue1'));
var gfpanjue2 = echarts.init(document.getElementById('gfpanjue2'));
var gfpanjue3 = echarts.init(document.getElementById('gfpanjue3'));
var sfpanjue1 = echarts.init(document.getElementById('sfpanjue1'));
var sfpanjue2 = echarts.init(document.getElementById('sfpanjue2'));
var sfpanjue3 = echarts.init(document.getElementById('sfpanjue3'));
var lawyernum1 = echarts.init(document.getElementById('lawyernum1'));
var lawyernum2 = echarts.init(document.getElementById('lawyernum2'));
var lawyernum3 = echarts.init(document.getElementById('lawyernum3'));







// 指定图表的配置项和数据
// 指定图表的配置项和数据
var lawyersite_1 = {
   title : {
        text: '地域分布',
        left: 'center',
        textStyle:{color:'#646464'},
    },
    
    tooltip : {
        trigger: 'item'
    },
    series : [
        {
            name: '案件地域分布',
            type: 'map',
            // mapType: 'china',
            map: 'china',
            roam: false,
            selectedMode : 'multiple',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{
                    label:{show:true}

                },
            data:[
                {name:'北京', selected:true,value:62},
                {name:'广东', selected:true,value:52},
                {name:'上海', selected:true,value:35}
            ]
        }
    }
]
};

var clienttype1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
            {
                name: '客户类型',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:0, name:'个人'},
                    
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c} ({d}%)"
                    }
                }
            }
        ]
    };

var clienttype2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
            {
                name: '机构客户类型',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'企业'},
                    {value:19, name:'非企业'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c} ({d}%)"
                    }
                }
            }
        ]
    };

var clientnum1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} "
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
        series : [
            {
                name: '代理客户',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'个人'}
                    
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c} "
                    }
                }
            }
        ]
    };
var clientnum2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#0180ab'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} "
    },
// legend: {
//     orient: 'vertical',
//     left: 'left',
//     data: ['个人','机构']
// },
    series : [
        {
            name: '代理客户',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'}
                
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} "
                }
            }
        }
    ]
};

var clientnum3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} "
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理客户',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'}
                
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}"
                }
            }
        }
    ]
};

var oppositetype1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
            {
                name: '对方当事人类型',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'个人'},
                    {value:19, name:'机构'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c} ({d}%)"
                    }
                }
            }
        ]
    };

var oppositetype2_1 = {
    title : {
        text: '机构',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
            {
                name: '对方机构当事人类型',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'企业'},
                    {value:19, name:'非企业'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c}({d}%)"
                    }
                }
            }
        ]
    };

var oppositelawfirm1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
        series : [
            {
                name: '律所名称',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'个人'}
                    
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：\n{c}"
                    }
                }
            }
        ]
    };
var oppositelawfirm2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#0180ab'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} "
    },
// legend: {
//     orient: 'vertical',
//     left: 'left',
//     data: ['个人','机构']
// },
    series : [
        {
            name: '律所名称',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'}
                
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} "
                }
            }
        }
    ]
};

var oppositelawfirm3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律所名称',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'}
                
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} "
                }
            }
        }
    ]
};

var casetype_1 = {
    title : {
        text: '案由整体分布',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#208E4E','#8DC553','#afd6dd'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casetype1_1 = {
    title : {
        text: '民事类案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '民事类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casetype2_1 = {
    title : {
        text: '知识产权类案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '知识产权类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casetype3_1 = {
    title : {
        text: '行政类案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#7EC590','#66BB74','#59B75B','#3AB034','#116F38','#B4DABB','#9FD2AF'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '行政类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casetype4_1 = {
    title : {
        text: '执行类案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#6AB0D5','#2788C0','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '执行类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casetype5_1 = {
    title : {
        text: '刑事类案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#8DC553','#7EC590','#66BB74','#59B75B','#3abbe5','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '刑事类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var rank_1 = {
    title : {
        text: '法院层级',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院层级',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var level_1 = {
    title : {
        text: '案件审级',
        subtext: '',
        x:'center',
        textStyle:{
        color:'#646464'
        },
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件审级',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casesite_1 = {
    title : {
        text: '地域分布',
        // subtext: '纯属虚构',
        left: 'center',
        textStyle:{
        color:'#646464'
        },
    },
    
    tooltip : {
        trigger: 'item'
    },

    series : [
        {
            name: '案件数量',
            type: 'map',
            // mapType: 'china',
            map: 'china',
            roam: false,
            selectedMode : 'multiple',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true}},

            },


            data:[
                {name:'广东', selected:true,value:55}
            ]
        }
    ]
};

var status1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#208E4E','#38B1D7'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '客户诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var status2_1 = {
    title : {
        text: '代理客户为攻方',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理客户为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};

var status3_1 = {
    title : {
        text: '代理客户为守方',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#208E4E','#3AB034','#8DC553','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '代理客户为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};

var doctype1_1 = {
    title : {
        text: '结案方式',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#192E4B','#208E4E'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '结案方式',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var doctype2_1 = {
    title : {
        text: '判决结案案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '判决结案案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c}({d}%)"
                }
            }
        }
    ]
};

var doctype3_1 = {
    title : {
        text: '裁定结案案件',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#2788C0','#187AA2','#5ABFBA','#36B0D6','#208E4E','#3AB034','#8DC553','#B0D9B8'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b}{c} "
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁定结案案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c}({d}%) "
                }
            }
        }
    ]
};

var gfpanjue1_1 = {
    title : {
        text: '代理攻方',
        subtext: '一审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#2788C0','#5CC0BA','#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '一审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
    
var gfpanjue2_1 = {
    title : {
        text: '代理攻方',
        subtext: '二审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#2788C0','#5CC0BA','#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '二审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

 var gfpanjue3_1 = {
    title : {
        text: '代理攻方',
        subtext: '再审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#2788C0','#5CC0BA','#1790cf'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '再审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var sfpanjue1_1 = {
    title : {
        text: '代理守方',
        subtext: '一审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#8DC553','#3AB034','#86DB81'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '一审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var sfpanjue2_1 = {
    title : {
        text: '代理守方',
        subtext: '二审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#8DC553','#3AB034','#86DB81'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '二审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var sfpanjue3_1 = {
    title : {
        text: '代理守方',
        subtext: '再审',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#8DC553','#3AB034','#86DB81'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '再审',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawyernum1_1 = {
    title : {
        text: '代理案件数量NO.1律师',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律师代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawyernum2_1 = {
    title : {
        text: '代理案件数量NO.2律师',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
         {
            name: '律师代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawyernum3_1 = {
    title : {
        text: '代理案件数量NO.3律师',
        subtext: '',
        x:'center',
        textStyle:{color:'#646464'},
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律师代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);

var NUM=19;
var now=0;
$(function(){
    $('#lawfirm>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#lawfirm>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#lawfirm>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#lawfirm>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#lawfirm>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});

