// 基于准备好的dom，初始化echarts实例
var rank = echarts.init(document.getElementById('rank'));
var site = echarts.init(document.getElementById('site'));
var month = echarts.init(document.getElementById('month'));
var gongtype1 = echarts.init(document.getElementById('gongtype1'));
var gongtype2 = echarts.init(document.getElementById('gongtype2'));
var shoutype1 = echarts.init(document.getElementById('shoutype1'));
var shoutype2 = echarts.init(document.getElementById('shoutype2'));

var gongtype3 = echarts.init(document.getElementById('gongtype3'));
var gongtype4 = echarts.init(document.getElementById('gongtype4'));
var shoutype3 = echarts.init(document.getElementById('shoutype3'));
var shoutype4 = echarts.init(document.getElementById('shoutype4'));


var casetype1 = echarts.init(document.getElementById('casetype1'));
var casetype2 = echarts.init(document.getElementById('casetype2'));
var judgeperiod1 = echarts.init(document.getElementById('judgeperiod1'));
var judgeperiod2 = echarts.init(document.getElementById('judgeperiod2'));
var level1 = echarts.init(document.getElementById('level1'));
var level2 = echarts.init(document.getElementById('level2'));
var level3 = echarts.init(document.getElementById('level3'));
var level4 = echarts.init(document.getElementById('level4'));
var level5 = echarts.init(document.getElementById('level5'));
var judgeresult1 = echarts.init(document.getElementById('judgeresult1'));
var judgeresult2 = echarts.init(document.getElementById('judgeresult2'));
var judgeresult3 = echarts.init(document.getElementById('judgeresult3'));
var secondtag1 = echarts.init(document.getElementById('secondtag1'));
var secondtag2 = echarts.init(document.getElementById('secondtag2'));
var secondtag3 = echarts.init(document.getElementById('secondtag3'));
var secondtag4 = echarts.init(document.getElementById('secondtag4'));
var secondtag5 = echarts.init(document.getElementById('secondtag5'));
var secondtag6 = echarts.init(document.getElementById('secondtag6'));
var casemoney = echarts.init(document.getElementById('casemoney'));
var money1 = echarts.init(document.getElementById('money1'));
var money2 = echarts.init(document.getElementById('money2'));
var money3 = echarts.init(document.getElementById('money3'));
var money4 = echarts.init(document.getElementById('money4'));
var money5 = echarts.init(document.getElementById('money5'));
var money6 = echarts.init(document.getElementById('money6'));
var money7 = echarts.init(document.getElementById('money7'));
var money8 = echarts.init(document.getElementById('money8'));
var lawfl1 = echarts.init(document.getElementById('lawfl1'));
var lawfl2 = echarts.init(document.getElementById('lawfl2'));
var lawfl3 = echarts.init(document.getElementById('lawfl3'));

var lawsfjs1 = echarts.init(document.getElementById('lawsfjs1'));
var lawsfjs2 = echarts.init(document.getElementById('lawsfjs2'));
var lawsfjs3 = echarts.init(document.getElementById('lawsfjs3'));

var lawxzfg1 = echarts.init(document.getElementById('lawxzfg1'));
var lawxzfg2 = echarts.init(document.getElementById('lawxzfg2'));
var lawxzfg3 = echarts.init(document.getElementById('lawxzfg3'));

// var result1 = echarts.init(document.getElementById('result1'));
// var result2 = echarts.init(document.getElementById('result2'));
// var judgement1 = echarts.init(document.getElementById('judgement1'));
// var judgement2 = echarts.init(document.getElementById('judgement2'));
// var judgement3 = echarts.init(document.getElementById('judgement3'));
// var judgement4 = echarts.init(document.getElementById('judgement4'));
// var judgement5 = echarts.init(document.getElementById('judgement5'));



// 指定图表的配置项和数据
// 指定图表的配置项和数据
var rank_1 = {
    title : {
        text: '层级分布',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '层级分布',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var site_1 = {
  title : {
        text: '',
        // subtext: '纯属虚构',
        left: 'center'
    },
    
    tooltip : {
        trigger: 'item'
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data:['iphone3','iphone4','iphone5']
    // },
    // visualMap: {
    //     min: 0,
    //     max: 2500,
    //     left: 'left',
    //     top: 'bottom',
    //     text:['高','低'],           // 文本，默认为数值文本
    //     calculable : true
    // },
    // toolbox: {
    //     show: true,
    //     orient : 'vertical',
    //     left: 'right',
    //     top: 'center',
    //     feature : {
    //         mark : {show: true},
    //         dataView : {show: true, readOnly: false},
    //         restore : {show: true},
    //         saveAsImage : {show: true}
    //     }
    // },
    series : [
        {
            name: '案件地域分布',
            type: 'map',
            // mapType: 'china',
            map: '北京',
            roam: false,
            selectedMode : 'multiple',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true}}
            },

            // label:{
            //     normal:{
            //         formatter: "{b}：{c} ({d}%)"
            //     }
            // },
            data:[
                {name:'东城区', selected:true,value:55}
            ]
            // markPoint : {
            //     symbol: 'circle',
            //     data : [
            //         {name : '打酱油的标注', value : 100, x:'5%', y:'50%', symbolSize:32},
            //         {name : '打酱油的标注', value : 100, x:'95%', y:'50%', symbolSize:32}
            //     ]
            // }
        }
    ]
};


var month_1 = {
    title: {
        text: '月度变化',
        x:'center'
    },
    color:'#3abbe5',
    tooltip: {
        trigger: 'axis'
    },
    xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: ['周一','周二','周三','周四','周五','周六','周日']
    },

    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value}'
        }
    },
    series: [
        {
            name:'案件数量',
            type:'line',
            data:[1, 2, 15, 13, 12, 13, 10],
        },
    ]
};


var gongtype1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var gongtype2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var shoutype1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var shoutype2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};



var gongtype3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var gongtype4_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var shoutype3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var shoutype4_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
    

var casetype1_1 = {
    title : {
        text: '案件类型',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
    
var casetype2_1 = {
    title : {
        text: '民事类案件',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '民事类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var judgeperiod1_1 = {
    title : {
        text: '可计算审理时间的案件',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var judgeperiod2_1 = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
  
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['周一','周二','周三','周四','周五','周六','周日']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
     series : [
        {
            name:'最大值',
            type:'bar',
            data:[320, 332, 301, 334, 390, 330, 320]
        },
        {
            name:'平均值',
            type:'bar',
            data:[120, 132, 101, 134, 90, 230, 210]
        },
        {
            name:'最小值',
            type:'bar',
            data:[62, 82, 91, 84, 109, 110, 120]
        }
    ]
};

var level1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var level3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level4_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level5_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var judgeresult1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var judgeresult2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var judgeresult3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag4_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag5_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var secondtag6_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var casemoney_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var money1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var money2_1 = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
   legend: {
        data:['诉请金额','判赔金额']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['最大值','平均值','中位数','最小值']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'诉请金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
         {
            name:'判赔金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
    ]
};

var money3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var money4_1 = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
   legend: {
        data:['诉请金额','判赔金额']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['最大值','平均值','中位数','最小值']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'诉请金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
         {
            name:'判赔金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
    ]
};

var money5_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var money6_1 = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
   legend: {
        data:['诉请金额','判赔金额']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['最大值','平均值','中位数','最小值']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'诉请金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
         {
            name:'判赔金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
    ]
};

var money7_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var money8_1 = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
   legend: {
        data:['诉请金额','判赔金额']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['最大值','平均值','中位数','最小值']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'诉请金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
         {
            name:'判赔金额',
            type:'bar',
            data:[320, 332, 301, 334]
        },
    ]
};


var lawfl1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center',
        // width:10,
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
   
var lawfl2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var lawfl3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawsfjs1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
   
var lawsfjs2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var lawsfjs3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var lawxzfg1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c} ({d}%)"
                }
            }
        }
    ]
};
   
var lawxzfg2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var lawxzfg3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '当事人类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);


var NUM=19;
var now=0;
$(function(){
    $('#case>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#case>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#case>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#case>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#case>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});
