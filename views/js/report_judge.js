// =================================================================================================================================基于准备好的dom，初始化echarts实例
var affiliatedjudge = echarts.init(document.getElementById('affiliatedjudge'));
var casetype = echarts.init(document.getElementById('casetype'));
var month = echarts.init(document.getElementById('month'));
var level = echarts.init(document.getElementById('level'));
var party1 = echarts.init(document.getElementById('party1'));
var party2 = echarts.init(document.getElementById('party2'));
var party3 = echarts.init(document.getElementById('party3'));
var party4 = echarts.init(document.getElementById('party4'));
var level1_result = echarts.init(document.getElementById('level1_result'));
var level2_result = echarts.init(document.getElementById('level2_result'));
var result_analysis1 = echarts.init(document.getElementById('result_analysis1'));
var result_analysis11 = echarts.init(document.getElementById('result_analysis11'));
var result_analysis12 = echarts.init(document.getElementById('result_analysis12'));
var result_analysis2 = echarts.init(document.getElementById('result_analysis2'));
var result_analysis21 = echarts.init(document.getElementById('result_analysis21'));
var result_analysis22 = echarts.init(document.getElementById('result_analysis22'));
var result_analysis3 = echarts.init(document.getElementById('result_analysis3'));
var result_analysis31 = echarts.init(document.getElementById('result_analysis31'));
var result_analysis32 = echarts.init(document.getElementById('result_analysis32'));
var result_analysis4 = echarts.init(document.getElementById('result_analysis4'));
var result_analysis41 = echarts.init(document.getElementById('result_analysis41'));
var result_analysis42 = echarts.init(document.getElementById('result_analysis42'));
var winrate1 = echarts.init(document.getElementById('winrate1'));
var winrate2 = echarts.init(document.getElementById('winrate2'));
var winrate3 = echarts.init(document.getElementById('winrate3'));
var winrate4 = echarts.init(document.getElementById('winrate4'));
var party_winrate1 = echarts.init(document.getElementById('party_winrate1'));
var party_winrate2 = echarts.init(document.getElementById('party_winrate2'));
var winrate11 = echarts.init(document.getElementById('winrate11'));
var winrate21 = echarts.init(document.getElementById('winrate21'));
var winrate31 = echarts.init(document.getElementById('winrate31'));
var winrate41 = echarts.init(document.getElementById('winrate41'));
var party_winrate11 = echarts.init(document.getElementById('party_winrate11'));
var party_winrate21 = echarts.init(document.getElementById('party_winrate21'));
var trialtime1 = echarts.init(document.getElementById('trialtime1'));
var trialtime2 = echarts.init(document.getElementById('trialtime2'));
var trialtime3 = echarts.init(document.getElementById('trialtime3'));
var trialtime4 = echarts.init(document.getElementById('trialtime4'));
var money = echarts.init(document.getElementById('money'));
var agent1 = echarts.init(document.getElementById('agent1'));
var agent2 = echarts.init(document.getElementById('agent2'));
var agent3 = echarts.init(document.getElementById('agent3'));
var agent4 = echarts.init(document.getElementById('agent4'));
var agent5 = echarts.init(document.getElementById('agent5'));
var agent6 = echarts.init(document.getElementById('agent6'));


var level1result1 = echarts.init(document.getElementById('level1result1'));
var level1result2 = echarts.init(document.getElementById('level1result2'));
var level2result3 = echarts.init(document.getElementById('level2result3'));
var level2result4 = echarts.init(document.getElementById('level2result4'));


// =======================================================================================================================================指定图表的配置项和数据

var affiliatedjudge_1 = {
    title: {
        text: '同案审理数量排名',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.1],
        show: false
    },
    yAxis: {
        type: 'category',
        boundaryGap: [0, 0.1],
        data: ['张晓津','杜长辉','姜颖','陈锦川','宿迟']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:30,
            data: [15, 17, 20, 23, 27],
            label: {
                normal: {
                    show: true,
                    position: 'insideRight',
                 }
            },
            itemStyle: {
                normal: {
                    color:'#F4B81C'
                }
            },
        },
    ]
};

var casetype_1 = {   
    color:['#249DCC','#F4B81C','#249DCC','#F6CE61','#F8DB8D'],
    // tooltip: {
    //     trigger: 'item',
    //     formatter: "{a} <br/>{b}: {c} ({d}%)"
    // },
    legend: {
        orient: 'vertical',
        x: 'center',
        data:['行政类案件','民事类案件']
    },
    series: [
        {
            name:'案件类型',
            type:'pie',
            selectedMode: 'single',
            radius: [0, '30%'],
            label: {
                normal: {
                    position: 'inner',
                    formatter: "{c}({d}%)"
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [
                {value:23, name:'行政类案件'},
                {value:2, name:'民事类案件'},
            ]
        },
        {
            name:'二级案由',
            type:'pie',
            radius: ['40%', '55%'],
            label: {
                normal: {
                    formatter: "{b}:{c} \n({d}%)"
                }
            },
            data:[
                {value:23, name:'行政确认'},
                {value:1, name:'知识产权纠纷'},
                {value:1, name:'竞争纠纷'},
               
            ]
        }
    ]
};


var month_1 = {
    title: {
        text: ''
    },
    tooltip: {
        trigger: 'axis'
    },
    // legend: {
    //     data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'], 
       
    },
    yAxis: {
        type: 'value',
        // lable:{
        //  // textStyle: {
        //  //                fontSize: 10,
        //  //                color: '#F4B81C'
        //  //            },       
        // }
       
    },
    series: [
        {
            name:'案件数量',
            type:'line',
            // color:'#F7931F',
            stack: '总量',
            data:[ 0,0,3,2,3,0,1,0,3,0,4 ],
            
            label: {
                normal: {
                show: true,
                position: 'top',
                textStyle: {
                        fontSize: 10,
                        color: 'black'
                    },    
                },
            },
            itemStyle:{
                normal:{
                    lineStyle:{
                        color:'#F4B81C',
                        type:'solid'
                    },   
                }
            }                 
        },
    ]    
};

var level_1 = {
    // title : {
    //     text: '原告',
    //     subtext: '',
    //     x:'center'
    // },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'一审'},
                {value:2, name:'二审'},
                {value:0, name:'再审'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};

var party1_1 = {
    title : {
        text: '原告',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'个人'},
                {value:2, name:'企业机构'},
                {value:0, name:'政府机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};


var party2_1 = {
    title : {
        text: '被告',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'个人'},
                {value:2, name:'企业机构'},
                {value:0, name:'政府机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};

var party3_1 = {
    title : {
        text: '上诉人',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'个人'},
                {value:2, name:'企业机构'},
                {value:0, name:'政府机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};

var party4_1 = {
    title : {
        text: '被上诉人',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '审理程序',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'个人'},
                {value:2, name:'企业机构'},
                {value:0, name:'政府机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};


var level1_result1 = {
    // title : {
    //     // text: '',
    //     // // subtext: '',
    //     // x:'center'
    // },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '结案方式',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'判决'},
                {value:2, name:'裁定'},
                {value:0, name:'其他'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};


var level2_result1 = {
    // title : {
    //     // text: '',
    //     // // subtext: '',
    //     // x:'center'
    // },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁判结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:14, name:'驳回'},
                {value:2, name:'支持'},
                {value:0, name:'其他'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};

var result_analysis1_1 = {
   // title : {
   //      text: '',
   //      subtext: '',
   //      x:'center'
   //  },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '胜败诉',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:10, name:'胜诉'},
                {value:4, name:'败诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result_analysis11_1 = {
    title : {
        text: '胜诉',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'个人'},
                {value:2, name:'企业'},
                {value:2, name:'政府'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var result_analysis12_1 = {
    title : {
        text: '败诉',
        subtext: '',
        subtextStyle:{
            fontSize: 18,
            fontWeight: 'bolder',
            color: '#333'
        },
        x:'center',
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'个人'},
                {value:1, name:'企业'},
                {value:2, name:'政府'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};
var result_analysis2_1 = {
   title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#F39800','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '胜败诉',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:10, name:'胜诉'},
                {value:4, name:'败诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result_analysis21_1 = {
    title : {
        text: '胜诉',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#187AA2','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'个人'},
                {value:2, name:'企业'},
                {value:2, name:'政府'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var result_analysis22_1 = {
    title : {
        text: '败诉',
        subtext: '',
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center',
    },
    color:['#F6AD3A','#F9C170','#FCD7A1','#F39800','#CC933B'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'个人'},
                {value:1, name:'企业'},
                {value:2, name:'政府'}

    
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};

var result_analysis3_1 = {
   title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#F39800','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '胜败诉',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:10, name:'胜诉'},
                {value:4, name:'败诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result_analysis31_1 = {
    title : {
        text: '胜诉',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#187AA2','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'个人'},
                {value:2, name:'企业'},
                {value:2, name:'政府'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var result_analysis32_1 = {
    title : {
        text: '败诉',
        subtext: '',
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center',
    },
    color:['#F6AD3A','#F9C170','#FCD7A1','#F39800','#CC933B'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'个人'},
                {value:1, name:'企业'},
                {value:2, name:'政府'}

    
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};
var result_analysis4_1 = {
   title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#F39800','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '胜败诉',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:10, name:'胜诉'},
                {value:4, name:'败诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result_analysis41_1 = {
    title : {
        text: '胜诉',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#187AA2','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'个人'},
                {value:2, name:'企业'},
                {value:2, name:'政府'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var result_analysis42_1 = {
    title : {
        text: '败诉',
        subtext: '',
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center',
    },
    color:['#F6AD3A','#F9C170','#FCD7A1','#F39800','#CC933B'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'个人'},
                {value:1, name:'企业'},
                {value:2, name:'政府'}

    
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};

var winrate1_1 = {
    title: {
        text: '原告胜诉率分析',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
   legend: {
        data: ["胜诉","败诉"],
        x: 'right',
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['个人','企业','政府','其他']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };



var winrate2_1 = {
    title: {
        text: '被告胜诉率分析',
        // subtext: '数据来自网络'
        x:'center'
    },
    legend: {
        data: ["胜诉","败诉"],
        x: 'right',

    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['个人','企业','政府','其他']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };
// ==========================================二审    

var winrate3_1 = {
    title: {
        text: '原告胜诉率分析',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
   legend: {
        data: ["胜诉","败诉"],
        x: 'right',
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['个人','企业','政府','其他']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };



var winrate4_1 = {
    title: {
        text: '被告胜诉率分析',
        // subtext: '数据来自网络'
        x:'center'
    },
    legend: {
        data: ["胜诉","败诉"],
        x: 'right',

    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['个人','企业','政府','其他']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };



var party_winrate1_1 = {
    title: {
        text: '原告',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
   legend: {
        data: ["胜诉","败诉"],
        x: 'right',
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['北京极科极客科技有限公司','北京盛雪城科贸有限公司','北京星纪开元科技发展有限公司']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };



var party_winrate2_1 = {
    title: {
        text: '被告',
        // subtext: '数据来自网络'
        x:'center'
    },
    legend: {
        data: ["胜诉","败诉"],
        x: 'right',

    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['国家工商行政总局','北京太合麦田音乐文化发展有限公司','北京爱奇艺科技有限公司']
    },
    series: [
            {
                name: '胜诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '败诉',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };


  //// =================================================================================================================================================================================================================================================柱状图加钥匙环  
var labelTop = {
    normal : {
        label : {
            show : false,
            position : 'center',
            formatter : '{b}',
           
        },
    }
};
var label1 = {
    normal: {
        color:'#0180AB'
        }
    };
var label2 = {
    normal: {
        color:'#7EBACC'
        }
    };    
// // var labelFromatter = {
// //     normal : {
// //         label : {
// //             formatter : function (params){
// //                 return 100 - params.value + '%'
// //             },

// //         }
// //     },
// // }
// var labelBottom = {
//     normal : {
//         color: '#ccc',
//         label : {
//             show : false,
//             position : 'center'
//         },

//     },
//     emphasis: {
//         color: 'rgba(0,0,0,0)'
//     }
// };
var radius = [33, 43];
var winrate11_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
    title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
          data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
    
        
    
    ]
};
var winrate21_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
     title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
    ]
};

var winrate31_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
    title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
          data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },    
    ]
};
var winrate41_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
     title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        }, 
    ]
};



var party_winrate11_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
    title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
          data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
    
        
    
    ]
};
var party_winrate21_1 = {
    // legend: {
    //     x : 'center',
    //     y : 'center',
    //     data:[
    //         'GoogleMaps','Facebook','Youtube','Google+'
    //     ],
    //     show : false,
    // },
     title : {
        text: '胜诉率',
        // subtext: 'from global web index',
        x: 'center'
    },
    
    series : [
        {
            type : 'pie',
            center : ['50%', '20%'],
            radius : radius,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '40%'],
            radius : radius,
            x:'20%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '60%'],
            radius : radius,
            x:'40%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
           data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
        {
            type : 'pie',
            center : ['50%', '80%'],
            radius : radius,
            x:'60%', // for funnel
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: "({d}%)"
                    
                },
                // emphasis: {
                //     show: true,
                //     textStyle: {
                //         fontSize: '30',
                //         fontWeight: 'bold'
                //     }
                // }
            },
            data : [
                  {name:'GoogleMaps', value:154,itemStyle:label1},
                  {name:'other', value:46,itemStyle:labelTop}
              
            ],

            itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
        },
    
        
    
    ]
};                

// ====================================================================================================================================================================================================//

var trialtime1_1 = {
    title: {
        text: '平均审理时长/天',
        subtext: '平均审理时长为104天',
        x:'center'
    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['行政确认类型案件','知识产权纠纷类型案件','竞争纠纷类型案件']
    },
    series: [
            {
                name: '平均审理时长',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            }, 
        ]
    };

var trialtime2_1 = {
    title: {
        text: '平均审理时长/天',
        subtext: '平均审理时长为104天',
        x:'center'
    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['裁定补正笔误','裁定管辖权异议成立','裁定不予受理','裁定驳回起诉','裁定撤诉']
    },
series: [
            {
                name: '平均审理时长',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            }, 
        ]
    };

var trialtime3_1 = {
    title: {
        text: '平均审理时长/天',
        subtext: '平均审理时长为104天',
        x:'center'
    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['行政确认类型案件','知识产权纠纷类型案件','竞争纠纷类型案件']
    },
    series: [
            {
                name: '平均审理时长',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            }, 
        ]
    };

var trialtime4_1 = {
    title: {
        text: '平均审理时长/天',
        subtext: '平均审理时长为104天',
        x:'center'
    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: ['裁定补正笔误','裁定管辖权异议成立','裁定不予受理','裁定驳回起诉','裁定撤诉']
    },
series: [
            {
                name: '平均审理时长',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            }, 
        ]
    };

var money_1 = {
    // title: {
    //     text: '',
    //     // subtext: '数据来自网络'
    //     // x:'center'
    // },
    legend: {
        data: ["诉请金额/万元","判赔金额/万元"],
        x: 'right',

    },
    // tooltip: {
    //     trigger: 'axis',
    //     axisPointer: {
    //         type: 'shadow'
    //     }
    // },
    grid: {
        left: '5%',
        right: '7%',
        bottom: '10%',
        containLabel: true
    },
    yAxis: {
        type: 'value',
        // boundaryGap: [0, 0.01]
    },
    xAxis: {
        type: 'category',
        data: ['平均值','极大值','极小值','中位数']
    },
    series: [
            {
                name: '诉请金额/万元',
                type: 'bar',
                barWidth:30,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [15, 17, 20, 23]
            },
            {
                name: '判赔金额/万元',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [5, 7, 2, 3]
            },
           
        ]
    };


var agent1_1 = {
    title: {
        text: '代理案件数量前三名',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};

var agent2_1 = {
    title: {
        text: '胜诉案件数量前三名',
        subtext: '平均胜诉案件数为8件',
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};
var agent3_1 = {
    title: {
        text: '案件胜诉率前三名',
        subtext: '平均胜诉率为20%',
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};

var agent4_1 = {
    title: {
        text: '代理案件数量前三名',
        // subtext: '数据来自网络'
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};

var agent5_1 = {
    title: {
        text: '胜诉案件数量前三名',
        subtext: '平均胜诉案件数为8件',
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};
var agent6_1 = {
    title: {
        text: '案件胜诉率前三名',
        subtext: '平均胜诉率为20%',
        x:'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    // legend: {
    //     data: ['2011年']
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        show: false
    },
    yAxis: {
        type: 'category',
        data: ['张晓津','杜长辉','姜颖']
    },
    series: [
        {
            name: '同审案件数量',
            type: 'bar',
            barWidth:20,
            data: [15, 17, 20],
    
             label: {
                                normal: {
                                    show: true,
                                    position: 'insideRight',
                                }
                            },
             itemStyle: {
                            normal: {
                                color:'#F4B81C'
                            }
                        },
        },
        // {
        //     name: '2012年',
        //     type: 'bar',
        //     data: [19325, 23438, 31000, 121594, 134141, 681807]
        // }
    ]
};


// ======================================================
var level1result1_1 = {
    title : {
        text: '判决',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'支持攻方部分诉请'},
                {value:2, name:'支持攻方全部诉请'},
                {value:2, name:'驳回攻方全部诉请'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var level1result2_1 = {
    title : {
        text: '裁定',
        subtext: '',
        subtextStyle:{
            fontSize: 18,
            fontWeight: 'bolder',
            color: '#333'
        },
        x:'center',
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'裁定驳回起诉'},
                {value:1, name:'裁定不予受理'},
                {value:2, name:'裁定撤诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};

var level2result3_1 = {
    title : {
        text: '判决',
        // subtext: '',
        x:'center'
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:6, name:'支持攻方部分诉请'},
                {value:2, name:'支持攻方全部诉请'},
                {value:2, name:'驳回攻方全部诉请'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var level2result4_1 = {
    title : {
        text: '裁定',
        subtext: '',
        subtextStyle:{
            fontSize: 18,
            fontWeight: 'bolder',
            color: '#333'
        },
        x:'center',
    },
    color:['#249DCC','#F4B81C','#F6CE61','#F8DB8D'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:1, name:'裁定驳回起诉'},
                {value:1, name:'裁定不予受理'},
                {value:2, name:'裁定撤诉'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};
// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);

var NUM=21;
var now=0;
$(function(){
    $('#judge>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#judge>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#judge>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#judge>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#judge>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});

