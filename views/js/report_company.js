// 基于准备好的dom，初始化echarts实例
var casetype1 = echarts.init(document.getElementById('casetype1'));
var casetype2 = echarts.init(document.getElementById('casetype2'));
var casetype3 = echarts.init(document.getElementById('casetype3'));
var casetype4 = echarts.init(document.getElementById('casetype4'));
var casetype5 = echarts.init(document.getElementById('casetype5'));
var month = echarts.init(document.getElementById('month'));
var rank = echarts.init(document.getElementById('rank'));
var site = echarts.init(document.getElementById('site'));
var courtname1 = echarts.init(document.getElementById('courtname1'));
var courtname2 = echarts.init(document.getElementById('courtname2'));
var courtname3 = echarts.init(document.getElementById('courtname3'));
var status1 = echarts.init(document.getElementById('status1'));
var level1 = echarts.init(document.getElementById('level1'));
var level2 = echarts.init(document.getElementById('level2'));
var level3 = echarts.init(document.getElementById('level3'));
var level4 = echarts.init(document.getElementById('level4'));
var level5 = echarts.init(document.getElementById('level5'));
var level6 = echarts.init(document.getElementById('level6'));
var judgeresult1 = echarts.init(document.getElementById('judgeresult1'));
var judgeresult2 = echarts.init(document.getElementById('judgeresult2'));
var judgeresult3 = echarts.init(document.getElementById('judgeresult3'));
var result1 = echarts.init(document.getElementById('result1'));
var result2 = echarts.init(document.getElementById('result2'));
var result3 = echarts.init(document.getElementById('result3'));
var result4 = echarts.init(document.getElementById('result4'));
var result5 = echarts.init(document.getElementById('result5'));
var result6 = echarts.init(document.getElementById('result6'));
var lawfirmname1 = echarts.init(document.getElementById('lawfirmname1'));
var lawfirmname2 = echarts.init(document.getElementById('lawfirmname2'));
var lawfirmname3 = echarts.init(document.getElementById('lawfirmname3'));
var type1 = echarts.init(document.getElementById('type1'));
var type2 = echarts.init(document.getElementById('type2'));
var oppositename1 = echarts.init(document.getElementById('oppositename1'));
var oppositename2 = echarts.init(document.getElementById('oppositename2'));
var oppositename3 = echarts.init(document.getElementById('oppositename3'));

// 指定图表的配置项和数据
// 指定图表的配置项和数据
var casetype1_1 = {
    title : {
        text: '案件类型',
        subtext: '',
        x:'center'
    },
    // color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案件类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var casetype2_1 = {
    title : {
        text: '民事类案件',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '民事类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:15, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var casetype3_1 = {
    title : {
        text: '刑事类案件',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '刑事类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:18, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var casetype4_1 = {
    title : {
        text: '行政类案件',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '行政类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:12, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var casetype5_1 = {
    title : {
        text: '执行类案件',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '执行类案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:8, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var month_1 = {
    title: {
        text: '月度变化',
        x:'center'
    },
    color:'#3abbe5',
    tooltip: {
        trigger: 'axis'
    },
    xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: ['周一','周二','周三','周四','周五','周六','周日']
    },

    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value}'
        }
    },
    series: [
        {
            name:'案件数量',
            type:'line',
            data:[1, 2, 15, 13, 12, 13, 10],
        },
    ]
};

var rank_1 = {
    title : {
        text: '法院层级',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#A776BF'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院层级',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var site_1 = {
    title : {
        text: '地域分布',
        left: 'center'
    },
    color:'orange',
    // color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#A776BF'],
    tooltip : {
        trigger: 'item'
    },
    series : [
        {
            name: '地域分布',
            type: 'map',
            // mapType: 'china',
            map: 'china',
            roam: false,
            selectedMode : 'multiple',
            itemStyle:{
                normal:{
                    label:{show:true}
                },
                
                emphasis:{
                    label:{show:true},
                    areaColor:'#5F3870'
                },

            },
            data:[
                {name:'北京', selected:true,value:25},
                {name:'广东', selected:true,value:20},
                {name:'上海', selected:true,value:20}
            ]
            
        }
    ]
};

var courtname1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院审理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};


var courtname2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院审理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};

var courtname3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法院审理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};


var status1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '诉讼地位',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var level1_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level2_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var level3_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level4_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var level5_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var level6_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var judgeresult1_1 = {
    title : {
        text: '整体裁判结果',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '整体裁判结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};


var judgeresult2_1 = {
    title : {
        text: '判决结果',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} :{c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '判决结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};

var judgeresult3_1 = {
    title : {
        text: '裁定结果',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁定结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：\n{c} ({d}%)"
                }
            }
        }
    ]
};


var result1_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var result2_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   

var result3_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为攻方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var result4_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result5_1 = {
    title : {
        text: '公司作为攻方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var result6_1 = {
    title : {
        text: '公司作为守方',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '公司作为守方',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawfirmname1_1 = {
    title : {
        text: '律所代理案件数量NO.1',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律所代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var lawfirmname2_1 = {
    title : {
        text: '律所代理案件数量NO.2',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律所代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var lawfirmname3_1 = {
    title : {
        text: '律所代理案件数量NO.3',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '律所代理案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var type1_1 = {
    title : {
        text: '类型分析',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '类型分析',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var type2_1 = {
    title : {
        text: '机构细分',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '机构细分',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


var oppositename1_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '对方当事人涉案数量',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
   
var oppositename2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '对方当事人涉案数量',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var oppositename3_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#5F3870','#734F85','#A47FB7','#B597C5','#D1BBDA','#DAC9E2','#E3D7EA','#ECE5F1'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '对方当事人涉案数量',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};


// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);


var NUM=15;
var now=0;
$(function(){
    $('#company>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#company>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#company>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#company>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#company>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});

