// 基于准备好的dom，初始化echarts实例
var lawsuit1 = echarts.init(document.getElementById('lawsuit1'));
var lawsuit2 = echarts.init(document.getElementById('lawsuit2'));
var party11 = echarts.init(document.getElementById('party11'));
var party12 = echarts.init(document.getElementById('party12'));
var party21 = echarts.init(document.getElementById('party21'));
var party22 = echarts.init(document.getElementById('party22'));
var numrank1 = echarts.init(document.getElementById('numrank1'));
var numrank2 = echarts.init(document.getElementById('numrank2'));
var total0 = echarts.init(document.getElementById('total0'));
var total1 = echarts.init(document.getElementById('total1'));
var total2 = echarts.init(document.getElementById('total2'));
var win0 = echarts.init(document.getElementById('win0'));
var win1 = echarts.init(document.getElementById('win1'));
var win2 = echarts.init(document.getElementById('win2'));
var win3 = echarts.init(document.getElementById('win3'));
var win4 = echarts.init(document.getElementById('win4'));
var top10 = echarts.init(document.getElementById('top10'));
var rank10 = echarts.init(document.getElementById('rank10'));
var rank11 = echarts.init(document.getElementById('rank11'));
var rank12 = echarts.init(document.getElementById('rank12'));
var rank13 = echarts.init(document.getElementById('rank13'));
var rank14 = echarts.init(document.getElementById('rank14'));
var rank20 = echarts.init(document.getElementById('rank20'));
var rank21 = echarts.init(document.getElementById('rank21'));
var rank22 = echarts.init(document.getElementById('rank22'));
var rank23 = echarts.init(document.getElementById('rank23'));
var rank24 = echarts.init(document.getElementById('rank24'));
var rank30 = echarts.init(document.getElementById('rank30'));
var rank31 = echarts.init(document.getElementById('rank31'));
var rank32 = echarts.init(document.getElementById('rank32'));
var rank33 = echarts.init(document.getElementById('rank33'));
var rank34 = echarts.init(document.getElementById('rank34'));
var organization1 = echarts.init(document.getElementById('organization1'));
var falv0 = echarts.init(document.getElementById('falv0'));
var falv1 = echarts.init(document.getElementById('falv1'));
var falv2 = echarts.init(document.getElementById('falv2'));
var sifajieshi0 = echarts.init(document.getElementById('sifajieshi0'));
var sifajieshi1 = echarts.init(document.getElementById('sifajieshi1'));
var sifajieshi2 = echarts.init(document.getElementById('sifajieshi2'));
var xingzhengfagui0 = echarts.init(document.getElementById('xingzhengfagui0'));
var xingzhengfagui1 = echarts.init(document.getElementById('xingzhengfagui1'));
var xingzhengfagui2 = echarts.init(document.getElementById('xingzhengfagui2'));



// 指定图表的配置项和数据
var lawsuit1_1 = {
    title: {
        text: ''
    },

    tooltip : {
        trigger: 'axis'
    },
    // legend: {
    //     data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
    // },
    // toolbox: {
    //     feature: {
    //         saveAsImage: {}
    //     }
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['周一']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'案件数量',
            type:'line',
            color:'#F7931F',
            label: {
                normal: {
                show: true,
                position: 'bottom'},
                   },
            data:[120],

            itemStyle:{
                normal:{
                    lineStyle:{
                    color:'#1485AF',
                    }
                }
            }
        },   
    ]
};

var lawsuit2_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '案由分布',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var party11_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方当事人',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}:{c}\n({d}%)"
                }
            }
        }
    ]
};
var party12_1 = {
    title : {
        text: '机构',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方机构当事人',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var party21_1 = {
    title : {
        text: '',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方当事人',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var party22_1 = {
    title : {
        text: '机构',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方机构当事人',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};

var numrank1_1 = {
        title:{
            text:'攻方数量排行前五名',
            x:'center'
        },
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },

        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四','周五']
        },
        series: [
            {
                name: '案件数量',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'insideRight'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [820, 832, 901, 934, 1290]
            }
        ]
    };

    var numrank2_1={
        
        title:{
        text:'守方数量排行前五名',
        x:'center'
        },

        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },

        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四','周五']
        },
         series: [
            {
                name: '案件数量',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'insideRight'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#39BAE4'
                }
            },
                data: [820, 832, 901, 934, 1290]
            }
        ]
    };

var total0_1 = {
   title : {
        text: '整体裁判结果',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#F39800','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '客户类型',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};
var total1_1 = {
    title : {
        text: '判决结果',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#187AA2','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '判决结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var total2_1 = {
    title : {
        text: '裁定结果',
        subtext: '',
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center',
    },
    color:['#F6AD3A','#F9C170','#FCD7A1','#F39800','#CC933B'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '裁定结果',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n ({d}%)"
                }
            }
        }
    ]
};
var win0_1 = {

       title:{
        text:'',
        x:'center'
        },
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data: ['判决案件', '胜诉案件']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四','周五','周六','周日']
        },
        series: [
            {
                name: '胜诉案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [320, 302, 301, 334]
            },
            {
                name: '判决案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [120, 132, 101, 134]
            },
           
        ]
    };

var win1_1 = {
    title : {
        text: '攻方机构胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方机构胜诉案件案由',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};

var win2_1 = {
    title : {
        text: '攻方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方个人胜诉案件案由',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};

var win3_1 = {
    title : {
        text: '守方机构胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方机构胜诉案件案由',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};

var win4_1 = {
    title : {
        text: '守方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方个人胜诉案件案由',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var top10_1 = {
        title:{
        text:'法官审理案件数量排行前十名',
        x:'center'
        },
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },

        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四','周五','周六','周日']
        },
        series: [
            {
                name: '审理案件数量',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'insideRight'
                    }
                },
                itemStyle: {
                normal: {
                     color: function(params) {
                        // build a color map as your need.
                        var colorList = [
                       '#3ABBE5','#3ABBE5','#3ABBE5','#3ABBE5','#3ABBE5','#3ABBE5','#3ABBE5','#0081AB','#0081AB','#1E2E4C'
                        ];
                        return colorList[params.dataIndex]
                    },
                }
            },
                   
                data: [320, 302, 301, 334, 390, 330, 320]
            },
        ]
    };
    var rank10_1 = {
        title:{
        text:"",
        subtext:"",
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
        },
        legend: {
            data: ['判决案件', '胜诉案件']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四']
        },
        series: [
            {
                name: '胜诉案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [320, 302, 301, 334, 390, 330, 320]
            },
            {
                name: '判决案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [120, 132, 101, 134, 90, 230, 210]
            },
           
        ]
    };
var rank11_1 = {
    title : {
        text: '攻方机构胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方机构胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var rank12_1 = {
    title : {
        text: '攻方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var rank13_1 = {
    title : {
        text: '守方机构胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方机构胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} \n({d}%)"
                }
            }
        }
    ]
};
var rank14_1 = {
    title : {
        text: '守方个人胜诉案件',
        subtext: '',
        x:'center'
    },
     color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank20_1 = {
        title:{
        text:"",
        subtext:"",
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
        },
        legend: {
            data: ['判决案件', '胜诉案件']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四']
        },
        series: [
            {
                name: '胜诉案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [320, 302, 301, 334, 390, 330, 320]
            },
            {
                name: '判决案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [120, 132, 101, 134, 90, 230, 210]
            },
           
        ]
    };
var rank21_1 = {
        title : {
            text: '攻方机构胜诉案件',
            subtext: '',
            x:'center'
        },
        color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        // legend: {
        //     orient: 'vertical',
        //     left: 'left',
        //     data: ['个人','机构']
        // },
        series : [
            {
                name: '攻方机构胜诉案件',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:5, name:'个人'},
                    {value:19, name:'机构'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label:{
                    normal:{
                        formatter: "{b}：{c}\n({d}%)"
                    }
                }
            }
        ]
    };
var rank22_1 = {
    title : {
        text: '攻方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank23_1 = {
    title : {
        text: '守方机构胜诉案件',
        subtext: '',
        x:'center'
    },
     color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方机构胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank24_1 = {
    title : {
        text: '守方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank30_1 ={
        title:{
        text:"",
        subtext:"",
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
        },
        legend: {
            data: ['判决案件', '胜诉案件']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis:  {
            type: 'value'
        },
        yAxis: {
            type: 'category',
            data: ['周一','周二','周三','周四']
        },
        series: [
            {
                name: '胜诉案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                 itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [320, 302, 301, 334, 390, 330, 320]
            },
            {
                name: '判决案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [120, 132, 101, 134, 90, 230, 210]
            },
           
        ]
    };
var rank31_1 = {
    title : {
        text: '攻方机构胜诉案件',
        subtext: '',
        x:'center'
    },
     color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方机构胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank32_1 = {
    title : {
        text: '攻方个人胜诉案件',
        subtext: '',
        x:'center'
    },
     color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '攻方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank33_1 = {
    title : {
        text: '守方机构胜诉案件',
        subtext: '',
        x:'center'
    },
     color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方机构胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};
var rank34_1 = {
    title : {
        text: '守方个人胜诉案件',
        subtext: '',
        x:'center'
    },
    color:['#36B0D6','#ACD1D7','#5ABFBA','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '守方个人胜诉案件',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}\n({d}%)"
                }
            }
        }
    ]
};

var organization1_1 = {
        title:{
        text:'',
        subtext:"案件数量排行前五机构当事人",
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
        },
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data: ['判决案件', '胜诉案件']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis:  {
        type: 'value'
    },
    yAxis: {
        type: 'category',
        data: ['周一2','周一1','周二2','周二1','周三2','周三1','周四2','周四1','周五2','周五1']
    },
     series: [
            {
                name: '胜诉案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#0180AB'
                }
            },
                data: [320, 302, 301, 334, 500,444,444,444,444,444]
            },
            {
                name: '判决案件',
                type: 'bar',
                barWidth:20,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'inside'
                    }
                },
                itemStyle: {
                normal: {
                    color:'#7EBACC'
                }
            },
                data: [120, 132, 101, 134, 500,555,555,3,333,222]
            },
    ]
};
var organization2_1 = {
     title:{
        text:'',
        subtext:"机构当事人胜诉案件案由",
        subtextStyle:{
        fontSize: 18,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
        },
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data: ['劳动合同', '福利待遇','社会保险']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis:  {
        type: 'value'
    },
    yAxis: {
        type: 'category',
    },
    series: [
        {
            name: '劳动合同',
            type: 'bar',
            barWidth:50,
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'insideRight'
                }
            },
            data: [320, 302, 301, 334, ]
        },
        {
            name: '福利待遇',
            type: 'bar',
            barWidth:40,
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'insideRight'
                }
            },
            data: [120, 132, 101, 134, 90]
        },
        {
            name: '社会保险',
            type: 'bar',
            barWidth:40,
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'insideRight'
                }
            },
            data: [220, 182, 191, 234, 290]
        },
        
    ]
};



var falv0_1 = {
    title : {
        text: '法律',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '法律',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}:{c}\n({d}%)"
                }
            }
        }
    ]
};
var falv1_1 = {
    title : {
        text: '适用次数最多的法律之一',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}"
                }
            }
        }
    ]
};
var falv2_1 = {
    title : {
        text: '适用次数最多的法律之二',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} "
                }
            }
        }
    ]
};

var sifajieshi0_1 = {
    title : {
        text: '司法解释',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '司法解释',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c}({d}%)"
                }
            }
        }
    ]
};
var sifajieshi1_1 = {
    title : {
        text: '适用次数最多的司法解释之一',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}"
                }
            }
        }
    ]
};
var sifajieshi2_1 = {
    title : {
        text: '适用次数最多的司法解释之二',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c} "
                }
            }
        }
    ]
};

var xingzhengfagui0_1 = {
    title : {
        text: '行政法规',
        subtext: '',
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '行政法规',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}\n{c}({d}%)"
                }
            }
        }
    ]
};
var xingzhengfagui1_1 = {
    title : {
        text: '适用次数最多的行政法规之一',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}"
                }
            }
        }
    ]
};
var xingzhengfagui2_1 = {
    title : {
        text: '适用次数最多的行政法规之二',
        subtext: '',
        subtextStyle:{
        fontSize: 14,
        fontWeight: 'bolder',
        color: '#333'
        },
        x:'center'
    },
    color:['#1e2f4c','#0180ab','#1790cf','#3abbe5','#50d2ca','#afd6dd','#cef0ee'],
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    // legend: {
    //     orient: 'vertical',
    //     left: 'left',
    //     data: ['个人','机构']
    // },
    series : [
        {
            name: '适用情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:5, name:'个人'},
                {value:19, name:'机构'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label:{
                normal:{
                    formatter: "{b}：{c}"
                }
            }
        }
    ]
};

// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);

var NUM=17;
var now=0;
$(function(){
    $('#court>div').each(function(j){
            $("#page"+j).hide();
        });
    $("#page0").show();

    $("#button_firstpage").click(function(){
        $('#court>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page0").show();
        now=0;
    });
    $("#button_prevpage").click(function(){
        if (now!==0) {
            $('#court>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(--now)).show();
        }
    });
    $("#button_nextpage").click(function(){
        if (now!==NUM) {
            $('#court>div').each(function(j){
                $("#page"+j).hide();
            });
            $("#page"+(++now)).show();
        }
    });
    $("#button_lastpage").click(function(){
        $('#court>div').each(function(j){
            $("#page"+j).hide();
        });
        $("#page"+NUM).show();
        now=NUM;
    });
});

