// 基于准备好的dom，初始化echarts实例
var multitimespan = echarts.init(document.getElementById('multi_time_span'));
var multilocationspan = echarts.init(document.getElementById('multi_location_span'));
var relatedlaws = echarts.init(document.getElementById('related_laws'));

//第一家公司
var timespan = echarts.init(document.getElementById('time_span'));
var locationspan = echarts.init(document.getElementById('location_span'));
var courtprocedure = echarts.init(document.getElementById('court_procedure'));
var courtrank = echarts.init(document.getElementById('court_rank'));
var winorlose = echarts.init(document.getElementById('win_or_lose'));
var opponenttopfive = echarts.init(document.getElementById('opponent_top_five'));
var officetopfive = echarts.init(document.getElementById('office_top_five'));
var lawyertopfive = echarts.init(document.getElementById('lawyer_top_five'));
var courttopfive = echarts.init(document.getElementById('court_top_five'));
var judgetopfive = echarts.init(document.getElementById('judge_top_five'));


// 指定图表的配置项和数据
var multi_time_span = {
    title: {
        text: '时间分布',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '0.1%',
        right: '0.1%',
        bottom: '10%',
        containLabel: true
    },
    yAxis: {
        type: 'value'
    },
    xAxis: {
        type: 'category',
        data: ['2011年1月', '2011年2月', '2011年3月', '2011年4月', '2011年5月', '2011年6月',
            '2011年7月', '2011年8月', '2011年9月', '2011年10月', '2011年11月', '2011年12月',
            '2012年1月', '2012年2月', '2012年3月', '2012年4月', '2012年5月', '2012年6月',
            '2012年7月', '2012年8月', '2012年9月', '2012年10月', '2012年11月', '2012年12月',
            '2013年1月', '2013年2月', '2013年3月', '2013年4月', '2013年5月', '2013年6月',
            '2013年7月', '2013年8月', '2013年9月', '2013年10月', '2013年11月', '2013年12月',
            '2014年1月', '2014年2月', '2014年3月', '2014年4月', '2014年5月', '2014年6月',
            '2014年7月', '2014年8月', '2014年9月', '2014年10月', '2014年11月', '2014年12月',
            '2015年1月', '2015年2月', '2015年3月', '2015年4月', '2015年5月', '2015年6月',
            '2015年7月', '2015年8月', '2015年9月', '2015年10月', '2015年11月', '2015年12月'
        ],
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 10  //刻度大小
            },
            rotate: 60,
            interval: 3
        }
    },
    series: [
        {
            name: '数量',
            type: 'bar',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            data: [3, 10, 20, 35, 30, 6, 8, 45, 65, 22, 21, 34, 87, 90, 16, 49, 5, 38, 24,
                17, 14, 46, 7, 10, 20, 78, 70, 2, 8, 48, 28, 22, 21, 74, 47, 130, 12, 49, 8, 78, 24,
                17, 14, 42, 7, 50, 20, 78, 70, 32, 8, 88, 28, 122, 25, 78, 87, 20, 52, 82
            ],
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            }
        }
    ]
};


var multi_location_span = {
    title: {
        text: '地域分布',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'item',
        formatter: '{b}'
    },
    series: [
        {
            name: '中国',
            type: 'map',
            mapType: 'china',
            selectedMode: 'multiple',
            itemStyle: {
                normal: {
                    borderWidth: 1,
                    areaColor: '#60C0DD',
                    label: {
                        show: true,
                        textStyle: {
                            color: 'black'            //颜色再调一下
                        }
                    }
                },
                emphasis: {                 // 选中样式
                    borderWidth: 1,
                    borderColor: '#fff',
                    areaColor: '#ccc',
                    label: {
                        show: true,
                        formatter: "{b}\n{c}",
                        textStyle: {
                            fontSize: 14,
                            color: 'red'                   //颜色调一下
                        }
                    }
                }
            },
            data: [
                {name: '广东', selected: true, value: 10},
                {name: '山西', selected: true, value: 30},
                {name: '上海', selected: true, value: 24}
            ]
        }
    ]
};

var related_laws = {
    title: {
        text: '相关法律、法规、解释出现次数统计',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '700',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['《中华人民共和国侵权责任法》第三十六条',
            '《中华人民共和国著作法》第十条、第三十\n八条、第四十二条、第四十八条',
            '《信息网络传播权保护条例》',
            '《最高人民法院关于审理侵害信息网络传播\n民事纠纷案件适用法律若干问题的规定》'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [86, 1362, 350, 344].reverse()
        }
    ]
};

//各公司图表
//公司时间分布
var time_span = {
    title: {
        text: '时间分布',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '0.1%',
        right: '0.1%',
        bottom: '10%',
        containLabel: true
    },
    yAxis: {
        type: 'value'
    },
    xAxis: {
        type: 'category',
        data: ['2011年1月', '2011年2月', '2011年3月', '2011年4月', '2011年5月', '2011年6月',
            '2011年7月', '2011年8月', '2011年9月', '2011年10月', '2011年11月', '2011年12月',
            '2012年1月', '2012年2月', '2012年3月', '2012年4月', '2012年5月', '2012年6月',
            '2012年7月', '2012年8月', '2012年9月', '2012年10月', '2012年11月', '2012年12月',
            '2013年1月', '2013年2月', '2013年3月', '2013年4月', '2013年5月', '2013年6月',
            '2013年7月', '2013年8月', '2013年9月', '2013年10月', '2013年11月', '2013年12月',
            '2014年1月', '2014年2月', '2014年3月', '2014年4月', '2014年5月', '2014年6月',
            '2014年7月', '2014年8月', '2014年9月', '2014年10月', '2014年11月', '2014年12月',
            '2015年1月', '2015年2月', '2015年3月', '2015年4月', '2015年5月', '2015年6月',
            '2015年7月', '2015年8月', '2015年9月', '2015年10月', '2015年11月', '2015年12月'
        ],
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 10  //刻度大小
            },
            rotate: 60,
            interval: 3
        }
    },
    series: [
        {
            name: '数量',
            type: 'bar',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            data: [3, 10, 20, 35, 30, 6, 8, 45, 65, 22, 21, 34, 87, 90, 16, 49, 5, 38, 24,
                17, 14, 46, 7, 10, 20, 78, 70, 2, 8, 48, 28, 22, 21, 74, 47, 130, 12, 49, 8, 78, 24,
                17, 14, 42, 7, 50, 20, 78, 70, 32, 8, 88, 28, 122, 25, 78, 87, 20, 52, 82
            ],
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            }
        }
    ]
};

//公司地域分布
var location_span = {
    title: {
        text: '地域分布',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'item',
        formatter: '{b}'
    },
    series: [
        {
            name: '中国',
            type: 'map',
            mapType: 'china',
            selectedMode: 'multiple',
            itemStyle: {
                normal: {
                    borderWidth: 1,
                    areaColor: '#60C0DD',
                    label: {
                        show: true,
                        textStyle: {
                            color: 'black'            //颜色再调一下
                        }
                    }
                },
                emphasis: {                 // 选中样式
                    borderWidth: 1,
                    borderColor: '#fff',
                    areaColor: '#ccc',
                    label: {
                        show: true,
                        formatter: "{b}\n{c}",
                        textStyle: {
                            fontSize: 14,
                            color: 'red'                   //颜色调一下
                        }
                    }
                }
            },
            data: [
                {name: '广东', selected: true, value: 10},
                {name: '山西', selected: true, value: 30},
                {name: '上海', selected: true, value: 24}
            ]
        }
    ]
};

var court_procedure = {
    title: {
        text: '审理程序',
        subtext: ''
    },
    color: ['#1e2f4c', '#0180ab', '#1790cf', '#3abbe5', '#50d2ca', '#afd6dd', '#cef0ee'],
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        right: 'right',
        data: ['一审', '二审']
    },
    series: [
        {
            name: '审理类型',
            type: 'pie',
            radius: '75%',
            center: ['50%', '55%'],
            data: [
                {value: 217, name: '一审'},
                {value: 51, name: '二审'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            label: {
                normal: {
                    formatter: "{b}：{c} ({d}%)"
                }
            }
        }
    ]
};

var win_or_lose = {
    title: {
        text: '案件胜败',
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data: ['胜诉', '败诉'],
        top: 20
    },
    grid: {
        containLabel: true
    },
    xAxis: {
        type: 'value',
        inverse: true,
        axisLine: {show: false},
        axisTick: {show: false},
        splitLine: {show: false},
        axisLabel: {show: false}
    },
    yAxis: {
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'dashed'
            }
        },
        axisTick: {show: false},
        splitLine: {show: false},
        data: ['原告', '被告', '上诉人', '被上诉人'].reverse(),
        axisLabel: {
            margin: 50,
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 16,  //刻度大小
                align: 'center'
            }
        }
    },
    series: [
        {
            name: '胜诉',
            type: 'bar',
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'left',
                    textStyle: {
                        fontSize: 16
                    }
                }
            },
            data: [160, 191, 2, 0].reverse()
        },
        {
            name: '败诉',
            type: 'bar',
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'right',
                    textStyle: {
                        fontSize: 16
                    }
                }
            },
            data: [-23, -24, -34, -32].reverse()
        }
    ]
};

var opponent_top_five = {
    title: {
        text: '对方当事人（企业）案件数量 Top 5',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '70%',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['乐视网信息技术（北京）股份有限公司',
            '广州市千钧网络科技有限公司',
            '东阳神话影视发行有限公司',
            '深圳市迅雷网络技术有限公司',
            '上海众源网络有限公司'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [51, 31, 20, 18, 16].reverse()
        }
    ]
};

var office_top_five = {
    title: {
        text: '代理律所案件数量 Top 5',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '70%',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['北京德和衡律师事务所',
            '上海瀛泰律师事务所',
            '山东德衡律师事务所',
            '北京金诚同达律师事务所',
            '北京市亚奥事务所'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [56, 30, 6, 1, 1].reverse()
        }
    ]
};

var lawyer_top_five = {
    title: {
        text: '代理律师案件数量 Top 5',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '70%',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['姚克枫（北京德和衡律师事务所）',
            '杨忠勤（上海瀛泰律师事务所）',
            '戎燕茹（山东德衡律师事务所）',
            '史学清（北京金诚同达律师事务所）',
            '刘新炎（北京市亚奥事务所）'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [55, 30, 6, 1, 1].reverse()
        }
    ]
};

var court_top_five = {
    title: {
        text: '审理法院案件数量 Top 5',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '70%',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['北京市海淀区人民法院',
            '北京市朝阳区人民法院',
            '北京市第一中级人民法院',
            '广东省广州市天河区人民法院',
            '上海市第一中级人民法院'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [150, 27, 22, 16, 13].reverse()
        }
    ]
};

var judge_top_five = {
    title: {
        text: '审理法官案件数量 Top 5',
        textStyle: {
            color: '#60C0DD'
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        show: false,
        height: '70%',
        left: '280',
        right: '30',
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        show: true,
        type: 'category',
        data: ['王嘉佳（北京市海淀区人民法院）',
            '王婧玲（北京市海淀区人民法院）',
            '丁岚（北京市海淀区人民法院）',
            '张连勇（北京市海淀区人民法院）',
            '苏志甫（北京市朝阳区人民法院）'
        ].reverse(),
        axisTick: {
            show: false
        },
        axisLabel: {
            textStyle: {
                color: "black", //刻度颜色
                fontSize: 14  //刻度大小
            }
        },
        splitLine: {
            show: false
        }
    },
    series: [
        {
            name: 'related_laws',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#60C0DD'
                }
            },
            label: {
                normal: {
                    show: true,
                    //formatter: '{b}',
                    position: 'right',
                    textStyle: {
                        color: 'black'
                    }
                }
            },
            data: [57, 53, 38, 21, 16].reverse()
        }
    ]
};

// 使用刚指定的配置项和数据显示图表。
// myChart.setOption(option);

//新添加的图
multitimespan.setOption(multi_time_span);
multilocationspan.setOption(multi_location_span);
relatedlaws.setOption(related_laws);

timespan.setOption(time_span);
locationspan.setOption(location_span);
courtprocedure.setOption(court_procedure);
winorlose.setOption(win_or_lose);
opponenttopfive.setOption(opponent_top_five);
officetopfive.setOption(office_top_five);
lawyertopfive.setOption(lawyer_top_five);
courttopfive.setOption(court_top_five);
judgetopfive.setOption(judge_top_five);


