<?php 

class GetWhole{

	public function __construct($database_name,$collection_name){

		require_once('../libraries/pe/PHPExcel.php');  
		require_once('../libraries/pe/PHPExcel/Writer/Excel2007.php');  
		require_once('../libraries/pe/PHPExcel/Writer/Excel5.php'); 
		try {
			$conn = new MongoClient();
			$db = $conn ->$database_name;
			$this->test = $db->$collection_name;
			$arr=array();
			$this->rst=$test->find($arr);
		} catch (Exception $e) {
			file_put_contents('c:/dberror.log', $e,FILE_APPEND);			
		} 
	}


	public function getWhole($name){
		
		
		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel); 
		$objPHPExcel->setActiveSheetIndex(0);

		$kk=2;
		
		foreach ($this->rst as $val) {
			$fid=$val['_id'];
			$title=$val['title'];
			$doctype=$val['doctype'];
			$casenum=$val['casenum'];
			$casetype=$val['casetype'];
			$tag=$val['tag'];
			$subtag=$val['subtag'];
			$rank=$val['rank'];
			$date=$val['date'];
			$party=$val['party'];
			$agent=$val['agent'];
			$court=$val['court'];
			$outcome=$val['outcome'];
			$level=$val['level'];
			$justice=$val['justice'];
			$winner=$val['winner'];
			$site=$val['site'];
			$result=$val['result'];
			$procedure=$val['procedure'];

			$gong=array();
			$shou=array();
			$third=array();
			$gongLawyer=array();
			$shouLawyer=array();
			$thirdLawyer=array();


			$lawoffice=array();

			foreach ($party as $key => $value) {
				if ($name!="") {
					if ($value['name']==$name) {
						if ($value['type']=='plaintiff'||$value['type']=='appellant'||$value['type']=='proposer') {
							$gong=array_merge($gong, array($value['name']));
							if (count($value['mandatary'])!=0) {
								foreach ($value['mandatary'] as $k=> $v) {
									if ($v['type']=='lawyer') {
										$gongLawyer=array_merge($gongLawyer,array($v['name'].'（'.$v['office'].'）'));
									}
								}
							}
						} else if ($value['type']=='defendant'||$value['type']=='appellee'||$value['type']=='respondent') {
							$shou=array_merge($shou, array($value['name']));
							if (count($value['mandatary'])!=0) {
								foreach ($value['mandatary'] as $k=> $v) {
									if ($v['type']=='lawyer') {
										$shouLawyer=array_merge($shouLawyer,array($v['name'].'（'.$v['office'].'）'));
									}
								}
							}
						} else if ($value['type']=='thirdOne'||$value['type']=='thirdTwo'||$value['type']=='thirdThree') {
							$third=array_merge($third, array($value['name']));
							if (count($value['mandatary'])!=0) {
								foreach ($value['mandatary'] as $k=> $v) {
									if ($v['type']=='lawyer') {
										$thirdLawyer=array_merge($thirdLawyer,array($v['name'].'（'.$v['office'].'）'));
									}
								}
							}
						}
					}
				}else{
					if ($value['type']=='plaintiff'||$value['type']=='appellant'||$value['type']=='proposer') {
						$gong=array_merge($gong, array($value['name']));
						if (count($value['mandatary'])!=0) {
							foreach ($value['mandatary'] as $k=> $v) {
								if ($v['type']=='lawyer') {
									// echo "hehe";
									$gongLawyer=array_merge($gongLawyer,array($v['name'].'（'.$v['office'].'）'));
								}
							}
						}
					} else if ($value['type']=='defendant'||$value['type']=='appellee'||$value['type']=='respondent') {
						$shou=array_merge($shou, array($value['name']));
						if (count($value['mandatary'])!=0) {
							foreach ($value['mandatary'] as $k=> $v) {
								if ($v['type']=='lawyer') {
									// echo "haha";
									$shouLawyer=array_merge($shouLawyer,array($v['name'].'（'.$v['office'].'）'));
								}
							}
						}
					} else if ($value['type']=='thirdOne'||$value['type']=='thirdTwo'||$value['type']=='thirdThree') {
						$third=array_merge($third, array($value['name']));
						if (count($value['mandatary'])!=0) {
							foreach ($value['mandatary'] as $k=> $v) {
								if ($v['type']=='lawyer') {
									// echo "hihi";
									$thirdLawyer=array_merge($thirdLawyer,array($v['name'].'（'.$v['office'].'）'));
								}
							}
						}
					}
				}

			}

			// print_r($gongLawyer);
			// print_r($shouLawyer);
			// print_r($thirdLawyer);

			$judge = array();
			foreach ($justice as $key => $value) {
				// print_r($value);
				if ($value['type']=='presideJudge'||$value['type']=='judge') {
					$judge = array_merge($judge,array($value['name']));
				}
			}

			// print_r($judge);
			$gongName=implode("，", $gong);
			$shouName=implode("，", $shou);
			$thirdName=implode("，", $third);

			$gongLawyerName=implode("，", $gongLawyer);
			$shouLawyerName=implode("，", $shouLawyer);
			$thirdLawyerName=implode("，", $thirdLawyer);
			// echo $gongLawyerName;
			// echo "<br>";
			// echo $shouLawyerName;
			// echo "<br>";
			// echo $thirdLawyerName;
			// echo "<br>";

			$judgeName=implode("，", $judge);

			foreach ($agent as $key1 => $val1) {
				// echo $val1['name'];
				if (strpos($val1['office'], '方达')) {
					foreach ($val1['client'] as $key2 => $val2) {
						$status=$val2['type'];
					}
				}
			}

			// echo $status;
			$objPHPExcel->getActiveSheet()->setCellValue('A1', '标题'); 
			$objPHPExcel->getActiveSheet()->setCellValue('B1', '案号'); 
			$objPHPExcel->getActiveSheet()->setCellValue('C1', '攻方'); 
			$objPHPExcel->getActiveSheet()->setCellValue('D1', '守方'); 
			$objPHPExcel->getActiveSheet()->setCellValue('E1', '第三方'); 
			$objPHPExcel->getActiveSheet()->setCellValue('F1', '法院'); 
			// $objPHPExcel->getActiveSheet()->setCellValue('G1', '律所'); 
			$objPHPExcel->getActiveSheet()->setCellValue('H1', '法官'); 
			$objPHPExcel->getActiveSheet()->setCellValue('I1', '审级'); 
			$objPHPExcel->getActiveSheet()->setCellValue('J1', '层级'); 
			$objPHPExcel->getActiveSheet()->setCellValue('K1', '地域'); 
			$objPHPExcel->getActiveSheet()->setCellValue('L1', '案件类型'); 
			$objPHPExcel->getActiveSheet()->setCellValue('M1', '二级案由'); 
			$objPHPExcel->getActiveSheet()->setCellValue('N1', '三级案由'); 
			$objPHPExcel->getActiveSheet()->setCellValue('O1', '裁判日期'); 
			$objPHPExcel->getActiveSheet()->setCellValue('P1', '文书类型'); 
			$objPHPExcel->getActiveSheet()->setCellValue('Q1', '裁判结果'); 
			$objPHPExcel->getActiveSheet()->setCellValue('R1', '胜诉方'); 
			$objPHPExcel->getActiveSheet()->setCellValue('S1', '攻方律师'); 
			$objPHPExcel->getActiveSheet()->setCellValue('T1', '守方律师'); 
			$objPHPExcel->getActiveSheet()->setCellValue('U1', '第三方律师'); 
			$objPHPExcel->getActiveSheet()->setCellValue('V1', '诉讼地位'); 
			$objPHPExcel->getActiveSheet()->setCellValue('W1', '判决如下'); 
			$objPHPExcel->getActiveSheet()->setCellValue('X1', '审理经过'); 

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$kk, $title); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$kk, $casenum); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$kk, $gongName); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$kk, $shouName); 
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$kk, $thirdName); 
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$kk, $court); 
			// $objPHPExcel->getActiveSheet()->setCellValue('G'.$kk, $lawofficeName); 
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$kk, $judgeName); 
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$kk, $level); 
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$kk, $rank); 
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$kk, $site); 
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$kk, $casetype); 
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$kk, $tag); 
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$kk, $subtag); 
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$kk, $date); 
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$kk, $doctype); 
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$kk, $outcome); 
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$kk, $winner); 
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$kk, $gongLawyerName); 
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$kk, $shouLawyerName); 
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$kk, $thirdLawyerName); 
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$kk, $status); 
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$kk, $result); 
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$kk, $procedure); 

			$kk++;

		}
		$objWriter->save("C:/fangda-whole.xls");
	}


}
