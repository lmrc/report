<?php

/**
* 
*/
class ReportJudge
{
	
	function __construct()
	{
		
	}

	//判断律师代理案件的胜败情况
	function getLawyerShengbai($level='全部'){
		$lawyer=$this->getLawyer();
		if ($level=='全部') {
			$lawyerArr=$lawyer[0];
			$res=$this->getShengbai($lawyerArr);
		}else if ($level=='一审') {
			$lawyerArr=$lawyer[1];
			$res=$this->getShengbai($lawyerArr);
		}else if ($level=='二审') {
			$lawyerArr=$lawyer[2];
			$res=$this->getShengbai($lawyerArr);
		}else if ($level=='再审') {
			$lawyerArr=$lawyer[3];
			$res=$this->getShengbai($lawyerArr);
		}

		return $res;
	}

	//判断律师代理案件的胜诉率
	function getLawyerShenglv($level='全部'){
		$lawyer=$this->getLawyer();
		if ($level=='全部') {
			$lawyerArr=$lawyer[0];
			$res=$this->getShenglv($lawyerArr);
		}else if ($level=='一审') {
			$lawyerArr=$lawyer[1];
			$res=$this->getShenglv($lawyerArr);
		}else if ($level=='二审') {
			$lawyerArr=$lawyer[2];
			$res=$this->getShenglv($lawyerArr);
		}else if ($level=='再审') {
			$lawyerArr=$lawyer[3];
			$res=$this->getShenglv($lawyerArr);
		}

		return $res;
	}

	//判断律师代理案件的胜败情况
	function getShengbai($lawyerArr){
		
		foreach ($lawyerArr as $k=>$lawyer) {
			$lawyerArr[$k]['win']=0;
			foreach ($this->rst as $val) {
				$agentArr=$val['agent'];
				foreach ($agentArr as $agent) {
					if ($agent['type']=="lawyer"&&$lawyer['name']==$agent['name']&&$lawyer['office']==$agent['office']&&$agent['shengbai']=='win') {
						$lawyerArr[$k]['win']++;
					}
				}
			}
		}
		//按照win倒序排列
		array_multisort(array_column($lawyerArr,'win'),SORT_DESC,$lawyerArr);

		return $lawyerArr;
	}

	//判断律师代理案件的胜败情况
	function getShenglv($lawyerArr){
		
		foreach ($lawyerArr as $k=>$lawyer) {
			$lawyerArr[$k]['rate']=0;
			$lawyerArr[$k]['win']=0;
			foreach ($this->rst as $val) {
				$agentArr=$val['agent'];
				foreach ($agentArr as $agent) {
					if ($agent['type']=="lawyer"&&$lawyer['name']==$agent['name']&&$lawyer['office']==$agent['office']&&$agent['shengbai']=='win') {
						$lawyerArr[$k]['win']++;
					}
				}
			}
			$rate=$lawyerArr[$k]['win']/$lawyerArr[$k]['value'];
			// $lawyerArr[$k]['rate']=sprintf("%.1f", $rate*100).'%';
			$lawyerArr[$k]['rate']=$rate;
		}
		// return $lawyerArr;
		//按照win倒序排列
		array_multisort(array_column($lawyerArr,'rate'),SORT_DESC,$lawyerArr);
		//格式化胜诉率
		foreach ($lawyerArr as $k=>$lawyer) {
			$rate=$lawyerArr[$k]['win']/$lawyerArr[$k]['value'];
			$lawyerArr[$k]['rate']=sprintf("%.1f", $rate*100).'%';
		}

		return $lawyerArr;
	}

}