<?php


class Report{
	public function __construct(){
		require_once('basicexport_court.class.php');
		require_once('subjectexport_court.class.php');

		
		try {
			//本地数据库
			$conn = new MongoClient('192.168.170.33');
			//大鹏数据库
			// $conn = new MongoClient('192.168.170.33:27017');
			// $db = $conn ->local;
			$db = $conn ->test;
			// $test = $db->tencent;
			$test = $db->haidian;
			$arr=array();
			$this->rst=$test->find($arr);
		} catch (Exception $e) {
			file_put_contents('c:/dberror.log', $e,FILE_APPEND);	
		}
	}

	public function ajax(){
		header("Access-Control-Allow-Origin: *");
		// echo json_encode($_POST);  

		$arr = array(
			array('value'=>13,'name'=>'一审'),
			array('value'=>212,'name'=>'二审'),
			array('value'=>221,'name'=>'再审'),
			array('value'=>56,'name'=>'执行')
		);
		// print_r(json_encode($arr));
		echo json_encode($arr);
	}

	public function courtReport(){
		
		$courtname = '北京市海淀区人民法院';
		$date="2014年";
		$scale="劳动争议案件";
		$wenshusource="中国裁判文书网";
		$judgedate="2014年1月1日到2014年12月31日";
		$sitescope="北京市海淀区";
		$casefield="劳动争议";
		$casecount="1509";
		$othersource="北京市海淀区人民法院官网";
		$lastvisit="2015 年 10 月 15 日";
		
		
		$subjectExport = new SubjectExport();
		$basicExport = new BasicExport();
		$courtArr = array();

		//获取基本信息

		$courtArr=array_merge($courtArr,array('courtname'=>$courtname));
		$courtArr=array_merge($courtArr,array('date'=>$date));
		$courtArr=array_merge($courtArr,array('scale'=>$scale));
		$courtArr=array_merge($courtArr,array('wenshusource'=>$wenshusource));
		$courtArr=array_merge($courtArr,array('judgedate'=>$judgedate));
		$courtArr=array_merge($courtArr,array('sitescope'=>$sitescope));
		$courtArr=array_merge($courtArr,array('casefield'=>$casefield));
		$courtArr=array_merge($courtArr,array('casecount'=>$casecount));
		$courtArr=array_merge($courtArr,array('othersource'=>$othersource));
		$courtArr=array_merge($courtArr,array('lastvisit'=>$lastvisit));

        //月度分布
		$monthArr = $basicExport->getMonth($this->rst);
		$courtArr=array_merge($courtArr,array('04A1'=>array_values($monthArr)));
		$courtArr=array_merge($courtArr,array('04A2'=>array_keys($monthArr)));
        
        //三级案由分布
        $thirdtagArr = $basicExport->getLbthirdtag($this->rst);
		$courtArr=array_merge($courtArr,array('04B'=>$thirdtagArr));
                    
		// //获取攻方当事人类型
        $GdsrtypeArr = $subjectExport->getGdsrtype($this->rst);
		$courtArr=array_merge($courtArr,array('05A'=>$GdsrtypeArr));
		// // //获取攻方机构当事人细分类型
        $GjgdsrtypeArr=$subjectExport->isGCompany($this->rst);
        $courtArr=array_merge($courtArr,array('05B'=>$GjgdsrtypeArr));

        // //获取守方当事人类型
        $SdsrtypeArr = $subjectExport->getSdsrtype($this->rst);
		$courtArr=array_merge($courtArr,array('05C'=>$SdsrtypeArr));
		// // //获取守方机构当事人细分类型
        $SjgdsrtypeArr=$subjectExport->isSCompany($this->rst);
        $courtArr=array_merge($courtArr,array('05D'=>$SjgdsrtypeArr));

        //获取攻方机构当事人案件数量排行前五
        $gjgdsrno5Arr = $subjectExport->getGjgdsrno5($this->rst);
        $gjgdsrnameArr=array_keys($gjgdsrno5Arr);  
        $gjgdsrnumArr=array_values($gjgdsrno5Arr); 
        $gjgdsrno5nameArr=array($gjgdsrnameArr[4],$gjgdsrnameArr[3],$gjgdsrnameArr[2],$gjgdsrnameArr[1],$gjgdsrnameArr[0]);
        $gjgdsrno5numArr=array($gjgdsrnumArr[4],$gjgdsrnumArr[3],$gjgdsrnumArr[2],$gjgdsrnumArr[1],$gjgdsrnumArr[0]);
		
		$courtArr=array_merge($courtArr,array('06A1'=>$gjgdsrno5numArr));
		$courtArr=array_merge($courtArr,array('06A2'=>$gjgdsrno5nameArr));

		//获取守方机构当事人案件数量排行前五
        $sjgdsrno5Arr = $subjectExport->getSjgdsrno5($this->rst);
        $sjgdsrnameArr=array_keys($sjgdsrno5Arr);  
        $sjgdsrnumArr=array_values($sjgdsrno5Arr); 
        $sjgdsrno5nameArr=array($sjgdsrnameArr[4],$sjgdsrnameArr[3],$sjgdsrnameArr[2],$sjgdsrnameArr[1],$sjgdsrnameArr[0]);
        $sjgdsrno5numArr=array($sjgdsrnumArr[4],$sjgdsrnumArr[3],$sjgdsrnumArr[2],$sjgdsrnumArr[1],$sjgdsrnumArr[0]);
		
		$courtArr=array_merge($courtArr,array('06B1'=>$sjgdsrno5numArr));
		$courtArr=array_merge($courtArr,array('06B2'=>$sjgdsrno5nameArr));

		//获取文书类型
        $doctypeArr=$basicExport->getDoctype($this->rst);
        $courtArr=array_merge($courtArr,array('07A'=>$doctypeArr));

        //获取判决胜败结果
        $panjuewinnerArr=$basicExport->getpanjuewinner($this->rst);
        $courtArr=array_merge($courtArr,array('07B'=>$panjuewinnerArr));
        //获取裁定结果
        $caidingresultArr=$basicExport->getCaidingResult($this->rst);
        $courtArr=array_merge($courtArr,array('07C'=>$caidingresultArr));

        //获取当事人（区分机构和个人）案件胜率情况
        $panjueresultArr=$subjectExport->getgspanjueresult($this->rst);
        $yaxisArr=array($panjueresultArr[3]['name'],$panjueresultArr[2]['name'],$panjueresultArr[1]['name'],$panjueresultArr[0]['name']);
        $panjuecaseArr=array($panjueresultArr[3]['value'],$panjueresultArr[2]['value'],$panjueresultArr[1]['value'],$panjueresultArr[0]['value']);
        $shengsucaseArr=array($panjueresultArr[7]['value'],$panjueresultArr[6]['value'],$panjueresultArr[5]['value'],$panjueresultArr[4]['value']);
        $courtArr=array_merge($courtArr,array('08A1'=>$panjuecaseArr));
        $courtArr=array_merge($courtArr,array('08A2'=>$shengsucaseArr));
        $courtArr=array_merge($courtArr,array('08A3'=>$yaxisArr));

        //获取攻方机构当事人胜诉案件的三级案由
        $GJGpanjuecasetypeArr=$basicExport->getGJGpanjuecasetype($this->rst);
        $courtArr=array_merge($courtArr,array('08B'=>$GJGpanjuecasetypeArr));

        //获取攻方个人当事人胜诉案件的三级案由
        $GGRpanjuecasetypeArr=$basicExport->getGGRpanjuecasetype($this->rst);
        $courtArr=array_merge($courtArr,array('08C'=>$GGRpanjuecasetypeArr));

        //获取守方机构当事人胜诉案件的三级案由
        $SJGpanjuecasetypeArr=$basicExport->getSJGpanjuecasetype($this->rst);
        $courtArr=array_merge($courtArr,array('08D'=>$SJGpanjuecasetypeArr));

        //获取守方机构当事人胜诉案件的三级案由
        $SGRpanjuecasetypeArr=$basicExport->getSGRpanjuecasetype($this->rst);
        $courtArr=array_merge($courtArr,array('08E'=>$SGRpanjuecasetypeArr));

        //获取法官审理案件前十
        $judgeno10caseArr=$subjectExport->getJudge($this->rst);
        $arrjudgeno10nameArr=array_keys($judgeno10caseArr);
        $arrjudgeno10numArr=array_values($judgeno10caseArr);
        $arrjudgeno10nameArrsep=array($arrjudgeno10nameArr[9],$arrjudgeno10nameArr[8],$arrjudgeno10nameArr[7],$arrjudgeno10nameArr[6],$arrjudgeno10nameArr[5],$arrjudgeno10nameArr[4],$arrjudgeno10nameArr[3],$arrjudgeno10nameArr[2],$arrjudgeno10nameArr[1],$arrjudgeno10nameArr[0]);
        $arrjudgeno10numArrsep=array($arrjudgeno10numArr[9],$arrjudgeno10numArr[8],$arrjudgeno10numArr[7],$arrjudgeno10numArr[6],$arrjudgeno10numArr[5],$arrjudgeno10numArr[4],$arrjudgeno10numArr[3],$arrjudgeno10numArr[2],$arrjudgeno10numArr[1],$arrjudgeno10numArr[0]);
        $courtArr=array_merge($courtArr,array('09A1'=>$arrjudgeno10nameArrsep));
        $courtArr=array_merge($courtArr,array('09A2'=>$arrjudgeno10numArrsep));

        //获取审理案件数量top1法官审理案件判决案件、胜诉案件分布
        $no1gspanjueresultArr=$subjectExport->getno1gspanjueresult($this->rst);
        $no1yaxisArr=array($no1gspanjueresultArr[3]['name'],$no1gspanjueresultArr[2]['name'],$no1gspanjueresultArr[1]['name'],$no1gspanjueresultArr[0]['name']);
        $no1panjuecaseArr=array($no1gspanjueresultArr[3]['value'],$no1gspanjueresultArr[2]['value'],$no1gspanjueresultArr[1]['value'],$no1gspanjueresultArr[0]['value']);
        $no1shengsucaseArr=array($no1gspanjueresultArr[7]['value'],$no1gspanjueresultArr[6]['value'],$no1gspanjueresultArr[5]['value'],$no1gspanjueresultArr[4]['value']);
        $courtArr=array_merge($courtArr,array('10A1'=>$no1panjuecaseArr));
        $courtArr=array_merge($courtArr,array('10A2'=>$no1shengsucaseArr));
        $courtArr=array_merge($courtArr,array('10A3'=>$no1yaxisArr));
        $courtArr=array_merge($courtArr,array('10A4'=>$no1gspanjueresultArr[8]."(共审理案件".$no1gspanjueresultArr[11]."件)"));

        //获取审理案件数量top1法官审理案件攻方机构胜诉案件三级案由分布
        $no1GJGcasetypeArr=$basicExport->getno1GJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('10B'=>$no1GJGcasetypeArr));

        //获取审理案件数量top1法官审理案件攻方个人胜诉案件三级案由分布
        $no1GGRcasetypeArr=$basicExport->getno1GGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('10C'=>$no1GGRcasetypeArr));

        //获取审理案件数量top1法官审理案件守方机构胜诉案件三级案由分布
        $no1SJGcasetypeArr=$basicExport->getno1SJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('10D'=>$no1SJGcasetypeArr));

        //获取审理案件数量top1法官审理案件守方个人胜诉案件三级案由分布
        $no1SGRcasetypeArr=$basicExport->getno1SGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('10E'=>$no1SGRcasetypeArr));
        
         //获取审理案件数量top1法官审理案件判决案件、胜诉案件分布
        $no2gspanjueresultArr=$subjectExport->getno2gspanjueresult($this->rst);
        $no2yaxisArr=array($no2gspanjueresultArr[3]['name'],$no2gspanjueresultArr[2]['name'],$no2gspanjueresultArr[1]['name'],$no2gspanjueresultArr[0]['name']);
        $no2panjuecaseArr=array($no2gspanjueresultArr[3]['value'],$no2gspanjueresultArr[2]['value'],$no2gspanjueresultArr[1]['value'],$no2gspanjueresultArr[0]['value']);
        $no2shengsucaseArr=array($no2gspanjueresultArr[7]['value'],$no2gspanjueresultArr[6]['value'],$no2gspanjueresultArr[5]['value'],$no2gspanjueresultArr[4]['value']);
        $courtArr=array_merge($courtArr,array('11A1'=>$no2panjuecaseArr));
        $courtArr=array_merge($courtArr,array('11A2'=>$no2shengsucaseArr));
        $courtArr=array_merge($courtArr,array('11A3'=>$no2yaxisArr));
        $courtArr=array_merge($courtArr,array('11A4'=>$no2gspanjueresultArr[9]."(共审理案件".$no2gspanjueresultArr[12]."件)")); 

          //获取审理案件数量top2法官审理案件攻方机构胜诉案件三级案由分布
        $no2GJGcasetypeArr=$basicExport->getno2GJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('11B'=>$no2GJGcasetypeArr));

        //获取审理案件数量top2法官审理案件攻方个人胜诉案件三级案由分布
        $no2GGRcasetypeArr=$basicExport->getno2GGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('11C'=>$no2GGRcasetypeArr));

        //获取审理案件数量top2法官审理案件守方机构胜诉案件三级案由分布
        $no2SJGcasetypeArr=$basicExport->getno2SJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('11D'=>$no2SJGcasetypeArr));

        //获取审理案件数量top2法官审理案件守方个人胜诉案件三级案由分布
        $no2SGRcasetypeArr=$basicExport->getno2SGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('11E'=>$no2SGRcasetypeArr));

        //获取审理案件数量top3法官审理案件判决案件、胜诉案件分布
        $no3gspanjueresultArr=$subjectExport->getno3gspanjueresult($this->rst);
        $no3yaxisArr=array($no3gspanjueresultArr[3]['name'],$no3gspanjueresultArr[2]['name'],$no3gspanjueresultArr[1]['name'],$no3gspanjueresultArr[0]['name']);
        $no3panjuecaseArr=array($no3gspanjueresultArr[3]['value'],$no3gspanjueresultArr[2]['value'],$no3gspanjueresultArr[1]['value'],$no3gspanjueresultArr[0]['value']);
        $no3shengsucaseArr=array($no3gspanjueresultArr[7]['value'],$no3gspanjueresultArr[6]['value'],$no3gspanjueresultArr[5]['value'],$no3gspanjueresultArr[4]['value']);
        $courtArr=array_merge($courtArr,array('12A1'=>$no3panjuecaseArr));
        $courtArr=array_merge($courtArr,array('12A2'=>$no3shengsucaseArr));
        $courtArr=array_merge($courtArr,array('12A3'=>$no3yaxisArr));
        $courtArr=array_merge($courtArr,array('12A4'=>$no3gspanjueresultArr[10]."(共审理案件".$no3gspanjueresultArr[13]."件)")); 

        //获取审理案件数量top3法官审理案件攻方机构胜诉案件三级案由分布
        $no3GJGcasetypeArr=$basicExport->getno3GJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('12B'=>$no3GJGcasetypeArr));

        //获取审理案件数量top3法官审理案件攻方个人胜诉案件三级案由分布
        $no3GGRcasetypeArr=$basicExport->getno3GGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('12C'=>$no3GGRcasetypeArr));

        //获取审理案件数量top3法官审理案件守方机构胜诉案件三级案由分布
        $no3SJGcasetypeArr=$basicExport->getno3SJGcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('12D'=>$no3SJGcasetypeArr));

        //获取审理案件数量top3法官审理案件守方个人胜诉案件三级案由分布
        $no3SGRcasetypeArr=$basicExport->getno3SGRcasetype($this->rst);
        $courtArr=array_merge($courtArr,array('12E'=>$no3SGRcasetypeArr));

        //获取机构当事人案件数量排行前五的判决案件和胜诉案件数量
        $arr1=$basicExport->getJGdsrno5($this->rst);
        $yaxisArr1=array($arr1[4]['name'].'（守方）',$arr1[4]['name'].'（攻方）',$arr1[3]['name'].'（守方）',$arr1[3]['name'].'（攻方）',$arr1[2]['name'].'（守方）',$arr1[2]['name'].'（攻方）',$arr1[1]['name'].'（守方）',$arr1[1]['name'].'（攻方）',$arr1[0]['name'].'（守方）',$arr1[0]['name'].'（攻方）');
        $panjuecaseArr1=array($arr1[9]['value'],$arr1[4]['value'],$arr1[8]['value'],$arr1[3]['value'],$arr1[7]['value'],$arr1[2]['value'],$arr1[6]['value'],$arr1[1]['value'],$arr1[5]['value'],$arr1[0]['value']);
        $sspanjuecaseArr1=array($arr1[19]['value'],$arr1[14]['value'],$arr1[18]['value'],$arr1[13]['value'],$arr1[17]['value'],$arr1[12]['value'],$arr1[16]['value'],$arr1[11]['value'],$arr1[15]['value'],$arr1[10]['value']);
		$courtArr=array_merge($courtArr,array('13A1'=>$panjuecaseArr1));
        $courtArr=array_merge($courtArr,array('13A2'=>$sspanjuecaseArr1));
        $courtArr=array_merge($courtArr,array('13A3'=>$yaxisArr1));

        //获取适用实体法整体分布
        $totallawArr=$basicExport->getlaw($this->rst);
        //获取适用法律整体分布
        $courtArr=array_merge($courtArr,array('14A'=>$totallawArr[0]));
        //获取适用最多的两部法律
        $courtArr=array_merge($courtArr,array('14B1'=>$totallawArr[3]));
        $courtArr=array_merge($courtArr,array('14B2'=>$totallawArr[9]));

        $courtArr=array_merge($courtArr,array('14C1'=>$totallawArr[4]));
        $courtArr=array_merge($courtArr,array('14C2'=>$totallawArr[10]));
        //获取适用司法解释整体分布
        $courtArr=array_merge($courtArr,array('15A'=>$totallawArr[1]));
        //获取适用最多的两部司法解释
        $courtArr=array_merge($courtArr,array('15B1'=>$totallawArr[5]));
        $courtArr=array_merge($courtArr,array('15B2'=>$totallawArr[11]));

        $courtArr=array_merge($courtArr,array('15C1'=>$totallawArr[6]));
        $courtArr=array_merge($courtArr,array('15C2'=>$totallawArr[12]));
        //获取适用行政法规整体分布
        $courtArr=array_merge($courtArr,array('16A'=>$totallawArr[2]));
        //获取适用最多的两部行政法规
        $courtArr=array_merge($courtArr,array('16B1'=>$totallawArr[7]));
        $courtArr=array_merge($courtArr,array('16B2'=>$totallawArr[13]));

        $courtArr=array_merge($courtArr,array('16C1'=>$totallawArr[8]));
        $courtArr=array_merge($courtArr,array('16C2'=>$totallawArr[14]));
        

       
		file_put_contents('../views/js/report_court.txt', var_export($courtArr,true));
		file_put_contents('../views/js/report_court.json', json_encode($courtArr));
		return json_encode($courtArr);
	}
	
}