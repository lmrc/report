<?php


class Report{
	public function __construct(){
		require_once('basicexport_case.class.php');
		require_once('subjectexport.class.php');

		
		try {
			//本地数据库
			$conn = new MongoClient();
			//大鹏数据库
			// $conn = new MongoClient('192.168.170.33:27017');
			// $db = $conn ->local;
			$db = $conn ->report;
			// $test = $db->tencent;
			$test = $db->test1;
			$arr=array();
			$this->rst=$test->find($arr);
		} catch (Exception $e) {
			file_put_contents('c:/dberror.log', $e,FILE_APPEND);	
		}
	}

	public function ajax(){
		header("Access-Control-Allow-Origin: *");
		// echo json_encode($_POST);  

		$arr = array(
			array('value'=>13,'name'=>'一审'),
			array('value'=>212,'name'=>'二审'),
			array('value'=>221,'name'=>'再审'),
			array('value'=>56,'name'=>'执行')
		);
		// print_r(json_encode($arr));
		echo json_encode($arr);
	}



public function caseReport(){
		
		$casename = '商业秘密诉讼';
		$casename1 = '北京市商业秘密诉讼';
		$wenshusource="中国裁判文书网";
		$scale="全国范围诉讼案件";
		$judgedate="2013年1月1日到2015年09月30日"; 
		$judgedate1="2013年1月1日到2015年09月30日";
		$sitescope="北京市";
		$casefield="商业秘密";
		$casecount="31件";
		$othersource="人民法院网、最高人民法院公报";
		$lastvisit="2015年10月15日";
		
		
		$subjectExport = new SubjectExport();
		$basicExport = new BasicExport();
		$caseArr = array();

		//获取基本信息
		$caseArr=array_merge($caseArr,array('casename'=>$casename));
		$caseArr=array_merge($caseArr,array('casename1'=>$casename1));
		$caseArr=array_merge($caseArr,array('wenshusource'=>$wenshusource));
		$caseArr=array_merge($caseArr,array('scale'=>$scale));
		$caseArr=array_merge($caseArr,array('judgedate'=>$judgedate));
		$caseArr=array_merge($caseArr,array('judgedate1'=>$judgedate));
		$caseArr=array_merge($caseArr,array('sitescope'=>$sitescope));
		$caseArr=array_merge($caseArr,array('casefield'=>$casefield));
		$caseArr=array_merge($caseArr,array('casecount'=>$casecount));
		$caseArr=array_merge($caseArr,array('othersource'=>$othersource));
		$caseArr=array_merge($caseArr,array('lastvisit'=>$lastvisit));

		// //获取层级分布
		$rankArr = $basicExport->getRank($this->rst);
		$caseArr=array_merge($caseArr,array('03A'=>$rankArr));

		// //获取地域分布
		$siteArr = $basicExport->getSite($this->rst);
		$caseArr=array_merge($caseArr,array('04A'=>$siteArr));

		//月度分布
		$monthArr = $basicExport->getMonth($this->rst);
		$caseArr=array_merge($caseArr,array('04B1'=>array_values($monthArr)));
		$caseArr=array_merge($caseArr,array('04B2'=>array_keys($monthArr)));
		

		//获取对方当事人
		$reverseArr = $basicExport->getGSdsrtype($this->rst);
		// print_r($reverseArr[0]);
		//个人客户
		$personReverseArr=$basicExport->isPerson($reverseArr[0]);
		$caseArr=array_merge($caseArr,array('05A'=>$personReverseArr));
		//机构客户
		$companyReverseArr=$basicExport->isCompany($reverseArr[0]);
		$caseArr=array_merge($caseArr,array('05B'=>$companyReverseArr));

		$personReverseArr1=$basicExport->isPerson($reverseArr[1]);
		$caseArr=array_merge($caseArr,array('05C'=>$personReverseArr1));
		//机构客户
		$companyReverseArr1=$basicExport->isCompany($reverseArr[1]);
		$caseArr=array_merge($caseArr,array('05D'=>$companyReverseArr1));

		$personReverseArr2=$basicExport->isPerson($reverseArr[2]);
		$caseArr=array_merge($caseArr,array('06A'=>$personReverseArr2));
		//机构客户
		$companyReverseArr2=$basicExport->isCompany($reverseArr[2]);
		$caseArr=array_merge($caseArr,array('06B'=>$companyReverseArr2));

		$personReverseArr3=$basicExport->isPerson($reverseArr[3]);
		$caseArr=array_merge($caseArr,array('06C'=>$personReverseArr3));
		//机构客户
		$companyReverseArr3=$basicExport->isCompany($reverseArr[3]);
		$caseArr=array_merge($caseArr,array('06D'=>$companyReverseArr3));

		//获取案件类型
		$casetypeArr = $basicExport->getCasetype($this->rst);
		$caseArr=array_merge($caseArr,array('07A'=>$casetypeArr));

		//获取民事细分案由
		$secondtagArr = $basicExport->getSecondtag($this->rst);
		$caseArr=array_merge($caseArr,array('07B'=>$secondtagArr['民事']));

		// //获取裁判结果1
		$doctypeArr = $basicExport->getDoctype($this->rst);
		$caseArr=array_merge($caseArr,array('08A'=>$doctypeArr));
		// //获取裁判结果
		$timedoctypeArr = $basicExport->gettimeDoctype($this->rst);
		$caseArr=array_merge($caseArr,array('08B'=>$timedoctypeArr));
		// $caseArr=array_merge($caseArr,array('08B'=>$timedoctypeArr));
		// //审理程序
		$Casetype_SecondtagArr = $basicExport->getCasetype_Secondtag($this->rst);
		$caseArr=array_merge($caseArr,array('09A'=>$Casetype_SecondtagArr[0]));
		$caseArr=array_merge($caseArr,array('09B'=>$Casetype_SecondtagArr[1]));
		$caseArr=array_merge($caseArr,array('09C'=>$Casetype_SecondtagArr[2]));
		$caseArr=array_merge($caseArr,array('09D'=>$Casetype_SecondtagArr[3]));
		$caseArr=array_merge($caseArr,array('09E'=>$Casetype_SecondtagArr[4]));

		// 获取裁判结果1
		$DoctypeResultArr = $basicExport->getDoctypeResult($this->rst);
		$caseArr=array_merge($caseArr,array('10A'=>$DoctypeResultArr));

		//获取裁判结果2
		$PanjueResultArr = $basicExport->getPanjueResult($this->rst);//函数名有问题
		$caseArr=array_merge($caseArr,array('10B'=>$PanjueResultArr));
		//获取裁判结果3
		$caidingResultArr = $basicExport->getCaidingResult($this->rst);
		$caseArr=array_merge($caseArr,array('10C'=>$caidingResultArr));


	// /判决案件案由
		$PanjueSecondtagArr = $basicExport->getPanjuesecondtag($this->rst);
		$caseArr=array_merge($caseArr,array('11A'=>$PanjueSecondtagArr[0]));
		$caseArr=array_merge($caseArr,array('11B'=>$PanjueSecondtagArr[1]));
		$caseArr=array_merge($caseArr,array('11C'=>$PanjueSecondtagArr[2]));
		$caseArr=array_merge($caseArr,array('11D'=>$PanjueSecondtagArr[3]));
		$caseArr=array_merge($caseArr,array('11E'=>$PanjueSecondtagArr[4]));
		$caseArr=array_merge($caseArr,array('11F'=>$PanjueSecondtagArr[5]));


		// 案件金额分布
		$MoneyArr = $basicExport->getmoney($this->rst);
		$caseArr=array_merge($caseArr,array('12A'=>$MoneyArr));



		// 案件法律法规的抓取
		$LawArr = $basicExport->getlaw($this->rst);
		$caseArr=array_merge($caseArr,array('15A'=>$LawArr[0]));
		$caseArr=array_merge($caseArr,array('16A'=>$LawArr[1]));
		$caseArr=array_merge($caseArr,array('17A'=>$LawArr[2]));
		$caseArr=array_merge($caseArr,array('15B'=>$LawArr[3]));
		$caseArr=array_merge($caseArr,array('15C'=>$LawArr[4]));
		$caseArr=array_merge($caseArr,array('16B'=>$LawArr[5]));
		$caseArr=array_merge($caseArr,array('16C'=>$LawArr[6]));
		$caseArr=array_merge($caseArr,array('17B'=>$LawArr[7]));
		$caseArr=array_merge($caseArr,array('17C'=>$LawArr[8]));

		$caseArr=array_merge($caseArr,array('15B1'=>$LawArr[9]));
		$caseArr=array_merge($caseArr,array('15C1'=>$LawArr[10]));
		$caseArr=array_merge($caseArr,array('16B1'=>$LawArr[11]));
		$caseArr=array_merge($caseArr,array('16C1'=>$LawArr[12]));
		$caseArr=array_merge($caseArr,array('17B1'=>$LawArr[13]));
		$caseArr=array_merge($caseArr,array('17C1'=>$LawArr[14]));



		$LawArr = $basicExport->getlaw($this->rst);
		$caseArr=array_merge($caseArr,array('16A'=>$LawArr[1]));




		$LawArr = $basicExport->getlaw($this->rst);
		$caseArr=array_merge($caseArr,array('17A'=>$LawArr[2]));








		// $siteArr = $basicExport->getcompanycourtname($this->rst);
		// $caseArr=array_merge($caseArr,array('07A1'=>$siteArr[0]));
		// $caseArr=array_merge($caseArr,array('07A2'=>$siteArr[3]));
		// $caseArr=array_merge($caseArr,array('07B1'=>$siteArr[1]));
		// $caseArr=array_merge($caseArr,array('07B2'=>$siteArr[4]));
		// $caseArr=array_merge($caseArr,array('07C1'=>$siteArr[2]));
		// $caseArr=array_merge($caseArr,array('07C2'=>$siteArr[5]));
		
		// // //获取诉讼地位1
		// $statusArr = $basicExport->getcompanyStatus($this->rst,$companyname);
		// $statusArr1=array(
		// 		array('name'=>'攻方','value'=>array_sum($statusArr[0])),
		// 		array('name'=>'守方','value'=>array_sum($statusArr[1])),
		// 		array('name'=>'第三方','value'=>array_sum($statusArr[2]))
		// 	);
		// $caseArr=array_merge($caseArr,array('08A'=>$statusArr1));

		// //获取不同审级攻守的二级案由
		// $levelsecondArr = $basicExport->getlevelsecondtag($this->rst,$companyname);S
		// $caseArr=array_merge($caseArr,array('09A'=>$levelsecondArr[0]));
		// $caseArr=array_merge($caseArr,array('09B'=>$levelsecondArr[1]));
		// $caseArr=array_merge($caseArr,array('09C'=>$levelsecondArr[2]));
		// $caseArr=array_merge($caseArr,array('09D'=>$levelsecondArr[3]));
		// $caseArr=array_merge($caseArr,array('09E'=>$levelsecondArr[4]));
		// $caseArr=array_merge($caseArr,array('09F'=>$levelsecondArr[5]));



		// //获取判决结案案件分析1
		// $ResultArr = $basicExport->getGognShouPanjueResult($this->rst,$companyname);

		// $ResultArr1=array(
		// 		array('name'=>'支持攻方全部诉请','value'=>($ResultArr[0]['支持攻方全部诉请'])),
		// 		array('name'=>'支持攻方部分诉请','value'=>($ResultArr[0]['支持攻方部分诉请'])),
		// 		array('name'=>'驳回攻方全部诉请','value'=>($ResultArr[0]['驳回攻方全部诉请']))
		// 	);
		// $ResultArr2=array(
		// 		array('name'=>'支持攻方全部诉请','value'=>($ResultArr[1]['支持攻方全部诉请'])),
		// 		array('name'=>'支持攻方部分诉请','value'=>($ResultArr[1]['支持攻方部分诉请'])),
		// 		array('name'=>'驳回攻方全部诉请','value'=>($ResultArr[1]['驳回攻方全部诉请']))
		// 	);

		// $ResultArr3=array(
		// 		array('name'=>'支持攻方全部诉请','value'=>($ResultArr[2]['支持攻方全部诉请'])),
		// 		array('name'=>'支持攻方部分诉请','value'=>($ResultArr[2]['支持攻方部分诉请'])),
		// 		array('name'=>'驳回攻方全部诉请','value'=>($ResultArr[2]['驳回攻方全部诉请']))
		// 	);
		// $ResultArr4=array(
		// 		array('name'=>'支持攻方全部诉请','value'=>($ResultArr[3]['支持攻方全部诉请'])),
		// 		array('name'=>'支持攻方部分诉请','value'=>($ResultArr[3]['支持攻方部分诉请'])),
		// 		array('name'=>'驳回攻方全部诉请','value'=>($ResultArr[3]['驳回攻方全部诉请']))
		// 	);
		// $caseArr=array_merge($caseArr,array('11A'=>$ResultArr1));
		// $caseArr=array_merge($caseArr,array('11B'=>$ResultArr2));
		// $caseArr=array_merge($caseArr,array('11C'=>$ResultArr3));
		// $caseArr=array_merge($caseArr,array('11D'=>$ResultArr4));

		// //获取前三名法院案由
		// $LawfirmsecondtagArr = $basicExport->getLawfirmsecondtag($this->rst,$companyname);
		// $caseArr=array_merge($caseArr,array('12A1'=>$LawfirmsecondtagArr[0]));
		// $caseArr=array_merge($caseArr,array('12A2'=>$LawfirmsecondtagArr[3]));
		// $caseArr=array_merge($caseArr,array('12B1'=>$LawfirmsecondtagArr[1]));
		// $caseArr=array_merge($caseArr,array('12B2'=>$LawfirmsecondtagArr[4]));
		// $caseArr=array_merge($caseArr,array('12C1'=>$LawfirmsecondtagArr[2]));
		// $caseArr=array_merge($caseArr,array('12C2'=>$LawfirmsecondtagArr[5]));
		

		// //获取对方当事人
		// $reverseArr = $subjectExport->getcompanyReverseParty($this->rst,$companyname);
		// $personReverseArr=$subjectExport->isPerson($reverseArr);
		// //个人客户
		// $caseArr=array_merge($caseArr,array('13A'=>$personReverseArr));
		// $companyReverseArr=$subjectExport->isCompany($reverseArr);
		// //机构客户
		// $caseArr=array_merge($caseArr,array('13B'=>$companyReverseArr));
		
		// //获取前三名对方当事人案由
		// $Opposing_partysecondtagArr = $basicExport->getOpposing_partysecongtag($this->rst,$companyname);
		// $caseArr=array_merge($caseArr,array('14A1'=>$Opposing_partysecondtagArr[0]));
		// $caseArr=array_merge($caseArr,array('14A2'=>$Opposing_partysecondtagArr[3]));
		// $caseArr=array_merge($caseArr,array('14B1'=>$Opposing_partysecondtagArr[1]));
		// $caseArr=array_merge($caseArr,array('14B2'=>$Opposing_partysecondtagArr[4]));
		// $caseArr=array_merge($caseArr,array('14C1'=>$Opposing_partysecondtagArr[2]));
		// $caseArr=array_merge($caseArr,array('14C2'=>$Opposing_partysecondtagArr[5]));
		
		file_put_contents('../views/js/report_case.txt', var_export($caseArr,true));
		file_put_contents('../views/js/report_case.json', json_encode($caseArr));
		return json_encode($caseArr);
	}
}