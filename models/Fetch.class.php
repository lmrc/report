<?php

/**
* 
*/
class Fetch
{
	
	function __construct()
	{
		
	}

	public function fetchCase(){
		@session_start();
		$subType=$_SESSION['subType'];
		$name=$_SESSION['subName'];
		$subName=urlencode($name);
		$office=$_SESSION['subOffice'];
		$subOffice=urlencode($office);
		$beginDate=$_SESSION['beginDate'];
		$finishDate=$_SESSION['finishDate'];

		switch ($subType) {
			case 'lawyer':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer='.$subName.'&office='.$subOffice.'&judge=&litigant=&courtCode=&title=&proceedings=&fact=&courtConsiders=&result=';
				break;
			case 'lawfirm':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer=&office='.$subName.'&judge=&litigant=&courtCode=&title=&proceedings=&fact=&courtConsiders=&result=';
				break;
			case 'judge':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer=&office=&judge='.$subName.'&litigant=&courtCode='.$subOffice.'&title=&proceedings=&fact=&courtConsiders=&result=';
				break;
			case 'court':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer=&office=&judge=&litigant=&courtCode='.$subName.'&title=&proceedings=&fact=&courtConsiders=&result=';
				break;
			case 'company':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer=&office=&judge=&litigant='.$subName.'&courtCode=&title=&proceedings=&fact=&courtConsiders=&result=';
				break;
			case 'case':
				$url='http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$beginDate.'&endDate='.$finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer=&office=&judge=&litigant=&courtCode=&title='.$subName.'&proceedings=&fact=&courtConsiders=&result=';
				break;
			default:
				# code...
				break;
		}

		$header = array();
	    $header[] = 'Authorization: Basic dGVzdGxlZ2FsOmxlZ2FsdGVzdDE3ODk=';
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36");
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
	    
	    $response = curl_exec($ch);

	    if(curl_errno($ch)){
	    	file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).'Curl error: '.curl_error($ch),FILE_APPEND);
	    }

	    curl_close($ch);

    	$data=gzdecode($response);
		
	    $res=json_decode($data,true);
	    // var_dump($res);
	    // file_put_contents('c:/song.txt', var_export($res,true));
	    return $res;
	}


	public function writeCase($case){
		// $dbName=$_SESSION['dbName'];
		$dbName='report';
		$collName=$_SESSION['collName'];
		$subName=$_SESSION['subName'];

		try {
			$conn = new MongoClient();
			$db = $conn ->$dbName;
			$test = $db->$collName;
    		
		} catch (Exception $e) {
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).$e,FILE_APPEND);			
		}

		$pp=0;
		foreach ($case as $key => $val) {
			$pp++;
			$arr=array(
				'subName'=>$subName,
				'title'=>$val['title'],
				'court'=>$val['court'],
				'wenshu'=>$val['wenshu'],
				'casenum'=>$val['casenum'],
				'time'=>$val['time'],
				'subject'=>$val['subject'],
				'procedure'=>$val['procedure'],
				'subian'=>$val['subian'],
				'fact'=>$val['fact'],
				'reason'=>$val['reason'],
				'result'=>$val['result'],
				'judge'=>$val['judge'],
				'writer'=>$val['writer'],
				'doc'=>$val['doc'],
				"ID"=>$val['id']
				);
			if ($test->insert($arr)) {
				file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time())." 第".$pp."条数据插入成功",FILE_APPEND);
			} else {
				file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time())." 第".$pp."条数据插入失败",FILE_APPEND);
			}
		}
	}

	public function getCase(){
		@session_start();
		// $dbName=$_SESSION['dbName'];
		$dbName='report';
		$collName=$_SESSION['collName'];

		try {
			$conn = new MongoClient();
			$db = $conn->$dbName;
			$test = $db->$collName;
			$arr=array();
			$rst=$test->find($arr);
			$case=array();
			foreach ($rst as $key => $value) {
				$case[]=$value;
			}
		} catch (Exception $e) {
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).$e,FILE_APPEND);			
		}

		return $case;

		
	}

	public function updateCase($case){
		@session_start();
		// $dbName=$_SESSION['dbName'];
		$dbName='report';
		$collName=$_SESSION['collName'];

		try {
			$conn = new MongoClient();
			$db = $conn ->$dbName;
			$test = $db->$collName;
    		
		} catch (Exception $e) {
			file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time()).$e,FILE_APPEND);			
		}

		$pp=0;
		foreach ($case as $key => $val) {
			$pp++;
			//上传数据库
			$sarr=array('_id'=>$val['_id']);
			$darr=array('$set'=>array(
				'date'=>$val['date'],
				'month'=>$val['month'],
				'site'=>$val['site'],
				'doctype'=>$val['doctype'],
				'casetype'=>$val['casetype'],
				'level'=>$val['level'],
				'rank'=>$val['rank'],
				'secondtag'=>$val['secondtag'],
				'outcome'=>$val['outcome'],
				'winner'=>$val['winner'],
				'party'=>$val['party'],
				'agent'=>$val['agent'],
				'justice'=>$val['justice'],
				'status'=>$val['status']
				));
			$opts=array(false,true);

			if ($test->update($sarr,$darr,$opts)) {
				file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time())." 第".$pp."条数据更新成功",FILE_APPEND);
			} else {
				file_put_contents('../logs/report.log', "\n".date('Y-m-d H:i:s',time())." 第".$pp."条数据更新失败",FILE_APPEND);
			}
		}
	}
}