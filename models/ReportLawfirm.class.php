<?php 

/**
* 
*/
class ReportLawfirm
{
	
	function __construct()
	{

	}

	//遍历全部记录获取律所客户的数据
	function getLawfirmClient($rst,$name){

		$clientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministr=array();
				foreach ($val1['mandatary'] as $key2 => $val2) {
					// if ($val2['type']=='lawyer'&&in_array($val2['office'],$name)!==false) {
					if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
						if (in_array($val1['name'], $ministr)==false) {
							$ministr=array_merge($ministr,array($val1['name']));
						}
					}
				}
				$clientArr=array_merge($clientArr,$ministr);
			}

		}
		// print_r($clientArr);
		// echo '<hr>';

		//己方当事人名单去重
		// $clientUniqueRaw=array_unique($clientArr,SORT_STRING);
		// foreach ($clientUniqueRaw as $key => $value) {
		// 	$clientUnique[]=$value;
		// }
		// print_r($clientUnique);

		//己方当事人次数统计
		$clientCountValues=array_count_values($clientArr);
		// print_r($clientCountValues);

		//案件数量倒序排列
		arsort($clientCountValues);
		// print_r($clientCountValues);
		return $clientCountValues;
	}


	//遍历全部记录获取律所对方当事人（客户）的数据
	function getReverseClient($rst,$name){

		$reverseClientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $k => $v) {
				$ministr=array();
				foreach ($v['mandatary'] as $key0 => $val0) {
					if ($val0['type']=='lawyer'&&in_array($val0['office'], $name)){
						if ($v['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									// echo $val1['name'];
									if (in_array($val1['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val1['name']));
										// print_r($ministr);
									}
								}
							}
						}else if($v['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									// echo $val2['name'];
									if (in_array($val2['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val2['name']));
										// print_r($ministr);
									}
								}
							}	
						}else if($v['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									// echo $val3['name'];
									if (in_array($val3['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val3['name']));
										// print_r($ministr);
									}
				  				}
							}
			        	}else if($v['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									// echo $val4['name'];
									if (in_array($val4['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val4['name']));
										// print_r($ministr);
									}
								}	
							}
			 			}else if($v['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									// echo $val5['name'];
									if (in_array($val5['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val5['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									// echo $val6['name'];
									if (in_array($val6['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val6['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									// echo $val7['name'];
									if (in_array($val7['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val7['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									// echo $val8['name'];
									if (in_array($val8['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val8['name']));
										// print_r($ministr);
									}
								}	
							}
						}
					
						print_r($ministr);
					}
					
				}
				$reverseClientArr=array_merge($reverseClientArr,$ministr);
			}
		}
		// print_r($reverseClientArr);
		// echo '<hr>';

		// //名单去重
		// $reverseClientUniqueRaw=array_unique($reverseClientArr,SORT_STRING);
		// foreach ($reverseClientUniqueRaw as $key => $value) {
		// 	$reverseClientUnique[]=$value;
		// }

		//次数统计
		$reverseClientCountValues=array_count_values($reverseClientArr);

		//倒序排列
		arsort($reverseClientCountValues);


		// print_r($reverseClientCountValues);

		return $reverseClientCountValues;
	}

//**************************************法院报告中攻方当事人类型判断(用$value而不是$key，区别于律师报告)
    function getGdsrtype($rst){
   
    $GdsrArr=array();

		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrG=array();
				$ministrS=array();
				if ($val1['type']=='plaintiff'){
			    $ministrG=array_merge($ministrS,array($val1['name']));
				$GdsrArr=array_merge($GdsrArr,$ministrG);
                }
			 }
		}

		//攻方当事人名单去重
		$GdsrUniqueRaw=array_unique($GdsrArr);
		foreach ($GdsrUniqueRaw as $key => $value) {
		$GdsrUnique[]=$value;
		}

		

		$arr=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($GdsrUniqueRaw as $key => $value) {
         $value=preg_replace('/（.*?）/', "", $value);
			if (strlen($value)>9) {
				$arr[0]['value']++;
			}else{
				$arr[1]['value']++;
			}
		}
		return $arr;
    }

	//**************************************【9】遍历全部记录获取对手律所的数据
	function getReverseAgent($rst,$name){

		$reverseAgentArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			$ministr=array();
			foreach ($agent as $k1 => $v1) {
				if ($v1['type']=='lawyer'&&in_array($v1['office'], $name)) {  

					foreach ($agent as $k2 => $v2) {
						if ($v1['client'][0]['type']=='plaintiff'&&$v2['client'][0]['type']=='defendant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
						}else if ($v1['client'][0]['type']=='defendant'&&$v2['client'][0]['type']=='plaintiff') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellant'&&$v2['client'][0]['type']=='appellee') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellee'&&$v2['client'][0]['type']=='appellant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='proposer'&&$v2['client'][0]['type']=='respondent') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='respondent'&&$v2['client'][0]['type']=='proposer') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEr'&&$v2['client'][0]['type']=='exEd') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEd'&&$v2['client'][0]['type']=='exEr') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}
					}
					
				}
			}
			$reverseAgentArr=array_merge($reverseAgentArr,$ministr);
		}
		// print_r($reverseAgentArr);
		// echo '<hr>';

		//名单去重
		// $reverseAgentUniqueRaw=array_unique($reverseAgentArr,SORT_STRING);
		// foreach ($reverseAgentUniqueRaw as $key => $value) {
		// 	$reverseAgentUnique[]=$value;
		// }

		//次数统计
		$reverseAgentCountValues=array_count_values($reverseAgentArr);

		//倒序排列
		arsort($reverseAgentCountValues);

		
		print_r($reverseAgentCountValues);

		return $reverseAgentCountValues;
	}


//***************************************遍历全部记录获取律所代理客户诉讼地位
function getlawfirmStatus($rst,$name){
	$gong=array();
	$shou=array();
	$third=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$party=$val['party'];
		foreach ($party as $key1 => $val1) {
			foreach ($val1['mandatary'] as $key2 => $val2) {
				if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
					if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
						$gong=array_merge($gong, array($val1['type']));
						break;
					} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
						$shou=array_merge($shou, array($val1['type']));
					    break;
					} else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
						$third=array_merge($third, array($val1['type']));
					    break;
					}
				}
				// }
			}
		}
	}

	$gongArr=array_count_values($gong);
	if (array_key_exists('plaintiff', $gongArr)==false) {
		$gongArr['plaintiff']=0;
	}
	if (array_key_exists('appellant', $gongArr)==false) {
		$gongArr['appellant']=0;
	}
	if (array_key_exists('proposer', $gongArr)==false) {
		$gongArr['proposer']=0;
	}
	
	$shouArr=array_count_values($shou);
	if (array_key_exists('defendant', $shouArr)==false) {
		$shouArr['defendant']=0;
	}
	if (array_key_exists('appellee', $shouArr)==false) {
		$shouArr['appellee']=0;
	}
	if (array_key_exists('respondent', $shouArr)==false) {
		$shouArr['respondent']=0;
	}

	$thirdArr=array_count_values($third);
	if (array_key_exists('thirdOne', $thirdArr)==false) {
		$thirdArr['thirdOne']=0;
	}
	if (array_key_exists('thirdTwo', $thirdArr)==false) {
		$thirdArr['thirdTwo']=0;
	}
	if (array_key_exists('thirdThree', $thirdArr)==false) {
		$thirdArr['thirdThree']=0;
	}


	$arr=array($gongArr,$shouArr,$thirdArr);
	// file_put_contents('c:/test.log', var_export($arr,true));
	return $arr;
}
//***************************************获取攻方客户审级分布
	function getlawfirmgongStatuslevel($rst,$name){
	$gong=array();
	$shou=array();
	$third=array();
    foreach ($rst as $val) {
		$fid=$val['_id'];
		$party=$val['party'];
		foreach ($party as $key1 => $val1) {
			foreach ($val1['mandatary'] as $key2 => $val2) {
				if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
					if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
						$gong=array_merge($gong, array($val1['type']));
						break;
					} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
						$shou=array_merge($shou, array($val1['type']));
					    break;
					} else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
						$third=array_merge($third, array($val1['type']));
					    break;
					}
				}
				// }
			}
		}
	}

	$gongArr=array_count_values($gong);
	if (array_key_exists('plaintiff', $gongArr)==false) {
		$gongArr['plaintiff']=0;
	}
	if (array_key_exists('appellant', $gongArr)==false) {
		$gongArr['appellant']=0;
	}
	if (array_key_exists('proposer', $gongArr)==false) {
		$gongArr['proposer']=0;
	}
	
	$shouArr=array_count_values($shou);
	if (array_key_exists('defendant', $shouArr)==false) {
		$shouArr['defendant']=0;
	}
	if (array_key_exists('appellee', $shouArr)==false) {
		$shouArr['appellee']=0;
	}
	if (array_key_exists('respondent', $shouArr)==false) {
		$shouArr['respondent']=0;
	}

	$thirdArr=array_count_values($third);
	if (array_key_exists('thirdOne', $thirdArr)==false) {
		$thirdArr['thirdOne']=0;
	}
	if (array_key_exists('thirdTwo', $thirdArr)==false) {
		$thirdArr['thirdTwo']=0;
	}
	if (array_key_exists('thirdThree', $thirdArr)==false) {
		$thirdArr['thirdThree']=0;
	}

	foreach ($gongArr as $key1 => $value1) {
    		if ($key1=="appellant") {
    		$arrgonglevel1=array('value'=>$value1,'name'=>"二审上诉人");
    		}// 
    		else if ($key1=="plaintiff") {
    		$arrgonglevel2=array('value'=>$value1,'name'=>"一审原告");
    	    }	
    		else if ($key1=="proposer") {
    		$arrgonglevel3=array('value'=>$value1,'name'=>"再审申请人");
        }
    }

    $arr=array($arrgonglevel1,$arrgonglevel2,$arrgonglevel3);
    return $arr;
}
//***************************************获取守方客户审级分布
    function getlawfirmshouStatuslevel($rst,$name){
	$gong=array();
	$shou=array();
	$third=array();
    foreach ($rst as $val) {
		$fid=$val['_id'];
		$party=$val['party'];
		foreach ($party as $key1 => $val1) {
			foreach ($val1['mandatary'] as $key2 => $val2) {
				if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
					if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
						$gong=array_merge($gong, array($val1['type']));
						break;
					} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
						$shou=array_merge($shou, array($val1['type']));
					    break;
					} else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
						$third=array_merge($third, array($val1['type']));
					    break;
					}
				}
			}
		}
	}

	$gongArr=array_count_values($gong);
	if (array_key_exists('plaintiff', $gongArr)==false) {
		$gongArr['plaintiff']=0;
	}
	if (array_key_exists('appellant', $gongArr)==false) {
		$gongArr['appellant']=0;
	}
	if (array_key_exists('proposer', $gongArr)==false) {
		$gongArr['proposer']=0;
	}
	
	$shouArr=array_count_values($shou);
	if (array_key_exists('defendant', $shouArr)==false) {
		$shouArr['defendant']=0;
	}
	if (array_key_exists('appellee', $shouArr)==false) {
		$shouArr['appellee']=0;
	}
	if (array_key_exists('respondent', $shouArr)==false) {
		$shouArr['respondent']=0;
	}

	$thirdArr=array_count_values($third);
	if (array_key_exists('thirdOne', $thirdArr)==false) {
		$thirdArr['thirdOne']=0;
	}
	if (array_key_exists('thirdTwo', $thirdArr)==false) {
		$thirdArr['thirdTwo']=0;
	}
	if (array_key_exists('thirdThree', $thirdArr)==false) {
		$thirdArr['thirdThree']=0;
	}

	foreach ($shouArr as $key1 => $value1) {
    		if ($key1=="appellee") {
    		$arrshoulevel1=array('value'=>$value1,'name'=>"二审被上诉人");
    		}// 
    		else if ($key1=="defendant") {
    		$arrshoulevel2=array('value'=>$value1,'name'=>"一审被告");
    	    }	
    		else if ($key1=="respondent") {
    		$arrshoulevel3=array('value'=>$value1,'name'=>"再审被申请人");
        }
    }

    $arr=array($arrshoulevel1,$arrshoulevel2,$arrshoulevel3);
    return $arr;
  }
//***************************************获取判决案件客户诉讼地位分布
  	function getpanjueJudgeStatus($rst,$name){
		$gong=array();
		$shou=array();
		$third=array();

		foreach ($rst as $val) {
			$fid=$val['_id'];
			$doctype=$val['doctype'];
			$party=$val['party'];
			if ($doctype=='判决'){
				foreach ($party as $key1 => $val1) {
					foreach ($val1['mandatary'] as $key2 => $val2) {
						if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
							if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
								$gong=array_merge($gong, array($val1['type']));
								break;
							} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
								$shou=array_merge($shou, array($val1['type']));
								break;
							} else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
								$third=array_merge($third, array($val1['type']));
								break;
							}
						}
					}
				}
			}

		}

		$gongArr=array_count_values($gong);
		if (array_key_exists('plaintiff', $gongArr)==false) {
			$gongArr['plaintiff']=0;
		}
		if (array_key_exists('appellant', $gongArr)==false) {
			$gongArr['appellant']=0;
		}
		if (array_key_exists('proposer', $gongArr)==false) {
			$gongArr['proposer']=0;
		}
		
		$shouArr=array_count_values($shou);
		if (array_key_exists('defendant', $shouArr)==false) {
			$shouArr['defendant']=0;
		}
		if (array_key_exists('appellee', $shouArr)==false) {
			$shouArr['appellee']=0;
		}
		if (array_key_exists('respondent', $shouArr)==false) {
			$shouArr['respondent']=0;
		}

		$thirdArr=array_count_values($third);
		if (array_key_exists('thirdOne', $thirdArr)==false) {
			$thirdArr['thirdOne']=0;
		}
		if (array_key_exists('thirdTwo', $thirdArr)==false) {
			$thirdArr['thirdTwo']=0;
		}
		if (array_key_exists('thirdThree', $thirdArr)==false) {
			$thirdArr['thirdThree']=0;
		}

		$arr=array($gongArr,$shouArr,$thirdArr);
		// file_put_contents('c:/test.log', var_export($arr,true));
		return $arr;

	}


//***************************************获取判决一审攻守裁判结果
function getlevel1PanjueResult($rst,$name){
	$level1gongArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	$level1shouArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	// $third=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['outcome'];
		$party=$val['party'];
		$level=$val['level'];
		$name="方达";
		if ($doctype=='判决') {
			if ($level=="一审") {
				foreach ($party as $key1 => $val1) {
					foreach ($val1['mandatary'] as $key2 => $val2) {
						if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
							if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level1gongArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level1gongArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level1gongArr['驳回攻方全部诉请']++;
									break;
								}
								
							} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level1shouArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level1shouArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level1shouArr['驳回攻方全部诉请']++;
									break;
								}
								
							} 
							// else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
							// 	$third=array_merge($third, array($result));
								
							// }
						}
					// }
				}
			}
		}
	}
	// print_r($rankArr);
  }
  $arr=array($level1gongArr,$level1shouArr);
  // file_put_contents('c:/test.log', var_export($arr,true));
  return $arr;

}
//==================================================获取判决二审攻守裁判结果
function getlevel2PanjueResult($rst,$name){
	$level2gongArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	$level2shouArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	// $third=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['outcome'];
		$party=$val['party'];
		$level=$val['level'];
		$name="方达";
		if ($doctype=='判决') {
			if ($level=="二审") {
				foreach ($party as $key1 => $val1) {
					foreach ($val1['mandatary'] as $key2 => $val2) {
						if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
							if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level2gongArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level2gongArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level2gongArr['驳回攻方全部诉请']++;
									break;
								}
								
							} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level2shouArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level2shouArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level2shouArr['驳回攻方全部诉请']++;
									break;
								}
								
							} 
							// else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
							// 	$third=array_merge($third, array($result));
								
							// }
						}
					// }
				}
			}
		}
	}
	// print_r($rankArr);
  }
  $arr=array($level2gongArr,$level2shouArr);
  // file_put_contents('c:/test.log', var_export($arr,true));
  return $arr;

}
//==================================================获取判决再审攻守裁判结果
function getlevel3PanjueResult($rst,$name){
	$level3gongArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	$level3shouArr=array('支持攻方全部诉请'=>0,'支持攻方部分诉请'=>0,'驳回攻方全部诉请'=>0);
	// $third=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['outcome'];
		$party=$val['party'];
		$level=$val['level'];
		$name="方达";
		if ($doctype=='判决') {
			if ($level=="再审") {
				foreach ($party as $key1 => $val1) {
					foreach ($val1['mandatary'] as $key2 => $val2) {
						if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
							if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level3gongArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level3gongArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level3gongArr['驳回攻方全部诉请']++;
									break;
								}
								
							} else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
								if (strpos($result, '支持攻方全部')!==false) {
									$level3shouArr['支持攻方全部诉请']++;
									break;
								} else if (strpos($result, '部分')!==false) {
									$level3shouArr['支持攻方部分诉请']++;
									break;
								} else if (strpos($result, '驳回')!==false) {
									$level3shouArr['驳回攻方全部诉请']++;
									break;
								}
								
							} 
							// else if ($val1['type']=='thirdOne'||$val1['type']=='thirdTwo'||$val1['type']=='thirdThree') {
							// 	$third=array_merge($third, array($result));
								
							// }
						}
					// }
				}
			}
		}
	  }
	// print_r($rankArr);
    }
  $arr=array($level3gongArr,$level3shouArr);
  // file_put_contents('c:/test.log', var_export($arr,true));
  return $arr;

  }
	//===============================================获取代理案件数量最多的三名律师及其代理案件案由
    function getthreelawyernum($rst,$name){
	    $Totalarray=array();
		$lawyernum=array();
		$secondtagnum=array();
		$secondtagArr1=array();
		$secondtagArr2=array();
		$secondtagArr3=array();
		foreach ($rst as $key => $val) {
		$lawyerArr=array();
		$fid=$val['_id'];
		$party=$val['party'];
		$agent=$val['agent'];
		$secondtag=$val['secondtag'];

		foreach ($party as $key1 => $val1) {
				foreach ($val1['mandatary'] as $key2 => $val2) {
					if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
							$lawyerArr=array_merge($lawyerArr, array($val2['name']=>$secondtag));
						}
					}
				
			}
		// $lawyerArr1 =array_unique($lawyerArr); 找代理律师应该不去重	
		$Totalarray=array_merge_recursive($Totalarray, $lawyerArr);
		}

		$W=array();	
		foreach ($Totalarray as $k3 => $v3) {
			$Q=sizeof($v3);
			$W[$k3]=$Q;
		}
		arsort($W);
		$E=array_keys($W);
		foreach ($Totalarray as $k4 => $v4) {
			if ($E[0]==$k4) {
				$secondtagArr1=array_merge($secondtagArr1, $v4);
			}else if ($E[1]==$k4) {
				$secondtagArr2=array_merge($secondtagArr2, $v4);
			}else if ($E[2]==$k4) {
				$secondtagArr3=array_merge($secondtagArr3, $v4);
			}	
		}
		$lawyersecondtagArr1=array_count_values($secondtagArr1);
		$lawyersecondtagArr2=array_count_values($secondtagArr2);
		$lawyersecondtagArr3=array_count_values($secondtagArr3);

		arsort($lawyersecondtagArr1);
		arsort($lawyersecondtagArr2);
		arsort($lawyersecondtagArr3);
		$arr1=array();
		$arr2=array();
		$arr3=array();
		$i=$j=$k=0;
		foreach ($lawyersecondtagArr1 as $k1 => $v1) {
			if (empty($v1)) {
				array_push($arr1, array('value'=>0,'name'=>$k1));
			}
			array_push($arr1, array('value'=>$v1,'name'=>$k1));
			$i=$i+$v1;
			}
		$arr2=array();
		foreach ($lawyersecondtagArr2 as $k2 => $v2) {
			if (empty($v2)) {
				array_push($arr2, array('value'=>0,'name'=>$k2));
			}
			array_push($arr2, array('value'=>$v2,'name'=>$k2));
			$j=$j+$v2;
			}
		$arr3=array();
		foreach ($lawyersecondtagArr3 as $k3 => $v3) {
			if (empty($v3)) {
				array_push($arr3, array('value'=>0,'name'=>$k3));
			}
			array_push($arr3, array('value'=>$v3,'name'=>$k3));
			$k=$k+$v3;
			}
		$arr=array($arr1,$arr2,$arr3,$E[0]."(".$i."件)",$E[1]."(".$j."件)",$E[2]."(".$k."件)");
		// $arr=array($arr1,$arr2,$arr3,$E[0],$E[1],$E[2]);
		return ($arr) ;
  	}


}



