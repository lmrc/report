<?php 

define('BASE_URL', "C:/Myweb/Apache/htdocs/mydata");

define('FILE_LOC', 'C:/mydata');
/**
* 
*/
class BasicExport
{

function __construct()
{
	# code...
}


//***************************************获取案件类型
function getCasetype($rst){
	$casetypeArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$casetype=$val['casetype'];
		array_push($casetypeArr, $casetype);
	}
	// print_r($rankArr);

	$casetypeCountValues=array_count_values($casetypeArr);
	//案件数量排列
	arsort($casetypeCountValues);
	// print_r($casetypeCountValues);

	//旧版的操作方法
	// return $casetypeCountValues;

	//新版的操作方法
	$arr=array();
	foreach ($casetypeCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}
//***************************************获取二级案由
function getSecondtag($rst){
	$minshiArr=array();
	$xingshiArr=array();
	$xingzhengArr=array();
	$zhichanArr=array();
	$zhixingArr=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$casetype=$val['casetype'];
		$secondtag=$val['secondtag'];
		// var_dump($secondtag);
		// echo "<br>";
		// array_push($tagArr, $tag);

		switch ($casetype) {
			case '民事':
				$minshiArr=array_merge($minshiArr,array($secondtag));
				break;
			case '刑事':
				$xingshiArr=array_merge($xingshiArr,array($secondtag));
				break;
			case '行政':
				$xingzhengArr=array_merge($xingzhengArr,array($secondtag));
				break;
			case '知识产权':
				$zhichanArr=array_merge($zhichanArr,array($secondtag));
				break;
			case '执行':
				$zhixingArr=array_merge($zhixingArr,array($secondtag));
				break;
			default:
				# code...
				break;
		}
	}
	// print_r($rankArr);

	$minshiCountValues=array_count_values($minshiArr);
	$xingshiCountValues=array_count_values($xingshiArr);
	$xingzhengCountValues=array_count_values($xingzhengArr);
	$zhichanCountValues=array_count_values($zhichanArr);
	$zhixingCountValues=array_count_values($zhixingArr);
	//案件数量排列
	arsort($minshiCountValues);
	arsort($xingshiCountValues);
	arsort($xingzhengCountValues);
	arsort($zhichanCountValues);
	arsort($zhixingCountValues);
	// print_r($tagCountValues);

	//旧版的操作方法
	// return $tagCountValues;

	//新版的操作方法
	$arr_minshi=array();
	if (empty($minshiCountValues)) {
		$arr_minshi[0]['value']=0;
		$arr_minshi[0]['name']="无此类案件";
	}else{
	foreach ($minshiCountValues as $key => $value) {
		array_push($arr_minshi, array('value'=>$value,'name'=>$key));
	}
	}
	$arr_xingshi=array();
	if (empty($xingshiCountValues)) {
		$arr_xingshi[0]['value']=0;
		$arr_xingshi[0]['name']="无此类案件";
	}else{
	foreach ($xingshiCountValues as $key => $value) {
		array_push($arr_xingshi, array('value'=>$value,'name'=>$key));
	}
	}
	$arr_xingzheng=array();
	if (empty($xingzhengCountValues)) {
		$arr_xingzheng[0]['value']=0;
		$arr_xingzheng[0]['name']="无此类案件";
	}else{
	foreach ($xingzhengCountValues as $key => $value) {
		array_push($arr_xingzheng, array('value'=>$value,'name'=>$key));
	}
	}
	$arr_zhichan=array();
	if (empty($zhichanCountValues)) {
		$arr_zhichan[0]['value']=0;
		$arr_zhichan[0]['name']="无此类案件";
	}else{
	foreach ($zhichanCountValues as $key => $value) {
		array_push($arr_zhichan, array('value'=>$value,'name'=>$key));
	}
	}
	$arr_zhixing=array();
	if (empty($zhixingCountValues)) {
		$arr_zhixing[0]['value']=0;
		$arr_zhixing[0]['name']="无此类案件";
	}else{
	foreach ($zhixingCountValues as $key => $value) {
		array_push($arr_zhixing, array('value'=>$value,'name'=>$key));
	}
	}

	$arr=array();
	$arr['民事']=$arr_minshi;
	$arr['刑事']=$arr_xingshi;
	$arr['行政']=$arr_xingzheng;
	$arr['知产']=$arr_zhichan;
	$arr['执行']=$arr_zhixing;

	foreach ($arr as $key => $value) {
		if (empty($value)) {
			$arr[$key]=array('value'=>0,'name'=>$key);
		}
	}
	return $arr;
}
//***************************************获取劳动争议案件三级案由
function getLbthirdtag($rst){
	$lbthirdtagArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$thirdtag=$val['thirdtag'];
		array_push($lbthirdtagArr, $thirdtag);
	}
	$thirdtagArr=array_count_values($lbthirdtagArr);
	//案件数量排列
	ksort($thirdtagArr);
	$arr=array();
	foreach ($thirdtagArr as $key => $value) {
	    array_push($arr, array('value'=>$value,'name'=>$key));
	}
	// print_r($monthCountValues);
	return $arr;
}
//***************************************获取日期的数据
function getMonth($rst){
	$monthArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$date=$val['date'];
		if(preg_match("/^201(1|2|3|4|5)/", $date)==1){
			array_push($monthArr, substr($date, 0, 7));
		}

	}

	// print_r($monthArr);

	$monthCountValues=array_count_values($monthArr);
	//案件数量排列
	ksort($monthCountValues);
	// print_r($monthCountValues);
	return $monthCountValues;
}
//***************************************获取法院层级
function getRank($rst){
	$rankArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$rank=$val['rank'];
		array_push($rankArr, $rank);
	}
	// print_r($rankArr);

	$rankCountValues=array_count_values($rankArr);
	//旧版的操作方法
	// return $rankCountValues;
	//新版的操作方法
	$arr=array();
	foreach ($rankCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}

//***************************************获取地域的数据
function getSite($rst){

	$siteArr=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$court=$val['court'];
		$site=$val['site'];




    $siteArr=array_merge($siteArr,array($site));
	
	}

	// print_r($siteArr);

	$siteCountValues=array_count_values($siteArr);
	//案件数量排列
	arsort($siteCountValues);
	// print_r($siteCountValues);
	//旧版的操作方法
	// return $siteCountValues;
	//新版的操作方法
	$arr=array();
	foreach ($siteCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key,'selected'=>true));
	}

	// file_put_contents('c:/test.log', var_export($arr,true));
	return $arr;
}




// //***************************************获取文书类型
function getDoctype($rst){
	$doctypeArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		array_push($doctypeArr, $doctype);
	}
	// print_r($rankArr);

	$doctypeCountValues=array_count_values($doctypeArr);
	//案件数量排列
	arsort($doctypeCountValues);
	// print_r($doctypeCountValues);

	//旧版的操作方法
	// return $doctypeCountValues;

	//新版的操作方法
	$arr=array();
	foreach ($doctypeCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}
//***************************************获取案件审级
function getLevel($rst){
	$levelArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$level=$val['level'];
		array_push($levelArr, $level);
	}
	// print_r($levelArr);

	$levelCountValues=array_count_values($levelArr);
	//案件数量排列
	arsort($levelCountValues);
	// print_r($levelCountValues);

	//旧版的操作方法
	// return $levelCountValues;

	//新版的操作方法
	$arr=array();
	foreach ($levelCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}

//***************************************获取判决结果
function getPanjueResult($rst){
	$resultArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['juegeresult'];
		if ($doctype=='判决') {
			array_push($resultArr, $result);
		}
	}
	// print_r($rankArr);

	$resultCountValues=array_count_values($resultArr);
	//案件数量排列
	arsort($resultCountValues);
	// print_r($resultCountValues);

	//旧版的操作方法
	// return $resultCountValues;

	//新版的操作方法
	$arr=array();
	foreach ($resultCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}
//***************************************获取裁定结果
function getCaidingResult($rst){
	$resultArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['juegeresult'];
		if ($doctype=='裁定') {
			array_push($resultArr, $result);
		}
	}
	// print_r($rankArr);

	$resultCountValues=array_count_values($resultArr);
	//案件数量排列
	arsort($resultCountValues);
	// print_r($resultCountValues);

	//旧版的操作方法
	// return $resultCountValues;

	//新版的操作方法
	$arr=array();
	foreach ($resultCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key));
	}

	return $arr;
}



//===============================================获取代理案件数量最多的三名律师及其代理案件案由
    function getthreelawyernum($rst,$name){
    $Totalarray=array();
	$lawyernum=array();
	$secondtagnum=array();
	$secondtagArr1=array();
	$secondtagArr2=array();
	$secondtagArr3=array();
	foreach ($rst as $key => $val) {
	$lawyerArr=array();
	$fid=$val['_id'];
	$party=$val['party'];
	$agent=$val['agent'];
	$secondtag=$val['secondtag'];

	foreach ($party as $key1 => $val1) {
			foreach ($val1['mandatary'] as $key2 => $val2) {
				if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
						$lawyerArr=array_merge($lawyerArr, array($val2['name']=>$secondtag));
					}
				}
			
		}
	// $lawyerArr1 =array_unique($lawyerArr); 找代理律师应该不去重	
	$Totalarray=array_merge_recursive($Totalarray, $lawyerArr);
	}

	$W=array();	
	foreach ($Totalarray as $k3 => $v3) {
	$Q=sizeof($v3);
	$W[$k3]=$Q;
	}
	arsort($W);
	$E=array_keys($W);
	foreach ($Totalarray as $k4 => $v4) {
		if ($E[0]==$k4) {
			$secondtagArr1=array_merge($secondtagArr1, $v4);
		}else if ($E[1]==$k4) {
			$secondtagArr2=array_merge($secondtagArr2, $v4);
		}else if ($E[2]==$k4) {
			$secondtagArr3=array_merge($secondtagArr3, $v4);
		}	
	}
	$lawyersecondtagArr1=array_count_values($secondtagArr1);
	$lawyersecondtagArr2=array_count_values($secondtagArr2);
	$lawyersecondtagArr3=array_count_values($secondtagArr3);

	arsort($lawyersecondtagArr1);
	arsort($lawyersecondtagArr2);
	arsort($lawyersecondtagArr3);
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$i=$j=$k=0;
	foreach ($lawyersecondtagArr1 as $k1 => $v1) {
		if (empty($v1)) {
			array_push($arr1, array('value'=>0,'name'=>$k1));
		}
		array_push($arr1, array('value'=>$v1,'name'=>$k1));
		$i=$i+$v1;
		}
	$arr2=array();
	foreach ($lawyersecondtagArr2 as $k2 => $v2) {
		if (empty($v2)) {
			array_push($arr2, array('value'=>0,'name'=>$k2));
		}
		array_push($arr2, array('value'=>$v2,'name'=>$k2));
		$j=$j+$v2;
		}
	$arr3=array();
	foreach ($lawyersecondtagArr3 as $k3 => $v3) {
		if (empty($v3)) {
			array_push($arr3, array('value'=>0,'name'=>$k3));
		}
		array_push($arr3, array('value'=>$v3,'name'=>$k3));
		$k=$k+$v3;
		}
	$arr=array($arr1,$arr2,$arr3,$E[0]."(".$i."件)",$E[1]."(".$j."件)",$E[2]."(".$k."件)");
	// $arr=array($arr1,$arr2,$arr3,$E[0],$E[1],$E[2]);
	return ($arr) ;
  }

//===============================================获取法院报告判决的胜败结果
  function getpanjuewinner($rst){

    $resultpanjueArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$winner=$val['winner'];
		if ($doctype=='判决') {
			array_push($resultpanjueArr, $winner);
		}
        $resultpanjueCountValues=array_count_values($resultpanjueArr);
        $arr1=array_keys($resultpanjueCountValues);
        $arr2=array_values($resultpanjueCountValues);
	}
    $arr=array(array('value'=>$arr2[0],'name'=>$arr1[0].'胜诉'),array('value'=>$arr2[1],'name'=>$arr1[1].'胜诉'));	
	return $arr;
}
//===============================================获取法院报告攻方机构胜诉案件的三级案由
		  function getGJGpanjuecasetype($rst){
		  $GJGpanjuecasetypewinArr=array();

          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
          $thirdtag=$val['thirdtag'];

          foreach ($party as $key1 => $val1) {
          $$val1['name']=preg_replace('/（.*?）/', "", $val1['name']);
		  $ministr3=array();
				if (($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val1['name'])>9){
			    $ministr3=array_merge($ministr3,array($thirdtag));
				$GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
                }
			 }
         }
 	     $arr0=array_count_values($GJGpanjuecasetypewinArr);

 	
 	    $arr=array();
	    foreach ($arr0 as $key2 => $value2) {
		array_push($arr, array('value'=>$value2,'name'=>$key2));
	    }
 	     
 	    return $arr ;
   }

   //===============================================获取法院报告攻方个人胜诉案件的三级案由
		  function getGGRpanjuecasetype($rst){
		  $GGRpanjuecasetypewinArr=array();

          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
          $thirdtag=$val['thirdtag'];

          foreach ($party as $key1 => $val1) {
          $$val1['name']=preg_replace('/（.*?）/', "", $val1['name']);
		  $ministr3=array();
				if (($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val1['name'])<=9){
			    $ministr3=array_merge($ministr3,array($thirdtag));
				$GGRpanjuecasetypewinArr=array_merge($GGRpanjuecasetypewinArr,$ministr3);
                }
			 }
         }
 	     $arr0=array_count_values($GGRpanjuecasetypewinArr);
 	   
 	    $arr=array();
	    foreach ($arr0 as $key2 => $value2) {
		array_push($arr, array('value'=>$value2,'name'=>$key2));
	    }
 	    return $arr ;
   }

   //===============================================获取法院报告守方机构胜诉案件的三级案由
		  function getSJGpanjuecasetype($rst){
		  $SJGpanjuecasetypewinArr=array();

          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
          $thirdtag=$val['thirdtag'];

          foreach ($party as $key1 => $val1) {
          $$val1['name']=preg_replace('/（.*?）/', "", $val1['name']);
		  $ministr3=array();
				if (($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val1['name'])>9){
			    $ministr3=array_merge($ministr3,array($thirdtag));
				$SJGpanjuecasetypewinArr=array_merge($SJGpanjuecasetypewinArr,$ministr3);
                }
			 }
         }
 	     $arr0=array_count_values($SJGpanjuecasetypewinArr);
 	 
 	    $arr=array();
	    foreach ($arr0 as $key2 => $value2) {
		array_push($arr, array('value'=>$value2,'name'=>$key2));
	    }
 	     
 	     return $arr ;
   }

   //===============================================获取法院报告守方个人胜诉案件的三级案由
		  function getSGRpanjuecasetype($rst){
		  $SGRpanjuecasetypewinArr=array();

          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
          $thirdtag=$val['thirdtag'];

           foreach ($party as $key1 => $val1) {
            $$val1['name']=preg_replace('/（.*?）/', "", $val1['name']);
		    $ministr3=array();
				if (($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val1['name'])<=9){
			    $ministr3=array_merge($ministr3,array($thirdtag));
				$SGRpanjuecasetypewinArr=array_merge($SGRpanjuecasetypewinArr,$ministr3);
                }
			  }
           }
 	     $arr0=array_count_values($SGRpanjuecasetypewinArr);
 	     $arr=array();
	     foreach ($arr0 as $key2 => $value2) {
		 array_push($arr, array('value'=>$value2,'name'=>$key2));
	     }
 	     return $arr ;
   }

//===============================获取法院报告机构当事人案件数量排行前五名的判决案件和胜诉案件数量
    function getJGdsrno5($rst){
			
			$dsrArr=array();
		    foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];


			foreach ($party as $key1 => $val1) {
				$ministr=array();
			    $ministr=array_merge($ministr,array($val1['name']));
				$dsrArr=array_merge($dsrArr,$ministr);
                }
			 }
		  

			$jgdsr=array();
		    foreach ($dsrArr as $key2 => $value2) {
            $value2=preg_replace('/（.*?）/', "", $value2);
			if (strlen($value2)>9) {
            array_push($jgdsr,$value2);
            }
         }
            $jgdsrCountValues=array_count_values($jgdsr);
            arsort($jgdsrCountValues);
            
            // print_r($jgdsrCountValues);

            //array_slice函数——截取数组中的前五个值，0为开始的位置，截取5个值
            $arrno5=array_slice($jgdsrCountValues,0,5);
           
            $arrno5new=array();
            foreach ($arrno5 as $key6 => $value6) {
		    	array_push($arrno5new, array('value'=>$value6,'name'=>$key6));
		    }
            // print_r($arrno5new);

            //作为攻方时的判决案件数量

         	$arr2=array(
	          	array('name'=>$arrno5new[0]['name'],'value'=>0),
	          	array('name'=>$arrno5new[1]['name'],'value'=>0),
	          	array('name'=>$arrno5new[2]['name'],'value'=>0),
	          	array('name'=>$arrno5new[3]['name'],'value'=>0),
	          	array('name'=>$arrno5new[4]['name'],'value'=>0)
	          	);

            foreach ($rst as $key3 => $val3) {
	            $fid1=$val3['_id'];
				$party1=$val3['party'];
		        $doctype=$val3['doctype'];
		        foreach ($party1 as $key4 => $val4) {
		          	foreach ($arr2 as $key5 => $val5) {
		        		if (($val4['type']=='plaintiff'||$val4['type']=='appellant'||$val4['type']=='proposer')&&$doctype=="判决"&&$val4['name']==$val5['name']) {
			            	$arr2[$key5]['value']++;
		        	    }
		            }
		        }
			}

			// print_r($arr2);
            //作为守方时的判决案件数量
            $arr3=array(
	          	array('name'=>$arrno5new[0]['name'],'value'=>0),
	          	array('name'=>$arrno5new[1]['name'],'value'=>0),
	          	array('name'=>$arrno5new[2]['name'],'value'=>0),
	          	array('name'=>$arrno5new[3]['name'],'value'=>0),
	          	array('name'=>$arrno5new[4]['name'],'value'=>0)
	          	);

            foreach ($rst as $key7 => $val7) {
	            $fid2=$val7['_id'];
				$party2=$val7['party'];
		        $doctype1=$val7['doctype'];
		        foreach ($party2 as $key8 => $val8) {
		          	foreach ($arr3 as $key9 => $val9) {
		        		if ($val8['type']=='defendant'&&$doctype1=="判决"&&$val8['name']==$val9['name']) {
			            	$arr3[$key9]['value']++;
		        	    }
		            }
		        }
			}
            // print_r($arr3);

            //作为攻方时的胜诉案件数量
            $arr4=array(
	          	array('name'=>$arrno5new[0]['name'],'value'=>0),
	          	array('name'=>$arrno5new[1]['name'],'value'=>0),
	          	array('name'=>$arrno5new[2]['name'],'value'=>0),
	          	array('name'=>$arrno5new[3]['name'],'value'=>0),
	          	array('name'=>$arrno5new[4]['name'],'value'=>0)
	          	);

            foreach ($rst as $key10 => $val10) {
	            $fid3=$val10['_id'];
				$party3=$val10['party'];
		        $doctype2=$val10['doctype'];
		        $winner=$val10['winner'];

		        foreach ($party3 as $key11 => $val11) {
		          	foreach ($arr4 as $key12 => $val12) {
		        		if (($val11['type']=='plaintiff'||$val11['type']=='appellant'||$val11['type']=='proposer')&&$doctype2=="判决"&&$val11['name']==$val12['name']&&$winner=='攻方') {
			            	$arr4[$key12]['value']++;
		        	    }
		            }
		        }
			}
          
            // print_r($arr4);

            //作为守方时的胜诉案件数量

            $arr5=array(
	          	array('name'=>$arrno5new[0]['name'],'value'=>0),
	          	array('name'=>$arrno5new[1]['name'],'value'=>0),
	          	array('name'=>$arrno5new[2]['name'],'value'=>0),
	          	array('name'=>$arrno5new[3]['name'],'value'=>0),
	          	array('name'=>$arrno5new[4]['name'],'value'=>0)
	          	);

            foreach ($rst as $key13 => $val13) {
	            $fid4=$val13['_id'];
				$party4=$val13['party'];
		        $doctype3=$val13['doctype'];
		        $winner1=$val13['winner'];

		        foreach ($party4 as $key14 => $val14) {
		          	foreach ($arr5 as $key15 => $val15) {
		        		if ($val14['type']=='defendant'&&$doctype3=="判决"&&$val14['name']==$val15['name']&&$winner1=='守方') {
			            	$arr5[$key15]['value']++;
		        	    }
		            }
		        }
			}

	        // print_r($arr5);
            $arr1=array_merge($arr2,$arr3,$arr4,$arr5);
	        return ($arr1);

		}
        //=======================================获取审理案件数量no1法官中攻方机构胜诉案由分布
        function getno1GJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[0]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no1法官中攻方个人胜诉案由分布
      function getno1GGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[0]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }

     //============================================获取审理案件数量no1法官中守方机构胜诉案由分布
      function getno1SJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if ($val3['type']=='defendant'&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[0]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no1法官中守方个人胜诉案由分布
      function getno1SGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if ($val3['type']=='defendant'&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[0]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     
        //=======================================获取审理案件数量no2法官中攻方机构胜诉案由分布
        function getno2GJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[1]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no2法官中攻方个人胜诉案由分布
      function getno2GGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[1]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }

     //============================================获取审理案件数量no2法官中守方机构胜诉案由分布
      function getno2SJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='defendant'||$val3['type']=='appellee'||$val3['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[1]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no2法官中守方个人胜诉案由分布
      function getno2SGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='defendant'||$val3['type']=='appellee'||$val3['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[1]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }

    //=======================================获取审理案件数量no3法官中攻方机构胜诉案由分布
        function getno3GJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[2]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no3法官中攻方个人胜诉案由分布
      function getno3GGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='plaintiff'||$val3['type']=='appellant'||$val3['type']=='proposer')&&$doctype=="判决"&&$winner=="攻方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[2]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }

     //============================================获取审理案件数量no3法官中守方机构胜诉案由分布
      function getno3SJGcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='defendant'||$val3['type']=='appellee'||$val3['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])>9&&$val4['name']==$arr1[2]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
     }
     //============================================获取审理案件数量no3法官中守方个人胜诉案由分布
      function getno3SGRcasetype($rst){
 	    $judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
	    // print_r($judgeTotal);
        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //得到审理案件前三法官姓名 $arr1[0]  $arr1[1] $arr1[2]
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        $GJGpanjuecasetypewinArr=array();

            foreach ($rst as $key2 => $val2) {
			  $fid1=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $thirdtag=$val2['thirdtag'];
	          $judgetotal1=$val2['judges'];

	            foreach ($party as $key3 => $val3) {
		          $val3['name']=preg_replace('/（.*?）/', "", $val3['name']);
		          foreach ($judgetotal1 as $key4 => $val4) {
				  $ministr3=array();
					 if (($val3['type']=='defendant'||$val3['type']=='appellee'||$val3['type']=='respondent')&&$doctype=="判决"&&$winner=="守方"&&strlen($val3['name'])<=9&&$val4['name']==$arr1[2]){
				     $ministr3=array_merge($ministr3,array($thirdtag));
					 $GJGpanjuecasetypewinArr=array_merge($GJGpanjuecasetypewinArr,$ministr3);
	                 } 
		           }     
			    }
	        }
 	    $arr00=array_count_values($GJGpanjuecasetypewinArr);
 	    $arrGJGfinal=array();
	    foreach ($arr00 as $key5 => $value5) {
		array_push($arrGJGfinal, array('value'=>$value5,'name'=>$key5));
	    }
 	     
 	    return $arrGJGfinal;
    }

     //============================================获取实体法律适用
    	function getlaw($rst){
		
		$lawnameArr1=array();
		foreach ($rst as $val) {
		$fid=$val['_id'];
		$law=$val['law'];
		$lawnew=array();
		$lawnum=array();
			foreach ($law as $key1 => $value1) {
				$num=count($value1);
				$lawnum[$key1]=$num;
			}
			foreach ($lawnum as $key2 => $value2) {
				if (array_key_exists($key2, $lawnameArr1)) {
					$lawnameArr1[$key2]+=$value2;
				}else{
					$lawnameArr1[$key2]=$value2;
				}
			}
		}
		$lawArr1=array();
		$lawArr2=array();
		$lawArr3=array();
		$lawArr1_1=array();
		$lawArr2_1=array();
		$lawArr3_1=array();
		$lawnameArr111=array();
		foreach ($lawnameArr1 as $key => $value) {
			if (mb_strpos($key, "民事诉讼")==false) {
				if (mb_strrpos($key, "法")==mb_strlen($key)-1||mb_strrpos($key, "民法通则")==mb_strlen($key)-4) {
						$lawArr1[$key]=$value;
				}else if(mb_strpos($key, "最高人民法院")!==false||mb_strpos($key, "最高人民检察院")!==false){
						$lawArr2[$key]=$value;
				}else{
						$lawArr3[$key]=$value;
				}
			}
		}
		if (empty($lawArr1)) {
			$lawArr1_1[0]['value']=0;
			$lawArr1_1[0]['name']="未适用法律";
		}else{
			foreach ($lawArr1 as $k1 => $v1) {
			array_push($lawArr1_1, array('value'=>$v1,'name'=>$k1));
			}
		}
		if(empty($lawArr2)){
			$lawArr2_1[0]['value']=0;
			$lawArr2_1[0]['name']="未适用司法解释";
		}else{
			foreach ($lawArr2 as $k2 => $v2) {
			array_push($lawArr2_1, array('value'=>$v2,'name'=>$k2));
			}
		}
		if(empty($lawArr3)){
			$lawArr3_1[0]['value']=0;
			$lawArr3_1[0]['name']="未适用行政法规";
		}else{
			foreach ($lawArr3 as $k3 => $v3) {
			array_push($lawArr3_1, array('value'=>$v3,'name'=>$k3));
			}
		}
		

		arsort($lawArr1);
		arsort($lawArr2);
		arsort($lawArr3);
		$lawmax1=array_keys($lawArr1);
		$lawmax2=array_keys($lawArr2);
		$lawmax3=array_keys($lawArr3);
		$lawT0=array();
		$lawT0_1=array();
		$lawT1=array();
		$lawT1_1=array();
		$lawT2=array();
		$lawT2_1=array();
		$lawT3=array();
		$lawT3_1=array();
		$lawT4=array();
		$lawT4_1=array();
		$lawT5=array();
		$lawT5_1=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$law1=$val['law'];
			foreach ($law1 as $key3 => $value3) {
				if ($lawmax1[0]==$key3) {
					$lawT0=array_merge($lawT0, $value3);
				}else if ($lawmax1[1]==$key3) {
					$lawT1=array_merge($lawT1, $value3);
				}else if ($lawmax2[0]==$key3) {
					$lawT2=array_merge($lawT2, $value3);
				}else if ($lawmax2[1]==$key3) {
					$lawT3=array_merge($lawT3, $value3);
				}else if ($lawmax3[0]==$key3) {
					$lawT4=array_merge($lawT4, $value3);
				}else if ($lawmax3[1]==$key3) {
					$lawT5=array_merge($lawT5, $value3);
				}
			}
		}
		$lawT0=array_count_values($lawT0);
		$lawT1=array_count_values($lawT1);
		$lawT2=array_count_values($lawT2);
		$lawT3=array_count_values($lawT3);
		$lawT4=array_count_values($lawT4);
		$lawT5=array_count_values($lawT5);
		arsort($lawT0);
		arsort($lawT1);
		arsort($lawT2);
		arsort($lawT3);
		arsort($lawT4);
		arsort($lawT5);
		if (empty($lawT0)) {
			$lawT0_1[0]['value']=0;
			$lawT0_1[0]['name']="未适用法律";
		}else{
			foreach ($lawT0 as $k0 => $v0) {
			array_push($lawT0_1, array('value'=>$v0,'name'=>$k0));
			}
		}
		if(empty($lawT1)){
			$lawT1_1[0]['value']=0;
			$lawT1_1[0]['name']="未适用法律";
		}else{
			foreach ($lawT1 as $k1 => $v1) {
			array_push($lawT1_1, array('value'=>$v1,'name'=>$k1));
			}
		}
		if(empty($lawT2)){
			$lawT2_1[0]['value']=0;
			$lawT2_1[0]['name']="未适用司法解释";
		}else{
			foreach ($lawT2 as $k2 => $v2) {
			array_push($lawT2_1, array('value'=>$v2,'name'=>$k2));
			}
		}
		if(empty($lawT3)){
			$lawT3_1[0]['value']=0;
			$lawT3_1[0]['name']="未适用司法解释";
		}else{
			foreach ($lawT3 as $k3 => $v3) {
			array_push($lawT3_1, array('value'=>$v3,'name'=>$k3));
			}
		}
		if(empty($lawT4)){
			$lawT4_1[0]['value']=0;
			$lawT4_1[0]['name']="未适用行政法规";
		}else{
			foreach ($lawT4 as $k4 => $v4) {
			array_push($lawT4_1, array('value'=>$v4,'name'=>$k4));
			}
		}
		if(empty($lawT5)){
			$lawT5_1[0]['value']=0;
			$lawT5_1[0]['name']="未适用行政法规";
		}else{
			foreach ($lawT5 as $k5 => $v5) {
			array_push($lawT5_1, array('value'=>$v5,'name'=>$k5));
			}
		}
		
		$arrtest0_1=array_slice($lawT0_1,0,3);
		foreach ($lawT0_1 as $key0100 => $value0100) {
        $lawT0_100[]=$value0100['value'];
		}
		foreach ($arrtest0_1 as $key01000 => $value01000) {
        $arrtest_0100[]=$value01000['value'];
		}
		$arrayfinalT0_1=array_merge($arrtest0_1,array(array('name'=>'其他','value'=>(array_sum($lawT0_100)-array_sum($arrtest_0100)))));
		// print_r($arrayfinalT0_1);


	    $arrtest1_1=array_slice($lawT1_1,0,3);
		foreach ($lawT1_1 as $key1100 => $value1100) {
        $lawT1_100[]=$value1100['value'];
		}
		foreach ($arrtest1_1 as $key11000 => $value11000) {
        $arrtest_1100[]=$value11000['value'];
		}
		$arrayfinalT1_1=array_merge($arrtest1_1,array(array('name'=>'其他','value'=>(array_sum($lawT1_100)-array_sum($arrtest_1100)))));
		// print_r($arrayfinalT1_1);
  //    
	    $arrtest2_1=array_slice($lawT2_1,0,3);
		foreach ($lawT2_1 as $key2100 => $value2100) {
        $lawT2_100[]=$value2100['value'];
		}
		foreach ($arrtest2_1 as $key21000 => $value21000) {
        $arrtest_2100[]=$value21000['value'];
		}
		$arrayfinalT2_1=array_merge($arrtest2_1,array(array('name'=>'其他','value'=>(array_sum($lawT2_100)-array_sum($arrtest_2100)))));
		// print_r($arrayfinalT2_1);

		$arrtest3_1=array_slice($lawT3_1,0,3);
		foreach ($lawT3_1 as $key3100 => $value3100) {
        $lawT3_100[]=$value3100['value'];
		}
		foreach ($arrtest3_1 as $key31000 => $value31000) {
        $arrtest_3100[]=$value31000['value'];
		}
		$arrayfinalT3_1=array_merge($arrtest3_1,array(array('name'=>'其他','value'=>(array_sum($lawT3_100)-array_sum($arrtest_3100)))));
		// print_r($arrayfinalT3_1);

		$arrtest4_1=array_slice($lawT4_1,0,3);
		foreach ($lawT4_1 as $key4100 => $value4100) {
        $lawT4_100[]=$value4100['value'];
		}
		foreach ($arrtest4_1 as $key41000 => $value41000) {
        $arrtest_4100[]=$value41000['value'];
		}
		$arrayfinalT4_1=array_merge($arrtest4_1,array(array('name'=>'其他','value'=>(array_sum($lawT4_100)-array_sum($arrtest_4100)))));
		// print_r($arrayfinalT4_1);

		$arrtest5_1=array_slice($lawT5_1,0,3);
		foreach ($lawT5_1 as $key5100 => $value5100) {
        $lawT5_100[]=$value5100['value'];
		}
		foreach ($arrtest5_1 as $key51000 => $value51000) {
        $arrtest_5100[]=$value51000['value'];
		}
		$arrayfinalT5_1=array_merge($arrtest5_1,array(array('name'=>'其他','value'=>(array_sum($lawT5_100)-array_sum($arrtest_5100)))));
		// print_r($arrayfinalT5_1);
		

       
		$Arr=array($lawArr1_1,$lawArr2_1,$lawArr3_1,$arrayfinalT0_1,$arrayfinalT1_1,$arrayfinalT2_1,$arrayfinalT3_1,$arrayfinalT4_1,$arrayfinalT5_1,$lawmax1[0],$lawmax1[1],$lawmax2[0],$lawmax2[1],$lawmax3[0],$lawmax3[1]);
		return $Arr;
	  }
	    
	} 
