<?php

class Clean{

	function __construct($case){

		$this->case=$case;
		
		// try {
		// 	$conn = new MongoClient();
		// 	// $conn = new MongoClient('mongodb://192.168.170.33');
		// 	$db = $conn ->$database_name;
		// 	$this->test = $db->$collection_name;
		// } catch (Exception $e) {
		// 	file_put_contents('c:/dberror.log', $e,FILE_APPEND);			
		// }

	}

	//=======================================================删除重复文书
	public function cleanCase(){
	
		$starts=array();
		
		//删除重复文书
		foreach ($this->case as $key=>$val) {
			$content=$val['content'];
			$title=$val['title'];
			
			$pos=mb_strpos($content,"号");
			$start = mb_substr($content,0,$pos+1).$title;

			if (in_array($start, $starts)) {
				unset($this->case[$key]);
			}else{
				array_push($starts, $start);
			}
		}
		//删除空白字符
		foreach ($this->case as $key=>$val) {
			$content=$val['content'];
			
			$search = array(" ","　",'\r','\t','\ue5e5','+','?','&nbsp;','\\n\\t','&amp;#xA;');
			$replace = array("","","","","","","","","","\n");
			$doc = str_replace($search, $replace, $content);
			$doc=mb_convert_encoding($doc, 'UTF-8');
			$doc=preg_replace('/[\s]{2,}/', "\n", $doc);

			$this->case[$key]['doc']=$doc;
			unset($this->case[$key]['content']);
		}
		$_SESSION['totalCase']=count($this->case);
		return $this->case;
	}

}
