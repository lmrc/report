<?php 
// define('BASE_URL', "FILE_LOCMyweb/Apache/htdocs/mydata");
// define('FILE_LOC', 'c:/');
/**
* 
*/
class SubjectExport
{
	
	function __construct()
	{

	}
	
	//**************************************【1】遍历全部记录获取法院的数据

	function getCourt($rst){

		$courtTotal=array();
		//获取法院总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$court=$val['court'];
			array_push($courtTotal, $court);
		}
		// print_r($courtTotal);

		//获取法院案件次数
		$courtCountValues=array_count_values($courtTotal);

		//案件数量倒序排列
		arsort($courtCountValues);
		// print_r($courtCountValues);
		return $courtCountValues;
	}

	//**************************************添加法院信息到表格中
	function writeCourt($courtCountValues,$filename){
		require_once('../libraries/pexc/PHPExcel.php');
		require_once('../libraries/pexc/PHPExcel/Writer/Excel2007.php');
		require_once('../libraries/pexc/PHPExcel/Writer/Excel5.php'); 
		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '法院名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$k=2;
		foreach ($courtCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $k-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $value); 
			$k++;
		}

		$objWriter->save("FILE_LOC".$filename."-court.xls");
	}



	//**************************************【2】遍历全部记录获取法官的数据

	function getJudge($rst){
		$judgeTotal=array();

		//获取法官总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$justice=$val['justice'];
			foreach ($justice as $key => $value) {
				if ($value['type']=='presideJudge'||$value['type']=='judge') {
					unset($value['type']);
					array_push($judgeTotal,$value);
				}
			}
		}
		// print_r($judgeTotal);

		//获取法官去重的集合
		$judgeUnique=array_unique_fb_3($judgeTotal);
		// print_r($judgeUnique);

		//获取法官案件次数
		foreach ($judgeUnique as $key => $value) {
			foreach ($judgeTotal as $k => $v) {
				if($value==$v){
					$judgeUnique[$key]['times']++;
				}
			}
		}
		//案件数量倒序排列
		foreach ($judgeUnique as $key => $value) {
			$times[$key]=$value['times'];
			$name[$key]=$value['name'];
		}

		array_multisort($times,SORT_DESC,$name,$judgeUnique);
		// print_r($judgeUnique);
		return $judgeUnique;
	}


	//**************************************添加法官信息到表格中
	function writeJudge($judgeUnique,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '法官姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '所在法院');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '案件数量');

		$j=2;
		foreach ($judgeUnique as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $value['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['court']); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-judge.xls");

	}


	//**************************************【3】遍历全部记录获取律师的数据

	function getLawyer($rst,$name){
		$lawyerTotal=array();

		if (strpos(implode('、',$name), '律师事务所')!==false) {
			foreach ($rst as $val) {
				$fid=$val['_id'];
				$agent=$val['agent'];
				foreach ($agent as $key => $value) {
					if ($value['type']=="lawyer"){
						// echo $value['office'];
						if (in_array($value['office'], $name)) {
							echo $value['name'];
							$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
						}
					}
				}
			}
		}
		// else {
		// 	foreach ($rst as $val) {
		// 		$fid=$val['_id'];
		// 		$agent=$val['agent'];
		// 		foreach ($agent as $key => $value) {
		// 			if ($value['type']=="lawyer"){
		// 				if (count($name)!=0) {
		// 					if (in_array($value['client']['name'], $name)) {
		// 						$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
		// 					}
		// 				}else{
		// 					$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
						
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// print_r($lawyerTotal);

		//获取律师去重的集合
		// $lawyerUnique=array_unique_fb_2($lawyerTotal);
		// print_r($lawyerUnique);

		//获取律师案件次数
		// foreach ($lawyerUnique as $key => $value) {
		// 	foreach ($lawyerTotal as $k => $v) {
		// 		if($value==$v){
		// 			$lawyerUnique[$key]['times']++;
		// 		}
		// 	}
		// }
		// // print_r($lawyerUnique);
		// // echo "<br>";

		// //案件数量倒序排列
		// foreach ($lawyerUnique as $key => $value) {
		// 	$times[$key]=$value['times'];
		// 	$name[$key]=$value['name'];
		// }
		// array_multisort($times,SORT_DESC,$name,$lawyerUnique);
		// // print_r($lawyerUnique);

		// return $lawyerUnique;


		$lawyerCountValues=array_count_values($lawyerTotal);
		arsort($lawyerCountValues);
		return $lawyerCountValues;
	}

	//**************************************添加律师信息到表格中
	function writeLawyer1($lawyerUnique,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律师姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '所在律所');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '案件数量');


		$j=2;
		foreach ($lawyerUnique as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $value['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['office']); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawyer.xls");

	}

	//**************************************添加律师信息到表格中
	function writeLawyer2($lawyerCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律师姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');


		$j=2;
		foreach ($lawyerCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value);
			// $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['office']); 
			// $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawyer.xls");

	}

	//**************************************【4】遍历全部记录获取律所的数据

	function getLawfirm($rst,$name){
		$lawFirmTotal=array();
		//获取律所总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$agent=$val['agent'];
			// print_r($agent);

			foreach ($agent as $key => $value) {
				if ($value['type']=="lawyer") {
					if ($name!="") {
						if ($value['client']['name']==$name) {
							array_push($lawFirmTotal,$value['office']);
						}
					}else{
						array_push($lawFirmTotal,$value['office']);
					}
				}
			}
		}
		// print_r($lawFirmTotal);

		//获取律所案件次数
		$lawFirmCountValues=array_count_values($lawFirmTotal);

		//案件数量倒序排列
		arsort($lawFirmCountValues);
		// print_r($lawFirmCountValues);
		return $lawFirmCountValues;
	}


	//**************************************添加律所信息到表格中

	function writeLawfirm($lawFirmCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律所名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$k=2;
		foreach ($lawFirmCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $k-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $value); 
			$k++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawfirm.xls");
	}

	//**************************************【5】遍历全部记录获取当事人的数据

	function getParty($rst){

		$partyTotal=array();

		//获取当事人总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key => $value) {
				unset($value['type']);
				array_push($partyTotal,$value['name']);
			}
		}

		//获取当事人案件次数
		$partyCountValues=array_count_values($partyTotal);
		// print_r($partyCountValues);

		//案件数量倒序排列
		arsort($partyCountValues);
		// print_r($partyCountValues);

		return $partyCountValues;
	}

	//**************************************添加当事人信息到表格中
	function writeParty($partyCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($partyCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-party.xls");

	}

	//**************************************【6】遍历全部记录获取对方当事人的数据
	function getReverseParty($rst,$name){

		$reverseArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key0 => $val0) {
				if ($val0['name']==$name) {
					if ($val0['type']=="plaintiff") {
						foreach ($party as $key1 => $val1) {
							if ($val1['type']=="defendant") {
								$reverseArr[]=$val1['name'];
							}
						}
					}else if($val0['type']=="appellant"){
						foreach ($party as $key2 => $val2) {
							if ($val2['type']=="appellee"){
								$reverseArr[]=$val2['name'];
							}
						}	
					}else if($val0['type']=="defendant"){
						foreach ($party as $key3 => $val3) {
							if ($val3['type']=="plaintiff"){
								$reverseArr[]=$val3['name'];
			  				}
						}
		        	}else if($val0['type']=="appellee"){
						foreach ($party as $key4 => $val4) {
							if ($val4['type']=="appellant"){
								$reverseArr[]=$val4['name'];
							}	
						}
		 			}else if($val0['type']=="proposer"){
						foreach ($party as $key5 => $val5) {
							if ($val5['type']=="respondent"){
								$reverseArr[]=$val5['name'];
							}	
						}
					}else if($val0['type']=="respondent"){
						foreach ($party as $key6 => $val6) {
							if ($val6['type']=="proposer"){
								$reverseArr[]=$val6['name'];
							}	
						}
					}else if($val0['type']=="exEr"){
						foreach ($party as $key7 => $val7) {
							if ($val7['type']=="exEd"){
								$reverseArr[]=$val7['name'];
							}	
						}
					}else if($val0['type']=="exEd"){
						foreach ($party as $key8 => $val8) {
							if ($val8['type']=="exEr"){
								$reverseArr[]=$val8['name'];
							}	
						}
					}
				}
			}
		}
		// print_r($reverseArr);
		// echo '<br>';

		//对方当事人名单去重
		$reverseUniqueRaw=array_unique($reverseArr,SORT_STRING);
		foreach ($reverseUniqueRaw as $key => $value) {
			$reverseUnique[]=$value;
		}
		// print_r($reverseUnique);

		//对方当事人次数统计
		$reverseCountValues=array_count_values($reverseArr);
		// print_r($reverseCountValues);

		//案件数量倒序排列
		arsort($reverseCountValues);
		return $reverseCountValues;
	}

//**************************************【6】遍历全部记录获取律师的对方当事人的数据
	function getLawyerReverseParty($rst,$name){

		$reverseArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key0 => $val0) {
				foreach ($val0['mandatary'] as $keym => $valm) {
					if ($valm['name']==$name) {
						if ($val0['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									$reverseArr[]=$val1['name'];
								}
							}
						}else if($val0['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									$reverseArr[]=$val2['name'];
								}
							}	
						}else if($val0['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									$reverseArr[]=$val3['name'];
				  				}
							}
			        	}else if($val0['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									$reverseArr[]=$val4['name'];
								}	
							}
			 			}else if($val0['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									$reverseArr[]=$val5['name'];
								}	
							}
						}else if($val0['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									$reverseArr[]=$val6['name'];
								}	
							}
						}else if($val0['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									$reverseArr[]=$val7['name'];
								}	
							}
						}else if($val0['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									$reverseArr[]=$val8['name'];
								}	
							}
						}
					}
				}
			}
		}
		// print_r($reverseArr);
		// echo '<br>';

		//对方当事人名单去重
		$reverseUniqueRaw=array_unique($reverseArr,SORT_STRING);
		foreach ($reverseUniqueRaw as $key => $value) {
			$reverseUnique[]=$value;
		}
		// print_r($reverseUnique);

		//对方当事人次数统计
		$reverseCountValues=array_count_values($reverseArr);
		// print_r($reverseCountValues);

		//案件数量倒序排列
		arsort($reverseCountValues);
		//旧版的操作方法
		// return $reverseCountValues;

		//新版的操作方法
		$arr=array();
		foreach ($reverseCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}

		return $arr;
	}
// ================
//**************************************【6】遍历全部记录获取公司的对方当事人的数据
	function getcompanyReverseParty($rst,$name){

		$reverseArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key0 => $val0) {
				// foreach ($val0['mandatary'] as $keym => $valm) {
					if ($val0['name']==$name) {
						if ($val0['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									$reverseArr[]=$val1['name'];
								}
							}
						}else if($val0['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									$reverseArr[]=$val2['name'];
								}
							}	
						}else if($val0['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									$reverseArr[]=$val3['name'];
				  				}
							}
			        	}else if($val0['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									$reverseArr[]=$val4['name'];
								}	
							}
			 			}else if($val0['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									$reverseArr[]=$val5['name'];
								}	
							}
						}else if($val0['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									$reverseArr[]=$val6['name'];
								}	
							}
						}else if($val0['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									$reverseArr[]=$val7['name'];
								}	
							}
						}else if($val0['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									$reverseArr[]=$val8['name'];
								}	
							}
						}
					}
				// }
			}
		}
		// print_r($reverseArr);
		// echo '<br>';

		//对方当事人名单去重
		$reverseUniqueRaw=array_unique($reverseArr,SORT_STRING);
		foreach ($reverseUniqueRaw as $key => $value) {
			$reverseUnique[]=$value;
		}
		// print_r($reverseUnique);

		//对方当事人次数统计
		$reverseCountValues=array_count_values($reverseArr);
		// print_r($reverseCountValues);

		//案件数量倒序排列
		arsort($reverseCountValues);
		//旧版的操作方法
		// return $reverseCountValues;

		//新版的操作方法
		$arr=array();
		foreach ($reverseCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}

		return $arr;
	}

// ==============
	//**************************************将对方当事人信息导入Excel

	function writeReverseParty($reverseCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverse.xls");

	}
	//**************************************【7】遍历全部记录获取律师己方当事人（客户）的数据
	function getLawyerClient($rst,$name){
		$clientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];


			foreach ($party as $key1 => $val1) {
				$ministr=array();
				foreach ($val1['mandatary'] as $key2 => $val2) {
					// if ($val2['type']=='lawyer'&&in_array($val2['office'],$name)!==false) {
					if ($val2['type']=='lawyer'&&$val2['name']==$name) {
						if (in_array($val1['name'], $ministr)==false) {
							$ministr=array_merge($ministr,array($val1['name']));
						}
					}
				}
				$clientArr=array_merge($clientArr,$ministr);
			}

		}
		// print_r($clientArr);
		// echo '<hr>';

		//己方当事人名单去重
		// $clientUniqueRaw=array_unique($clientArr,SORT_STRING);
		// foreach ($clientUniqueRaw as $key => $value) {
		// 	$clientUnique[]=$value;
		// }
		// print_r($clientUnique);

		//己方当事人次数统计
		$clientCountValues=array_count_values($clientArr);
		// print_r($clientCountValues);

		//案件数量倒序排列
		arsort($clientCountValues);
		// print_r($clientCountValues);
		//旧版的操作方法
		// return $clientCountValues;
 
		//新版的操作方法
		$arr=array();
		foreach ($clientCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}

		return $arr;

	}

	//**************************************【7】遍历全部记录获取律所己方当事人（客户）的数据
	function getLawfirmClient($rst,$name){

		$clientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministr=array();
				foreach ($val1['mandatary'] as $key2 => $val2) {
					// if ($val2['type']=='lawyer'&&in_array($val2['office'],$name)!==false) {
					if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
						if (in_array($val1['name'], $ministr)==false) {
							$ministr=array_merge($ministr,array($val1['name']));
						}
					}
				}
				$clientArr=array_merge($clientArr,$ministr);
			}

		}
		// print_r($clientArr);
		// echo '<hr>';

		//己方当事人名单去重
		// $clientUniqueRaw=array_unique($clientArr,SORT_STRING);
		// foreach ($clientUniqueRaw as $key => $value) {
		// 	$clientUnique[]=$value;
		// }
		// print_r($clientUnique);

		//己方当事人次数统计
		$clientCountValues=array_count_values($clientArr);
		// print_r($clientCountValues);

		//案件数量倒序排列
		arsort($clientCountValues);
		// print_r($clientCountValues);
		return $clientCountValues;
	}


	//**************************************将己方当事人信息导入Excel

	function writeClient($clientCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($clientCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		// $objWriter->save("FILE_LOC".$filename."-client.xls");
		$objWriter->save("c:/testclient.xls");

	}


	//**************************************【8】遍历全部记录获取律所律师对方当事人（客户）的数据
	function getReverseClient($rst,$name){

		$reverseClientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $k => $v) {
				$ministr=array();
				foreach ($v['mandatary'] as $key0 => $val0) {
					if ($val0['type']=='lawyer'&&in_array($val0['office'], $name)) {
						if ($v['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									// echo $val1['name'];
									if (in_array($val1['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val1['name']));
										// print_r($ministr);
									}
								}
							}
						}else if($v['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									// echo $val2['name'];
									if (in_array($val2['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val2['name']));
										// print_r($ministr);
									}
								}
							}	
						}else if($v['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									// echo $val3['name'];
									if (in_array($val3['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val3['name']));
										// print_r($ministr);
									}
				  				}
							}
			        	}else if($v['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									// echo $val4['name'];
									if (in_array($val4['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val4['name']));
										// print_r($ministr);
									}
								}	
							}
			 			}else if($v['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									// echo $val5['name'];
									if (in_array($val5['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val5['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									// echo $val6['name'];
									if (in_array($val6['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val6['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									// echo $val7['name'];
									if (in_array($val7['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val7['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									// echo $val8['name'];
									if (in_array($val8['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val8['name']));
										// print_r($ministr);
									}
								}	
							}
						}
					
						print_r($ministr);
					}
					
				}
				$reverseClientArr=array_merge($reverseClientArr,$ministr);
			}
		}
		// print_r($reverseClientArr);
		// echo '<hr>';

		// //名单去重
		// $reverseClientUniqueRaw=array_unique($reverseClientArr,SORT_STRING);
		// foreach ($reverseClientUniqueRaw as $key => $value) {
		// 	$reverseClientUnique[]=$value;
		// }

		//次数统计
		$reverseClientCountValues=array_count_values($reverseClientArr);

		//倒序排列
		arsort($reverseClientCountValues);


		// print_r($reverseClientCountValues);

		return $reverseClientCountValues;
	}

	//**************************************将信息导入Excel

	function writeReverseClient($reverseClientCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseClientCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverseClient.xls");

	}



	//**************************************【9】遍历全部记录获取对手律所的数据
	function getReverseAgent($rst,$name){

		$reverseAgentArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			$ministr=array();
			foreach ($agent as $k1 => $v1) {
				if ($v1['type']=='lawyer'&&in_array($v1['office'], $name)) {

					foreach ($agent as $k2 => $v2) {
						if ($v1['client'][0]['type']=='plaintiff'&&$v2['client'][0]['type']=='defendant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
						}else if ($v1['client'][0]['type']=='defendant'&&$v2['client'][0]['type']=='plaintiff') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellant'&&$v2['client'][0]['type']=='appellee') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellee'&&$v2['client'][0]['type']=='appellant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='proposer'&&$v2['client'][0]['type']=='respondent') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='respondent'&&$v2['client'][0]['type']=='proposer') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEr'&&$v2['client'][0]['type']=='exEd') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEd'&&$v2['client'][0]['type']=='exEr') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}
					}
					
				}
			}
			$reverseAgentArr=array_merge($reverseAgentArr,$ministr);
		}
		// print_r($reverseAgentArr);
		// echo '<hr>';

		//名单去重
		// $reverseAgentUniqueRaw=array_unique($reverseAgentArr,SORT_STRING);
		// foreach ($reverseAgentUniqueRaw as $key => $value) {
		// 	$reverseAgentUnique[]=$value;
		// }

		//次数统计
		$reverseAgentCountValues=array_count_values($reverseAgentArr);

		//倒序排列
		arsort($reverseAgentCountValues);

		
		print_r($reverseAgentCountValues);

		return $reverseAgentCountValues;
	}

	//**************************************将信息导入Excel

	function writeReverseAgent($reverseAgentCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseAgentCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverseAgent.xls");

	}


	//=================================================
	function array_unique_fb_1($array2D) 
	{ 
		foreach ($array2D as $v) 
		{ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v) 
		{ 
			$temp[$k] = explode(",",$v); //再将拆开的数组重新组装 
		} 
		return $temp; 
	} 


	//=================================================
	function array_unique_fb_2($array2D){ 
		foreach ($array2D as $k=>$v){ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[$k] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v){ 
			$array=explode(",",$v); //再将拆开的数组重新组装 
			$temp2[$k]["name"] =$array[0]; 
			$temp2[$k]["office"] =$array[1]; 
		} 
		return $temp2; 
	} 

	//===============================================
	function array_unique_fb_3($array2D){ 
		foreach ($array2D as $k=>$v){ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[$k] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v){ 
			$array=explode(",",$v); //再将拆开的数组重新组装 
			$temp2[$k]["name"] =$array[0]; 
			$temp2[$k]["court"] =$array[1]; 
		} 
		return $temp2; 
	} 


	//===============================================判断对方当事人类型【机构、个人】
	function isPerson($nameUnique){
		// file_put_contents('c:/test.log', var_export($nameUnique,true));
		$nameType=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($nameUnique as $key => $value) {

			// $pos4=strlen($value);
			if (strlen($value['name'])>9) {
				$nameType[0]['value']++;
			}else{
				$nameType[1]['value']++;
			}
		}
		// print_r($nameType);
		return $nameType;
	}

	//===============================================判断对方当事人类型【企业、其他】
	function isCompany($nameUnique){

		$nameType=array(
			array('name'=>'企业','value'=>0),
			array('name'=>'其他机构','value'=>0)
			);

		foreach ($nameUnique as $key => $value) {
			if (strlen($value['name'])>9) {
			    $pos1=strpos($value['name'],"公司");
				$pos2=strpos($value['name'],"企业");
				$pos3=strpos($value['name'],"厂");
			    $pos4=strpos($value['name'],"店");
			    $pos5=strpos($value['name'],"超市");
				if($pos1!==false||$pos2!==false||$pos3!==false||$pos4!==false||$pos5!==false){
					$nameType[0]['value']++;
				}else {
					$nameType[1]['value']++;
				}
			}
		}
		// print_r($nameType);
		return $nameType;
	}

}



