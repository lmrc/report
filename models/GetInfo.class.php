<?php 
/**
* 
*/
class GetInfo
{
	
	function __construct($case)
	{
		$this->case=$case;
		$this->name=$_SESSION['subName'];
		$this->office=$_SESSION['subOffice'];
	}

	public function get1D($dimension,$rst=array()){
		if (empty($rst)) $rst=$this->case;
		
		//获取所有出现的元素
		$arr=array_column($rst,$dimension);

		$arrTotal=array();
		foreach ($arr as $k => $v) {
			if (is_array($v)) {
				$arrTotal=array_merge($arrTotal,$v);
			} else {
				$arrTotal[]=$v;
			}
		}
		if (empty($arrTotal)||is_array($arrTotal[0])) {
			echo $dimension;
			exit();
		}
		//获取每个元素出现的次数
		$arrCount=array_count_values($arrTotal);
		//案件数量排列
		arsort($arrCount);
		//直接输出数组
		// return $arrCount;
		//输出Name=>Value
		$res=array();
		foreach ($arrCount as $key => $value) {
			$res[]=array('value'=>$value,'name'=>$key);
		}
		return $res;
	}

	public function get2D($dimension1,$dimension2,$rst=array()){
		if (empty($rst)) $rst=$this->case;

		$arr=array_column($rst,$dimension1);
		$arrCount=array_count_values($arr);
		$arrMid=array();
		foreach ($arrCount as $k => $v) {
			if ($k!=='') $arrMid[$k]=array();
		}
		
		foreach ($rst as $k => $v) {
			// if (empty($v[$dimension1])) {
			// 	echo $dimension1;
			// 	echo $dimension2;
			// 	print_r($v);
			// 	exit();
			// }
			if ($v[$dimension1]!=='') {
				if (is_array($v[$dimension2])) {
					$arrMid[$v[$dimension1]]=array_merge($arrMid[$v[$dimension1]],$v[$dimension2]);
				} else {
					$arrMid[$v[$dimension1]][]=$v[$dimension2];
				}
			}
			
		}

		$paiXu=array();
		foreach ($arrMid as $k => $v) {
			
			$arrCount=array_count_values($v);
			$sum=array_sum($arrCount);
			// arsort($arrCount);
			$res=array();
			foreach ($arrCount as $key => $value) {
				$res[]=array('value'=>$value,'name'=>$key);
			}
			$paiXu[$k]=array('res'=>$res,'sum'=>$sum);
		}
		@$sumCol=array_column($paiXu,'sum');
		array_multisort($sumCol,SORT_DESC,$paiXu);
		
		$arrFinal=array();
		foreach ($paiXu as $key => $val) {
			$arrFinal[$key]=$val['res'];
		}
		
		return $arrFinal;
	}

	public function get3D($dimension1,$dimension2,$dimension3,$rst=array()){
		if (empty($rst)) $rst=$this->case;

		$arr1=array_column($rst,$dimension1);
		$arr2=array_column($rst,$dimension2);

		$arrCount1=array_count_values($arr1);
		$arrCount2=array_count_values($arr2);

		$arrMid=array();
		foreach ($arrCount1 as $k1 => $v1) {
			foreach ($arrCount2 as $k2 => $v2) {
				$arrMid[$k1][$k2]=array();
			}
		}
		
		foreach ($rst as $k => $v) {
			// if (@!is_array($arrMid[$v[$dimension1]][$v[$dimension2]])) {
			// 	echo $dimension1;
			// 	echo $dimension2;
			// 	print_r($v);
			// 	exit();
			// }
			if (is_array($v[$dimension3])) {
				$arrMid[$v[$dimension1]][$v[$dimension2]]=array_merge($arrMid[$v[$dimension1]][$v[$dimension2]],$v[$dimension3]);
			} else {
				$arrMid[$v[$dimension1]][$v[$dimension2]][]=$v[$dimension3];
			}
		}

		$arrFinal=array();
		foreach ($arrMid as $k1 => $v1) {
			foreach ($v1 as $k2 => $v2) {
								
				$arrCount=array_count_values($v2);
				
				arsort($arrCount);
				
				$res=array();
				foreach ($arrCount as $key => $value) {
					$res[]=array('value'=>$value,'name'=>$key);
				}
				$arrFinal[$k1][$k2]=$res;
			}
		}
		
		return $arrFinal;
	}

	public function get4D($dimension1,$dimension2,$dimension3,$dimension4,$rst=array()){
		if (empty($rst)) $rst=$this->case;

		$arr1=array_column($rst,$dimension1);
		$arr2=array_column($rst,$dimension2);
		$arr3=array_column($rst,$dimension3);

		$arrCount1=array_count_values($arr1);
		$arrCount2=array_count_values($arr2);
		$arrCount3=array_count_values($arr3);

		$arrMid=array();
		foreach ($arrCount1 as $k1 => $v1) {
			foreach ($arrCount2 as $k2 => $v2) {
				foreach ($arrCount3 as $k3 => $v3) {
					$arrMid[$k1][$k2][$k3]=array();
				}
			}
		}
		
		foreach ($rst as $k => $v) {
			
			$arrMid[$v[$dimension1]][$v[$dimension2]][$v[$dimension3]][]=$v[$dimension4];
		}

		$arrFinal=array();
		foreach ($arrMid as $k1 => $v1) {
			foreach ($v1 as $k2 => $v2) {
				foreach ($v2 as $k3 => $v3) {
					$arrCount=array_count_values($v3);
					arsort($arrCount);
					
					$res=array();
					foreach ($arrCount as $key => $value) {
						$res[]=array('value'=>$value,'name'=>$key);
					}
					$arrFinal[$k1][$k2][$k3]=$res;
				}
			}
		}
		
		return $arrFinal;
	}

	//为数值为0的字段增加标签
	public function fillEmpty($arr){
		if (empty($arr)) {
			$arr[]=array('value'=>0,'name'=>'无此项数据');
		}
		return $arr;
	}

	//计算每个图的总数
	public function countSum($arr){
		$vals=array_column($arr,'value');
		$sum=array_sum($vals);
		return $sum;
	}

	//遍历全部记录获取律师己方当事人的数据
	public function getLawyerClient(){

		$clientArr=array();
		foreach ($this->case as $key => $val) {
			$agent=$val['agent'];
			foreach ($agent as $a) {

				if ($a['name']==$this->name&&$a['office']==$this->office) {
					$clientArr=array_merge($clientArr,$a['client']);
				}
			}
		}
		$_SESSION['totalClient']=count($clientArr);
		
		return $clientArr;
	}


	//遍历全部记录获取律师的对方当事人的数据
	public function getLawyerReverseParty(){

		$reverseArr=array();
		foreach ($this->case as $key => $val) {
			$agent=$val['agent'];
			$clientArr=array();
			foreach ($agent as $a) {

				$clientArr=array_merge($clientArr,$a['client']);
			}
			foreach ($agent as $a) {
				if ($a['name']==$this->name&&$a['office']==$this->office) {
					if ($a['client'][0]['status']=='攻方') {
						$this->case[$key]['status']='攻方';
						foreach ($clientArr as $c) {
							if ($c['status']=='守方'){
								$reverseArr[]=$c;
							}
						}
					}else if ($a['client'][0]['status']=='守方') {
						$this->case[$key]['status']='守方';
						foreach ($clientArr as $c) {
							if ($c['status']=='攻方'){
								$reverseArr[]=$c;
							}
						}
					}else{
						$this->case[$key]['status']='第三方';
					}
					break;
				}
			}
		}
		return $reverseArr;
	}

	

	//遍历全部记录获取公司的代理律所
	public function getCompanyLawfirm(){

		foreach ($this->case as $key => $val) {
			$this->case[$key]['lawfirm']='';
			$party=$val['party'];
			foreach ($party as $p) {
				if ($p['name']==$this->name&&(!empty($p['mandatary']))&&$p['mandatary'][0]['type']=='lawyer') {
					$this->case[$key]['lawfirm']=$p['mandatary'][0]['office'];
					break;
				}
			}
		}
	}

	//遍历全部记录获取公司的对方当事人的数据
	public function getCompanyReverseParty(){

		$reverseArr=array();
		foreach ($this->case as $key => $val) {
			$party=$val['party'];
			$secondtag=$val['secondtag'];
			
			foreach ($party as $p) {
				if ($p['name']==$this->name) {
					if ($p['status']=='攻方') {
						foreach ($party as $c) {
							if ($c['status']=='守方'){
								$c['secondtag']=$secondtag;
								$reverseArr[]=$c;
							}
						}
					}else if ($p['status']=='守方') {
						foreach ($party as $c) {
							if ($c['status']=='攻方'){
								$c['secondtag']=$secondtag;
								$reverseArr[]=$c;
							}
						}
					}
					break;
				}
			}
		}
		return $reverseArr;
	}

	//遍历全部记录获取月度分布
	public function getMonth(){

		$beginDate=$_SESSION['beginDate'];
		$finishDate=$_SESSION['finishDate'];

		//获取两个日期之间的所有月份
		$time1 = strtotime($beginDate); // 自动为00:00:00 时分秒
		$time2 = strtotime($finishDate);
		 
		$monthArr = array();
		$monthArr[] = date('Y-m',$time1); // 当前月;
		while( ($time1 = strtotime('+1 month', $time1)) <= $time2){
		      $monthArr[] = date('Y-m',$time1); // 取得递增月;  
		}
		// file_put_contents('c:/month.txt', var_export($monthArr,true));
		// exit();
		//获取所有出现的元素
		$arr=array_column($this->case,'date');

		$arrTotal=array();
		foreach ($arr as $k => $v) {
			$arrTotal[]=substr($v,0,7);
		}
		
		//获取每个元素出现的次数
		$arrCount=array_count_values($arrTotal);
		//案件数量排列
		arsort($arrCount);
		

		$res=array();
		foreach ($monthArr as $m) {
			if (array_key_exists($m,$arrCount)) {
				$res[$m]=$arrCount[$m];
			}else{
				$res[$m]=0;
			}
		}
		return $res;
	}

	// public function getCase($name){
	// 	foreach ($this->case as $key => $val) {
			
	// 	}
	// }

}