<?php 
// define('BASE_URL', "FILE_LOCMyweb/Apache/htdocs/mydata");
// define('FILE_LOC', 'c:/');
/**
* 
*/
class SubjectExport
{
	
	function __construct()
	{

		require_once('../libraries/pexc/PHPExcel.php');
		require_once('../libraries/pexc/PHPExcel/Writer/Excel2007.php');
		require_once('../libraries/pexc/PHPExcel/Writer/Excel5.php'); 
	}
	
	//**************************************【1】遍历全部记录获取法院的数据

	function getCourt($rst){

		$courtTotal=array();
		//获取法院总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$court=$val['court'];
			array_push($courtTotal, $court);
		}
		// print_r($courtTotal);

		//获取法院案件次数
		$courtCountValues=array_count_values($courtTotal);

		//案件数量倒序排列
		arsort($courtCountValues);
		// print_r($courtCountValues);
		return $courtCountValues;
	}

	//**************************************添加法院信息到表格中
	function writeCourt($courtCountValues,$filename){
		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '法院名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$k=2;
		foreach ($courtCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $k-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $value); 
			$k++;
		}

		$objWriter->save("FILE_LOC".$filename."-court.xls");
	}



	//**************************************法院报告遍历全部记录获取法官的数据

	function getJudge($rst){
		$judgeTotal=array();

		//获取法官总集合
				$judgeTotal=array();

		//获取法官总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key => $value) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$value['name']);
				}
			}
		
	   // print_r($judgeTotal);

        $arr=array_count_values($judgeTotal);
        arsort($arr);
        
        return $arr;
	}


	//**************************************添加法官信息到表格中
	function writeJudge($judgeUnique,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '法官姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '所在法院');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '案件数量');

		$j=2;
		foreach ($judgeUnique as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $value['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['court']); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-judge.xls");

	}


	//**************************************【3】遍历全部记录获取律师的数据

	function getLawyer($rst,$name){
		$lawyerTotal=array();

		if (strpos(implode('、',$name), '律师事务所')!==false) {
			foreach ($rst as $val) {
				$fid=$val['_id'];
				$agent=$val['agent'];
				foreach ($agent as $key => $value) {
					if ($value['type']=="lawyer"){
						// echo $value['office'];
						if (in_array($value['office'], $name)) {
							echo $value['name'];
							$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
						}
					}
				}
			}
		}
		// else {
		// 	foreach ($rst as $val) {
		// 		$fid=$val['_id'];
		// 		$agent=$val['agent'];
		// 		foreach ($agent as $key => $value) {
		// 			if ($value['type']=="lawyer"){
		// 				if (count($name)!=0) {
		// 					if (in_array($value['client']['name'], $name)) {
		// 						$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
		// 					}
		// 				}else{
		// 					$lawyerTotal=array_merge($lawyerTotal,array($value['name']));
						
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// print_r($lawyerTotal);

		//获取律师去重的集合
		// $lawyerUnique=array_unique_fb_2($lawyerTotal);
		// print_r($lawyerUnique);

		//获取律师案件次数
		// foreach ($lawyerUnique as $key => $value) {
		// 	foreach ($lawyerTotal as $k => $v) {
		// 		if($value==$v){
		// 			$lawyerUnique[$key]['times']++;
		// 		}
		// 	}
		// }
		// // print_r($lawyerUnique);
		// // echo "<br>";

		// //案件数量倒序排列
		// foreach ($lawyerUnique as $key => $value) {
		// 	$times[$key]=$value['times'];
		// 	$name[$key]=$value['name'];
		// }
		// array_multisort($times,SORT_DESC,$name,$lawyerUnique);
		// // print_r($lawyerUnique);

		// return $lawyerUnique;


		$lawyerCountValues=array_count_values($lawyerTotal);
		arsort($lawyerCountValues);
		return $lawyerCountValues;
	}

	//**************************************添加律师信息到表格中
	function writeLawyer1($lawyerUnique,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律师姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '所在律所');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', '案件数量');


		$j=2;
		foreach ($lawyerUnique as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $value['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['office']); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawyer.xls");

	}

	//**************************************添加律师信息到表格中
	function writeLawyer2($lawyerCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律师姓名');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');


		$j=2;
		foreach ($lawyerCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value);
			// $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value['office']); 
			// $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $value['times']); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawyer.xls");

	}

	//**************************************【4】遍历全部记录获取律所的数据

	function getLawfirm($rst,$name){
		$lawFirmTotal=array();
		//获取律所总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$agent=$val['agent'];
			// print_r($agent);

			foreach ($agent as $key => $value) {
				if ($value['type']=="lawyer") {
					if ($name!="") {
						if ($value['client']['name']==$name) {
							array_push($lawFirmTotal,$value['office']);
						}
					}else{
						array_push($lawFirmTotal,$value['office']);
					}
				}
			}
		}
		// print_r($lawFirmTotal);

		//获取律所案件次数
		$lawFirmCountValues=array_count_values($lawFirmTotal);

		//案件数量倒序排列
		arsort($lawFirmCountValues);
		// print_r($lawFirmCountValues);
		return $lawFirmCountValues;
	}


	//**************************************添加律所信息到表格中

	function writeLawfirm($lawFirmCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '律所名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$k=2;
		foreach ($lawFirmCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $k-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $value); 
			$k++;
		}

		$objWriter->save("FILE_LOC".$filename."-lawfirm.xls");
	}

	//**************************************【5】遍历全部记录获取当事人的数据

	function getParty($rst){

		$partyTotal=array();

		//获取当事人总集合
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key => $value) {
				unset($value['type']);
				array_push($partyTotal,$value['name']);
			}
		}

		//获取当事人案件次数
		$partyCountValues=array_count_values($partyTotal);
		// print_r($partyCountValues);

		//案件数量倒序排列
		arsort($partyCountValues);
		// print_r($partyCountValues);

		return $partyCountValues;
	}

	//**************************************添加当事人信息到表格中
	function writeParty($partyCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0);  

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($partyCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $j-1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $value); 
			$j++;
		}

		$objWriter->save("FILE_LOC".$filename."-party.xls");

	}

	//**************************************【6】遍历全部记录获取对方当事人的数据
	function getReverseParty($rst,$name){

		$reverseArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key0 => $val0) {
				if ($val0['name']==$name) {
					if ($val0['type']=="plaintiff") {
						foreach ($party as $key1 => $val1) {
							if ($val1['type']=="defendant") {
								$reverseArr[]=$val1['name'];
							}
						}
					}else if($val0['type']=="appellant"){
						foreach ($party as $key2 => $val2) {
							if ($val2['type']=="appellee"){
								$reverseArr[]=$val2['name'];
							}
						}	
					}else if($val0['type']=="defendant"){
						foreach ($party as $key3 => $val3) {
							if ($val3['type']=="plaintiff"){
								$reverseArr[]=$val3['name'];
			  				}
						}
		        	}else if($val0['type']=="appellee"){
						foreach ($party as $key4 => $val4) {
							if ($val4['type']=="appellant"){
								$reverseArr[]=$val4['name'];
							}	
						}
		 			}else if($val0['type']=="proposer"){
						foreach ($party as $key5 => $val5) {
							if ($val5['type']=="respondent"){
								$reverseArr[]=$val5['name'];
							}	
						}
					}else if($val0['type']=="respondent"){
						foreach ($party as $key6 => $val6) {
							if ($val6['type']=="proposer"){
								$reverseArr[]=$val6['name'];
							}	
						}
					}else if($val0['type']=="exEr"){
						foreach ($party as $key7 => $val7) {
							if ($val7['type']=="exEd"){
								$reverseArr[]=$val7['name'];
							}	
						}
					}else if($val0['type']=="exEd"){
						foreach ($party as $key8 => $val8) {
							if ($val8['type']=="exEr"){
								$reverseArr[]=$val8['name'];
							}	
						}
					}
				}
			}
		}
		// print_r($reverseArr);
		// echo '<br>';

		//对方当事人名单去重
		$reverseUniqueRaw=array_unique($reverseArr,SORT_STRING);
		foreach ($reverseUniqueRaw as $key => $value) {
			$reverseUnique[]=$value;
		}
		// print_r($reverseUnique);

		//对方当事人次数统计
		$reverseCountValues=array_count_values($reverseArr);
		// print_r($reverseCountValues);

		//案件数量倒序排列
		arsort($reverseCountValues);
		return $reverseCountValues;
	}

//**************************************【6】遍历全部记录获取律师的对方当事人的数据
	function getLawyerReverseParty($rst,$name){

		$reverseArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];

			foreach ($party as $key0 => $val0) {
				foreach ($val0['mandatary'] as $keym => $valm) {
					if ($valm['name']==$name) {
						if ($val0['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									$reverseArr[]=$val1['name'];
								}
							}
						}else if($val0['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									$reverseArr[]=$val2['name'];
								}
							}	
						}else if($val0['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									$reverseArr[]=$val3['name'];
				  				}
							}
			        	}else if($val0['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									$reverseArr[]=$val4['name'];
								}	
							}
			 			}else if($val0['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									$reverseArr[]=$val5['name'];
								}	
							}
						}else if($val0['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									$reverseArr[]=$val6['name'];
								}	
							}
						}else if($val0['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									$reverseArr[]=$val7['name'];
								}	
							}
						}else if($val0['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									$reverseArr[]=$val8['name'];
								}	
							}
						}
					}
				}
			}
		}
		// print_r($reverseArr);
		// echo '<br>';

		//对方当事人名单去重
		$reverseUniqueRaw=array_unique($reverseArr,SORT_STRING);
		foreach ($reverseUniqueRaw as $key => $value) {
			$reverseUnique[]=$value;
		}
		// print_r($reverseUnique);

		//对方当事人次数统计
		$reverseCountValues=array_count_values($reverseArr);
		// print_r($reverseCountValues);

		//案件数量倒序排列
		arsort($reverseCountValues);
		//旧版的操作方法
		// return $reverseCountValues;

		//新版的操作方法
		$arr=array();
		foreach ($reverseCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}

		return $arr;
	}

	//**************************************将对方当事人信息导入Excel

	function writeReverseParty($reverseCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverse.xls");

	}
	//**************************************【7】遍历全部记录获取律师己方当事人（客户）的数据
	function getLawyerClient($rst,$name){
		$clientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];


			foreach ($party as $key1 => $val1) {
				$ministr=array();
				foreach ($val1['mandatary'] as $key2 => $val2) {
					// if ($val2['type']=='lawyer'&&in_array($val2['office'],$name)!==false) {
					if ($val2['type']=='lawyer'&&$val2['name']==$name) {
						if (in_array($val1['name'], $ministr)==false) {
							$ministr=array_merge($ministr,array($val1['name']));
						}
					}
				}
				$clientArr=array_merge($clientArr,$ministr);
			}

		}
		// print_r($clientArr);
		// echo '<hr>';

		//己方当事人名单去重
		// $clientUniqueRaw=array_unique($clientArr,SORT_STRING);
		// foreach ($clientUniqueRaw as $key => $value) {
		// 	$clientUnique[]=$value;
		// }
		// print_r($clientUnique);

		//己方当事人次数统计
		$clientCountValues=array_count_values($clientArr);
		// print_r($clientCountValues);

		//案件数量倒序排列
		arsort($clientCountValues);
		// print_r($clientCountValues);
		//旧版的操作方法
		// return $clientCountValues;

		//新版的操作方法
		$arr=array();
		foreach ($clientCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}

		return $arr;

	}

	//**************************************【7】遍历全部记录获取律所己方当事人（客户）的数据
	function getLawfirmClient($rst,$name){

		$clientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministr=array();
				foreach ($val1['mandatary'] as $key2 => $val2) {
					// if ($val2['type']=='lawyer'&&in_array($val2['office'],$name)!==false) {
					if ($val2['type']=='lawyer'&&strpos($val2['office'],$name)!==false) {
						if (in_array($val1['name'], $ministr)==false) {
							$ministr=array_merge($ministr,array($val1['name']));
						}
					}
				}
				$clientArr=array_merge($clientArr,$ministr);
			}

		}
		// print_r($clientArr);
		// echo '<hr>';

		//己方当事人名单去重
		// $clientUniqueRaw=array_unique($clientArr,SORT_STRING);
		// foreach ($clientUniqueRaw as $key => $value) {
		// 	$clientUnique[]=$value;
		// }
		// print_r($clientUnique);

		//己方当事人次数统计
		$clientCountValues=array_count_values($clientArr);
		// print_r($clientCountValues);

		//案件数量倒序排列
		arsort($clientCountValues);
		// print_r($clientCountValues);
		return $clientCountValues;
	}


	//**************************************将己方当事人信息导入Excel

	function writeClient($clientCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '客户名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($clientCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		// $objWriter->save("FILE_LOC".$filename."-client.xls");
		$objWriter->save("c:/testclient.xls");

	}


	//**************************************【8】遍历全部记录获取律所律师对方当事人（客户）的数据
	function getReverseClient($rst,$name){

		$reverseClientArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $k => $v) {
				$ministr=array();
				foreach ($v['mandatary'] as $key0 => $val0) {
					if ($val0['type']=='lawyer'&&strpos($val0['office'], $name)==!false){
						if ($v['type']=="plaintiff") {
							foreach ($party as $key1 => $val1) {
								if ($val1['type']=="defendant") {
									// echo $val1['name'];
									if (in_array($val1['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val1['name']));
										// print_r($ministr);
									}
								}
							}
						}else if($v['type']=="appellant"){
							foreach ($party as $key2 => $val2) {
								if ($val2['type']=="appellee"){
									// echo $val2['name'];
									if (in_array($val2['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val2['name']));
										// print_r($ministr);
									}
								}
							}	
						}else if($v['type']=="defendant"){
							foreach ($party as $key3 => $val3) {
								if ($val3['type']=="plaintiff"){
									// echo $val3['name'];
									if (in_array($val3['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val3['name']));
										// print_r($ministr);
									}
				  				}
							}
			        	}else if($v['type']=="appellee"){
							foreach ($party as $key4 => $val4) {
								if ($val4['type']=="appellant"){
									// echo $val4['name'];
									if (in_array($val4['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val4['name']));
										// print_r($ministr);
									}
								}	
							}
			 			}else if($v['type']=="proposer"){
							foreach ($party as $key5 => $val5) {
								if ($val5['type']=="respondent"){
									// echo $val5['name'];
									if (in_array($val5['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val5['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="respondent"){
							foreach ($party as $key6 => $val6) {
								if ($val6['type']=="proposer"){
									// echo $val6['name'];
									if (in_array($val6['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val6['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEr"){
							foreach ($party as $key7 => $val7) {
								if ($val7['type']=="exEd"){
									// echo $val7['name'];
									if (in_array($val7['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val7['name']));
										// print_r($ministr);
									}
								}	
							}
						}else if($v['type']=="exEd"){
							foreach ($party as $key8 => $val8) {
								if ($val8['type']=="exEr"){
									// echo $val8['name'];
									if (in_array($val8['name'], $ministr)==false) {
										$ministr=array_merge($ministr,array($val8['name']));
										// print_r($ministr);
									}
								}	
							}
						}
					
						print_r($ministr);
					}
					
				}
				$reverseClientArr=array_merge($reverseClientArr,$ministr);
			}
		}
		// print_r($reverseClientArr);
		// echo '<hr>';

		// //名单去重
		// $reverseClientUniqueRaw=array_unique($reverseClientArr,SORT_STRING);
		// foreach ($reverseClientUniqueRaw as $key => $value) {
		// 	$reverseClientUnique[]=$value;
		// }

		//次数统计
		$reverseClientCountValues=array_count_values($reverseClientArr);

		//倒序排列
		arsort($reverseClientCountValues);


		// print_r($reverseClientCountValues);

		return $reverseClientCountValues;
	}

	//**************************************将信息导入Excel

	function writeReverseClient($reverseClientCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseClientCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverseClient.xls");

	}
//**************************************法院报告中攻方当事人类型判断
    function getGdsrtype($rst){
   
    $GdsrArr=array();

		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrG=array();
				if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer'){
			    $ministrG=array_merge($ministrG,array($val1['name']));
				$GdsrArr=array_merge($GdsrArr,$ministrG);
                }
			 }
		}

		//攻方当事人名单去重
		$GdsrUniqueRaw=array_unique($GdsrArr);
		foreach ($GdsrUniqueRaw as $key => $value) {
		$GdsrUnique[]=$value;
		}

		$arr=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($GdsrUnique as $key => $value) {
         $value=preg_replace('/（.*?）/', "", $value);
			if (strlen($value)>9) {
				$arr[0]['value']++;
			}else{
				$arr[1]['value']++;
			}
		}
		return $arr;
    }
//**************************************法院报告中守方当事人类型判断
    function getSdsrtype($rst){
   
    $SdsrArr=array();

		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrS=array();
				if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent'){
			    $ministrS=array_merge($ministrS,array($val1['name']));
				$SdsrArr=array_merge($SdsrArr,$ministrS);
                }
			 }
		}

		//守方当事人名单去重
		$SdsrUniqueRaw=array_unique($SdsrArr);
		foreach ($SdsrUniqueRaw as $key => $value) {
		$SdsrUnique[]=$value;
		}

		$arr=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($SdsrUniqueRaw as $key => $value) {
         $value=preg_replace('/（.*?）/', "", $value);
			if (strlen($value)>9) {
				$arr[0]['value']++;
			}else{
				$arr[1]['value']++;
			}
		}
		return $arr;
    }
	//**************************************【9】遍历全部记录获取对手律所的数据
	function getReverseAgent($rst,$name){

		$reverseAgentArr=array();
		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			$ministr=array();
			foreach ($agent as $k1 => $v1) {
				if ($v1['type']=='lawyer'&&strpos($v1['office'], $name)!==false) {   //=========注意要用strpos而不是array_in来筛选律所

					foreach ($agent as $k2 => $v2) {
						if ($v1['client'][0]['type']=='plaintiff'&&$v2['client'][0]['type']=='defendant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
						}else if ($v1['client'][0]['type']=='defendant'&&$v2['client'][0]['type']=='plaintiff') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellant'&&$v2['client'][0]['type']=='appellee') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='appellee'&&$v2['client'][0]['type']=='appellant') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='proposer'&&$v2['client'][0]['type']=='respondent') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='respondent'&&$v2['client'][0]['type']=='proposer') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEr'&&$v2['client'][0]['type']=='exEd') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}else if ($v1['client'][0]['type']=='exEd'&&$v2['client'][0]['type']=='exEr') {
							if (in_array($v2['office'], $ministr)==false) {
								$ministr=array_merge($ministr,array($v2['office']));
							}
							// $reverseAgentArr[]=$v2['name'];
							// $reverseAgentArr[]=$v2['office'];
						}
					}
					
				}
			}
			$reverseAgentArr=array_merge($reverseAgentArr,$ministr);
		}
		// print_r($reverseAgentArr);
		// echo '<hr>';

		//名单去重
		// $reverseAgentUniqueRaw=array_unique($reverseAgentArr,SORT_STRING);
		// foreach ($reverseAgentUniqueRaw as $key => $value) {
		// 	$reverseAgentUnique[]=$value;
		// }

		//次数统计
		$reverseAgentCountValues=array_count_values($reverseAgentArr);

		//倒序排列
		arsort($reverseAgentCountValues);

		
		print_r($reverseAgentCountValues);

		return $reverseAgentCountValues;
	}

	//**************************************将信息导入Excel

	function writeReverseAgent($reverseAgentCountValues,$filename){

		$objPHPExcel = new PHPExcel();  
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);  
		$objPHPExcel->setActiveSheetIndex(0); 

		$objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', '对方当事人名称');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', '案件数量');

		$j=2;
		foreach ($reverseAgentCountValues as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $j-1);  
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $key);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $value);
		    $j++;
		}
		
		$objWriter->save("FILE_LOC".$filename."-reverseAgent.xls");

	}


	//=================================================
	function array_unique_fb_1($array2D) 
	{ 
		foreach ($array2D as $v) 
		{ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v) 
		{ 
			$temp[$k] = explode(",",$v); //再将拆开的数组重新组装 
		} 
		return $temp; 
	} 


	//=================================================
	function array_unique_fb_2($array2D){ 
		foreach ($array2D as $k=>$v){ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[$k] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v){ 
			$array=explode(",",$v); //再将拆开的数组重新组装 
			$temp2[$k]["name"] =$array[0]; 
			$temp2[$k]["office"] =$array[1]; 
		} 
		return $temp2; 
	} 

	//===============================================
	function array_unique_fb_3($array2D){ 
		foreach ($array2D as $k=>$v){ 
			$v = join(",",$v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串 
			$temp[$k] = $v; 
		} 
		$temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组 
		foreach ($temp as $k => $v){ 
			$array=explode(",",$v); //再将拆开的数组重新组装 
			$temp2[$k]["name"] =$array[0]; 
			$temp2[$k]["court"] =$array[1]; 
		} 
		return $temp2; 
	} 


	//===============================================判断对方当事人类型【机构、个人】
	function isPerson($nameUnique){
		// file_put_contents('c:/test.log', var_export($nameUnique,true));
		$nameType=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($nameUnique as $key => $value) {
         $key=preg_replace('/（.*?）/', "", $key);
			// $pos4=strlen($value);
			if (strlen($key)>9) {
				$nameType[0]['value']++;
			}else{
				$nameType[1]['value']++;
			}
		}
		// print_r($nameType);
		return $nameType;
	}

	//===============================================判断法院报告中攻方当事人类型【企业or其他】
	function isGCompany($rst){
        
        $GdsrArr=array();

		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrG=array();
				if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer'){
			    $ministrG=array_merge($ministrG,array($val1['name']));
				$GdsrArr=array_merge($GdsrArr,$ministrG);
                }
			 }
		}

		//攻方当事人名单去重
		$GdsrUniqueRaw=array_unique($GdsrArr);
		foreach ($GdsrUniqueRaw as $key => $value) {
		$GdsrUnique[]=$value;
		}

		$arr=array(
			array('name'=>'企业','value'=>0),
			array('name'=>'其他机构','value'=>0)
			);

		foreach ($GdsrUnique as $key => $value) {
		 $value=preg_replace('/（.*?）/', "", $value);
			if (strlen($value)>9) {
			    $pos1=strpos($value,"公司");
				$pos2=strpos($value,"企业");
				$pos3=strpos($value,"厂");
			    $pos4=strpos($value,"店");
			    $pos5=strpos($value,"超市");
				if($pos1!==false||$pos2!==false||$pos3!==false||$pos4!==false||$pos5!==false){
					$arr[0]['value']++;
				}else {
					$arr[1]['value']++;
				}
			}
		}
		// print_r($arr);
		return $arr;
	}
//===============================================判断法院报告中守方当事人类型【企业or其他】
	function isSCompany($rst){
        
        $SdsrArr=array();

		foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrS=array();
				if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent'){
			    $ministrS=array_merge($ministrS,array($val1['name']));
				$SdsrArr=array_merge($SdsrArr,$ministrS);
                }
			 }
		}

		//攻方当事人名单去重
		$SdsrUniqueRaw=array_unique($SdsrArr);
		foreach ($SdsrUniqueRaw as $key => $value) {
		$SdsrUnique[]=$value;
		}

		$arr=array(
			array('name'=>'企业','value'=>0),
			array('name'=>'其他机构','value'=>0)
			);

		foreach ($SdsrUnique as $key => $value) {
		 $value=preg_replace('/（.*?）/', "", $value);
			if (strlen($value)>9) {
			    $pos1=strpos($value,"公司");
				$pos2=strpos($value,"企业");
				$pos3=strpos($value,"厂");
			    $pos4=strpos($value,"店");
			    $pos5=strpos($value,"超市");
				if($pos1!==false||$pos2!==false||$pos3!==false||$pos4!==false||$pos5!==false){
					$arr[0]['value']++;
				}else {
					$arr[1]['value']++;
				}
			}
		}
		// print_r($arr);
		return $arr;
	}
//===============================================获取攻方机构当事人案件数量排行前五
     function getGjgdsrno5($rst){
			
			$GdsrArr=array();
		    foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrG=array();
				if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer'){
			    $ministrG=array_merge($ministrG,array($val1['name']));
				$GdsrArr=array_merge($GdsrArr,$ministrG);
                }
			 }
		  }

			$Gjgdsr=array();
		    foreach ($GdsrArr as $key2 => $value2) {
             $value3=preg_replace('/（.*?）/', "", $value2);
			if (strlen($value3)>9) {
            array_push($Gjgdsr,$value3);
             }
         }
            $GjgdsrCountValues=array_count_values($Gjgdsr);
            arsort($GjgdsrCountValues);
            
          return $GjgdsrCountValues;
}

//===============================================获取守方机构当事人案件数量排行前五
     function getSjgdsrno5($rst){
			
			$SdsrArr=array();
		    foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministrS=array();
				if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent'){
			    $ministrS=array_merge($ministrS,array($val1['name']));
				$SdsrArr=array_merge($SdsrArr,$ministrS);
                }
			 }
		  }

			$Sjgdsr=array();
		    foreach ($SdsrArr as $key2 => $value2) {
            $value3=preg_replace('/（.*?）/', "", $value2);
			if (strlen($value3)>9) {
            array_push($Sjgdsr,$value3);
             }
         }
            $SjgdsrCountValues=array_count_values($Sjgdsr);
            arsort($SjgdsrCountValues);
            
          return $SjgdsrCountValues;
}


//===============================================获取法院报告当事人（区分机构和个人）案件胜率情况
	function getgspanjueresult($rst){
          
    //==================================判决案件数据提取
		  $GpanjuewinArr=array();
		  $SpanjuewinArr=array();
          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
         
          foreach ($party as $key1 => $val1) {
		  $ministr1=array();
				if ($val1['type']=='plaintiff'&&$doctype=="判决"){
			    $ministr1=array_merge($ministr1,array($val1['name']));
				$GpanjuewinArr=array_merge($GpanjuewinArr,$ministr1);
                }
			 
		  $ministr2=array();
				if ($val1['type']=='defendant'&&$doctype=="判决"){
			    $ministr2=array_merge($ministr2,array($val1['name']));
				$SpanjuewinArr=array_merge($SpanjuewinArr,$ministr2);
                }
			 }
          }
 	
		  $arr1=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));

		     foreach ($GpanjuewinArr as $key => $value) {
		        $value=preg_replace('/（.*?）/', "", $value);
				  if (strlen($value)>9) {
					$arr1[0]['value']++;
				  }else{
					$arr1[1]['value']++;
				  }
			  }
          
		  $arr2=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SpanjuewinArr as $key => $value) {
		        $value=preg_replace('/（.*?）/', "", $value);
				  if (strlen($value)>9) {
					$arr2[0]['value']++;
				  }else{
					$arr2[1]['value']++;
				  }
			  }
			  $panjuetotalArr=array_merge($arr1,$arr2);
			  // print_r($panjuetotalArr);
			 
           //================================================================胜诉案件数据提取
          
          $GsspanjuewinArr=array();
		  $SsspanjuewinArr=array();
          foreach ($rst as $key => $val) {
		  $fid=$val['_id'];
		  $party=$val['party'];
		  $agent=$val['agent'];
          $winner=$val['winner'];
          $doctype=$val['doctype'];
          foreach ($party as $key1 => $val1) {
		  $ministr3=array();
				if ($val1['type']=='plaintiff'&&$doctype=="判决"&&$winner=="攻方"){
			    $ministr3=array_merge($ministr1,array($val1['name']));
				$GsspanjuewinArr=array_merge($GsspanjuewinArr,$ministr3);
                }
			 
		  $ministr4=array();
				if ($val1['type']=='defendant'&&$doctype=="判决"&&$winner=="守方"){
			    $ministr4=array_merge($ministr4,array($val1['name']));
				$SsspanjuewinArr=array_merge($SsspanjuewinArr,$ministr4);
                }
			 }
          }
 	
		  $arr3=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));

		     foreach ($GsspanjuewinArr as $key => $value) {
		        $value=preg_replace('/（.*?）/', "", $value);
				  if (strlen($value)>9) {
					$arr3[0]['value']++;
				  }else{
					$arr3[1]['value']++;
				  }
			  }
          
		  $arr4=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SsspanjuewinArr as $key => $value) {
		        $value=preg_replace('/（.*?）/', "", $value);
				  if (strlen($value)>9) {
					$arr4[0]['value']++;
				  }else{
					$arr4[1]['value']++;
				  }
			  }
   //==========================================合成一个数组以便在class文件中提取数据
			  $arr=array_merge($arr1,$arr2,$arr3,$arr4);
			  return ($arr);

       }
       //==========================================获取法院报告机构当事人案件数量排行前5案由分布
       function getJGno5casetype($rst){
            $dsrArr=array();
		    foreach ($rst as $key => $val) {
			$fid=$val['_id'];
			$party=$val['party'];
			$agent=$val['agent'];

			foreach ($party as $key1 => $val1) {
				$ministr=array();
			    $ministr=array_merge($ministr,array($val1['name']));
				$dsrArr=array_merge($dsrArr,$ministr);
                }
			 }
		  
			$jgdsr=array();
		    foreach ($dsrArr as $key2 => $value2) {
            $value2=preg_replace('/（.*?）/', "", $value2);
			if (strlen($value2)>9) {
            array_push($jgdsr,$value2);
            }
         }
            $jgdsrCountValues=array_count_values($jgdsr);
            arsort($jgdsrCountValues);
            
            // print_r($jgdsrCountValues);

            //array_slice函数——截取数组中的前五个值，0为开始的位置，截取5个值
            $arrno5=array_slice($jgdsrCountValues,0,5);
           
            $arrno5new=array();
             foreach ($arrno5 as $key6 => $value6) {
		    	array_push($arrno5new, array('value'=>$value6,'name'=>$key6));
		     }
            // print_r($arrno5new);

            //作为攻方时的胜诉案件案由
            $arr4=array($arrno5new[0]['name'],$arrno5new[1]['name'],$arrno5new[2]['name'],$arrno5new[3]['name'],$arrno5new[4]['name']);
            $arrtype4=array();
              foreach ($rst as $key10 => $val10) {
	            $fid3=$val10['_id'];
				$party3=$val10['party'];
		        $doctype2=$val10['doctype'];
		        $winner=$val10['winner'];
		        $thirdtag=$val10['thirdtag'];
		          foreach ($party3 as $key11 => $val11) {
		          	 foreach ($arr4 as $key12 => $val12) {
		          		$ministr1=array();
		        		if ($val11['type']=='plaintiff'&&$doctype2=="判决"&&$val11['name']==$val12&&$winner=='攻方') {
			            	$ministr1[$val12]=$thirdtag;
			            	// $ministr1=array_merge($ministr1,array($val12,$thirdtag));
				            $arrtype4=array_merge_recursive($arrtype4,$ministr1);
		        	       }
		                }
		            }
			   }
               // print_r($arrtype4);
            // $arrtype4values=array_count_values($arrtype4);
            // arsort($arrtype4values);
            // print_r($arrtype4values);

   //          //作为守方时的胜诉案件案由

            $arr5=array($arrno5new[0]['name'],$arrno5new[1]['name'],$arrno5new[2]['name'],$arrno5new[3]['name'],$arrno5new[4]['name']);
            $arrtype5=array();
              foreach ($rst as $key13 => $val13) {
	            $fid4=$val13['_id'];
				$party4=$val13['party'];
		        $doctype3=$val13['doctype'];
		        $winner1=$val13['winner'];
		        $thirdtag1=$val13['thirdtag'];
		          foreach ($party4 as $key14 => $val14) {
		          	 foreach ($arr5 as $key15 => $val15) {
		          		$ministr2=array();
		        		if ($val14['type']=='defendant'&&$doctype3=="判决"&&$val14['name']==$val15&&$winner1=='守方') {
			            	$ministr2[$val15]=$thirdtag1;
			            	// $ministr1=array_merge($ministr1,array($val12,$thirdtag));
				            $arrtype5=array_merge_recursive($arrtype5,$ministr2);
		        	       }
		                }
		            }
			   }

            $arr1=array_merge($arrtype4,$arrtype5);
	        // print_r($arr1);
			   	
			   	$W=array();
	             foreach ($arr1 as $k1 => $v1) {
				    $Q=sizeof($v1);
				    $W[$k1]=$Q;
				 }
				$thirdtagArr1=array();
				$thirdtagArr2=array();
				$thirdtagArr3=array();
				$thirdtagArr4=array();
				$thirdtagArr5=array();
				 	$E=array_keys($W);
					foreach ($arr1 as $k4 => $v4) {
						if (count($v4)<=1) {
							$v4=array($v4);
						}
						if ($E[0]==$k4) {
							$thirdtagArr1=array_merge($thirdtagArr1, $v4);
					    }else if ($E[1]==$k4) {
							$thirdtagArr2=array_merge($thirdtagArr2, $v4);
					    }else if ($E[2]==$k4) {
							$thirdtagArr3=array_merge($thirdtagArr3, $v4);
						}else if ($E[3]==$k4) {
							$thirdtagArr4=array_merge($thirdtagArr4, $v4);
					    }else if ($E[4]==$k4) {
							$thirdtagArr5=array_merge($thirdtagArr5, $v4);
					    }	
					}		

					$courtthirdtag1=array_count_values($thirdtagArr1);
					$courtthirdtag2=array_count_values($thirdtagArr2);
					$courtthirdtag3=array_count_values($thirdtagArr3);
					$courtthirdtag4=array_count_values($thirdtagArr4);
					$courtthirdtag5=array_count_values($thirdtagArr5);

					// $lawfirmtagArr2=array_count_values($tagArr2);
					// $lawfirmtagArr3=array_count_values($tagArr3);

					// arsort($courtthirdtag1);
					// arsort($courtthirdtag2);
					// arsort($courtthirdtag3);
					// arsort($courtthirdtag4);
					// arsort($courtthirdtag5);

					$arr10=array();
					$arr20=array();
					$arr30=array();
					$arr40=array();
					$arr50=array();


				    foreach ($courtthirdtag1 as $k2 => $v2) {
				    array_push($arr10, array('value'=>$v2,'name'=>$k2));
				    }
					
					foreach ($courtthirdtag2 as $k2 => $v2) {
				    array_push($arr20, array('value'=>$v2,'name'=>$k2));
					}
						
					foreach ($courtthirdtag3 as $k2 => $v2) {
				    array_push($arr30, array('value'=>$v2,'name'=>$k2));
					}
					foreach ($courtthirdtag4 as $k2 => $v2) {
				    array_push($arr40, array('value'=>$v2,'name'=>$k2));
					}
					foreach ($courtthirdtag5 as $k2 => $v2) {
				    array_push($arr50, array('value'=>$v2,'name'=>$k2));
					}		

                   // print_r($arr10);
                   // print_r($arr20);
                   // print_r($arr30);
                   // print_r($arr40);
                   // print_r($arr50);
                $arrfinal=array($arr10,$arr20,$arr30,$arr40,$arr50);
                return ($arrfinal);
		}


//==========================================================获取法院报告审理案件数量排名第一法官的判决和胜诉案件
		function getno1gspanjueresult($rst){
          
        //获取法官总集合
		$judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
		
	   // print_r($judgeTotal);

        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //截取前三名法官姓名及审理的案件数量，以便填写subtext
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        
        //===================================获取法院报告top1法官审理案件判决案件、胜诉案件分布

        //判决案件数据提取
          $GpanjuewinArr=array();
		  $SpanjuewinArr=array();
          foreach ($rst as $key2 => $val2) {
			  $fid=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $judgetotal=$val2['judges'];
                   
              foreach ($party as $key3 => $val3) {
                foreach ($judgetotal as $key4 => $val4) {
			        $ministr1=array();
					if ($val3['type']=='plaintiff'&&$doctype=="判决"&&$val4['name']==$arr1[0]){
					    $ministr1=array_merge($ministr1,array($val3['name']));
						$GpanjuewinArr=array_merge($GpanjuewinArr,$ministr1);
	                }
		            $ministr2=array();
					if ($val3['type']=='defendant'&&$doctype=="判决"&&$val4['name']==$arr1[0]){
					    $ministr2=array_merge($ministr2,array($val3['name']));
						$SpanjuewinArr=array_merge($SpanjuewinArr,$ministr2);
	                }
				 }
	          }
 	       }
		  $arrno_1=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));

		     foreach ($GpanjuewinArr as $key5 => $value5) {
		        $value5=preg_replace('/（.*?）/', "", $value5);
				  if (strlen($value5)>9) {
					$arrno_1[0]['value']++;
				  }else{
					$arrno_1[1]['value']++;
				  }
			  }
          
		  $arrno_2=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SpanjuewinArr as $key6 => $val6) {
		        $val6=preg_replace('/（.*?）/', "", $val6);
				  if (strlen($val6)>9) {
					$arrno_2[0]['value']++;
				  }else{
					$arrno_2[1]['value']++;
				  }
			  }

            //胜诉案件数据提取
            $GsspanjuewinArr=array();
		    $SsspanjuewinArr=array();
            foreach ($rst as $key7 => $val7) {
			  $fid1=$val['_id'];
			  $party1=$val7['party'];
			  $agent1=$val7['agent'];
	          $winner1=$val7['winner'];
	          $doctype1=$val7['doctype'];
	          $judgetotal1=$val7['judges'];
                foreach ($party1 as $key8 => $val8) {
                  foreach ($judgetotal1 as $key9 => $val9) {
			  		$ministr3=array();
					if ($val8['type']=='plaintiff'&&$doctype1=="判决"&&$winner1=="攻方"&&$val9['name']==$arr1[0]){
				    $ministr3=array_merge($ministr1,array($val8['name']));
					$GsspanjuewinArr=array_merge($GsspanjuewinArr,$ministr3);
	                }
	                // print_r($GsspanjuewinArr);
		            $ministr4=array();
					if ($val8['type']=='defendant'&&$doctype1=="判决"&&$winner1=="守方"&&$val9['name']==$arr1[0]){
				    $ministr4=array_merge($ministr4,array($val8['name']));
					$SsspanjuewinArr=array_merge($SsspanjuewinArr,$ministr4);
				    } 
                  }
			   }
            }

            $arrno_3=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));
		     foreach ($GsspanjuewinArr as $key10 => $val10) {
		          $val10=preg_replace('/（.*?）/', "", $val10);
				  if (strlen($val10)>9) {
					$arrno_3[0]['value']++;
				  }else{
					$arrno_3[1]['value']++;
				  }
			 }

		  $arrno_4=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SsspanjuewinArr as $key11 => $val11) {
		        $value=preg_replace('/（.*?）/', "", $val11);
				  if (strlen($val11)>9) {
					$arrno_4[0]['value']++;
				  }else{
					$arrno_4[1]['value']++;
				  }
			  }

			  $arrfinal=array_merge($arrno_1,$arrno_2,$arrno_3,$arrno_4);
              $arrfinalmost=array_merge($arrfinal,$arr1,$arr1000);
			  return $arrfinalmost;
       }

//==========================================================获取法院报告审理案件数量排名第二法官的判决和胜诉案件
		function getno2gspanjueresult($rst){
          
        //获取法官总集合
		$judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
		
	   // print_r($judgeTotal);

        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //截取前三名法官姓名及审理的案件数量，以便填写subtext
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        
        //===================================获取法院报告top2法官审理案件判决案件、胜诉案件分布

        //判决案件数据提取
          $GpanjuewinArr=array();
		  $SpanjuewinArr=array();
          foreach ($rst as $key2 => $val2) {
			  $fid=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $judgetotal=$val2['judges'];
                   
              foreach ($party as $key3 => $val3) {
                foreach ($judgetotal as $key4 => $val4) {
			        $ministr1=array();
					if ($val3['type']=='plaintiff'&&$doctype=="判决"&&$val4['name']==$arr1[1]){
					    $ministr1=array_merge($ministr1,array($val3['name']));
						$GpanjuewinArr=array_merge($GpanjuewinArr,$ministr1);
	                }
		            $ministr2=array();
					if ($val3['type']=='defendant'&&$doctype=="判决"&&$val4['name']==$arr1[1]){
					    $ministr2=array_merge($ministr2,array($val3['name']));
						$SpanjuewinArr=array_merge($SpanjuewinArr,$ministr2);
	                }
				 }
	          }
 	       }
		  $arrno_1=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));

		     foreach ($GpanjuewinArr as $key5 => $value5) {
		        $value5=preg_replace('/（.*?）/', "", $value5);
				  if (strlen($value5)>9) {
					$arrno_1[0]['value']++;
				  }else{
					$arrno_1[1]['value']++;
				  }
			  }
          
		  $arrno_2=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SpanjuewinArr as $key6 => $val6) {
		        $val6=preg_replace('/（.*?）/', "", $val6);
				  if (strlen($val6)>9) {
					$arrno_2[0]['value']++;
				  }else{
					$arrno_2[1]['value']++;
				  }
			  }

            //胜诉案件数据提取
            $GsspanjuewinArr=array();
		    $SsspanjuewinArr=array();
            foreach ($rst as $key7 => $val7) {
			  $fid1=$val['_id'];
			  $party1=$val7['party'];
			  $agent1=$val7['agent'];
	          $winner1=$val7['winner'];
	          $doctype1=$val7['doctype'];
	          $judgetotal1=$val7['judges'];
                foreach ($party1 as $key8 => $val8) {
                  foreach ($judgetotal1 as $key9 => $val9) {
			  		$ministr3=array();
					if ($val8['type']=='plaintiff'&&$doctype1=="判决"&&$winner1=="攻方"&&$val9['name']==$arr1[1]){
				    $ministr3=array_merge($ministr1,array($val8['name']));
					$GsspanjuewinArr=array_merge($GsspanjuewinArr,$ministr3);
	                }
	                // print_r($GsspanjuewinArr);
		            $ministr4=array();
					if ($val8['type']=='defendant'&&$doctype1=="判决"&&$winner1=="守方"&&$val9['name']==$arr1[1]){
				    $ministr4=array_merge($ministr4,array($val8['name']));
					$SsspanjuewinArr=array_merge($SsspanjuewinArr,$ministr4);
				    } 
                  }
			   }
            }

            $arrno_3=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));
		     foreach ($GsspanjuewinArr as $key10 => $val10) {
		          $val10=preg_replace('/（.*?）/', "", $val10);
				  if (strlen($val10)>9) {
					$arrno_3[0]['value']++;
				  }else{
					$arrno_3[1]['value']++;
				  }
			 }

		  $arrno_4=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SsspanjuewinArr as $key11 => $val11) {
		        $value=preg_replace('/（.*?）/', "", $val11);
				  if (strlen($val11)>9) {
					$arrno_4[0]['value']++;
				  }else{
					$arrno_4[1]['value']++;
				  }
			  }

			  $arrfinal=array_merge($arrno_1,$arrno_2,$arrno_3,$arrno_4);
              $arrfinalmost=array_merge($arrfinal,$arr1,$arr1000);
			  return $arrfinalmost;
       }
    //===================================获取法院报告top3法官审理案件判决案件、胜诉案件分布
    function getno3gspanjueresult($rst){
          
        //获取法官总集合
		$judgeTotal=array();
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$judgetotal=$val['judges'];
			foreach ($judgetotal as $key1 => $val1) {
				
					// unset($value['court']);
				   array_push($judgeTotal,$val1['name']);
				}
			}
		
	   // print_r($judgeTotal);

        $arr=array_count_values($judgeTotal);
        arsort($arr);
        //截取前三名法官姓名及审理的案件数量，以便填写subtext
        $arr0=array_keys($arr);
        $arr1=array_slice($arr0,0,3);
        $arr0000=array_values($arr);
        $arr1000=array_slice($arr0000,0,3);
        
        //===================================获取法院报告top3法官审理案件判决案件、胜诉案件分布

        //判决案件数据提取
          $GpanjuewinArr=array();
		  $SpanjuewinArr=array();
          foreach ($rst as $key2 => $val2) {
			  $fid=$val2['_id'];
			  $party=$val2['party'];
			  $agent=$val2['agent'];
	          $winner=$val2['winner'];
	          $doctype=$val2['doctype'];
	          $judgetotal=$val2['judges'];
                   
              foreach ($party as $key3 => $val3) {
                foreach ($judgetotal as $key4 => $val4) {
			        $ministr1=array();
					if ($val3['type']=='plaintiff'&&$doctype=="判决"&&$val4['name']==$arr1[2]){
					    $ministr1=array_merge($ministr1,array($val3['name']));
						$GpanjuewinArr=array_merge($GpanjuewinArr,$ministr1);
	                }
		            $ministr2=array();
					if ($val3['type']=='defendant'&&$doctype=="判决"&&$val4['name']==$arr1[2]){
					    $ministr2=array_merge($ministr2,array($val3['name']));
						$SpanjuewinArr=array_merge($SpanjuewinArr,$ministr2);
	                }
				 }
	          }
 	       }
		  $arrno_1=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));

		     foreach ($GpanjuewinArr as $key5 => $value5) {
		        $value5=preg_replace('/（.*?）/', "", $value5);
				  if (strlen($value5)>9) {
					$arrno_1[0]['value']++;
				  }else{
					$arrno_1[1]['value']++;
				  }
			  }
          
		  $arrno_2=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SpanjuewinArr as $key6 => $val6) {
		        $val6=preg_replace('/（.*?）/', "", $val6);
				  if (strlen($val6)>9) {
					$arrno_2[0]['value']++;
				  }else{
					$arrno_2[1]['value']++;
				  }
			  }

            //胜诉案件数据提取
            $GsspanjuewinArr=array();
		    $SsspanjuewinArr=array();
            foreach ($rst as $key7 => $val7) {
			  $fid1=$val['_id'];
			  $party1=$val7['party'];
			  $agent1=$val7['agent'];
	          $winner1=$val7['winner'];
	          $doctype1=$val7['doctype'];
	          $judgetotal1=$val7['judges'];
                foreach ($party1 as $key8 => $val8) {
                  foreach ($judgetotal1 as $key9 => $val9) {
			  		$ministr3=array();
					if ($val8['type']=='plaintiff'&&$doctype1=="判决"&&$winner1=="攻方"&&$val9['name']==$arr1[2]){
				    $ministr3=array_merge($ministr1,array($val8['name']));
					$GsspanjuewinArr=array_merge($GsspanjuewinArr,$ministr3);
	                }
	                // print_r($GsspanjuewinArr);
		            $ministr4=array();
					if ($val8['type']=='defendant'&&$doctype1=="判决"&&$winner1=="守方"&&$val9['name']==$arr1[2]){
				    $ministr4=array_merge($ministr4,array($val8['name']));
					$SsspanjuewinArr=array_merge($SsspanjuewinArr,$ministr4);
				    } 
                  }
			   }
            }

            $arrno_3=array(array('name'=>'攻方（机构）','value'=>0),array('name'=>'攻方（个人）','value'=>0));
		     foreach ($GsspanjuewinArr as $key10 => $val10) {
		          $val10=preg_replace('/（.*?）/', "", $val10);
				  if (strlen($val10)>9) {
					$arrno_3[0]['value']++;
				  }else{
					$arrno_3[1]['value']++;
				  }
			 }

		  $arrno_4=array(array('name'=>'守方（机构）','value'=>0),array('name'=>'守方（个人）','value'=>0));

		     foreach ($SsspanjuewinArr as $key11 => $val11) {
		        $value=preg_replace('/（.*?）/', "", $val11);
				  if (strlen($val11)>9) {
					$arrno_4[0]['value']++;
				  }else{
					$arrno_4[1]['value']++;
				  }
			  }

			  $arrfinal=array_merge($arrno_1,$arrno_2,$arrno_3,$arrno_4);
              $arrfinalmost=array_merge($arrfinal,$arr1,$arr1000);
			  return $arrfinalmost;
       }


}