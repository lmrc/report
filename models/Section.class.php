<?php

/**
* 这种分段方法是将全文通过换行符分割，添加到一个数组中
* 遍历数组，针对每个元素进行分析，将其归入相应的段落。
*/
class Section
{
	
	function __construct($case)
	{
		$this->case=$case;
		// try {
		// 	$conn = new MongoClient();
		// 	// $conn = new MongoClient('mongodb://192.168.170.33');
		// 	$db = $conn ->$database_name;
		// 	$this->test = $db->$collection_name;
		// 	$arr=array();
		// 	$this->rst=$this->test->find($arr);
		// } catch (Exception $e) {
		// 	file_put_contents('c:/dberror.log', $e,FILE_APPEND);			
		// } 
	}

	//=======================================================截取start
	function setDevide(){
		foreach ($this->case as $key=>$val) {
			$doc=$val['doc'];

			$arr = explode("\n", $doc);

			//法院名称
			$courtStr='';
			//文书类型
			$wenshuStr='';
			//案号
			$casenumStr='';
			//当事人
			$subjectArr=array();
			//审理经过
			$procedureArr=array();
			//诉辩称
			$subianArr=array();
			//事实认定
			$factArr=array();
			//法院意见
			$reasonArr=array();
			//裁判结果
			$resultArr=array();
			//审判人员
			$judgeArr=array();
			//裁判日期
			$timeStr='';
			//书记员
			$writerArr=array();
			// $endArr=array();

			$flag=0;
			foreach ($arr as $v) {
				
				if ($flag==0&&$this->judgeFinish($v,array('院'))) {
					$v=str_replace('中华人民共和国', '', $v);
					$courtStr=$v;
					$flag=1;
				}else if ($flag==1&&$this->judgeFinish($v,array('书'))) {
					$wenshuStr=$v;
					$flag=2;
				}else if ($flag==2&&$this->judgeFinish($v,array('号'))) {
					$casenumStr=$v;
					$flag=3;
				}else if ($flag==3){
					//判断审理经过的关键词
					$bool1 = $this->judgeExists($v,array('审理','受理','本院','我院'));
					//判断裁判结果的关键词（注：裁判结果在这些关键词的下一段）
					//有些裁定文书较短，审理经过与法院意见可能合并在一段
					$bool2 = $this->judgeFinish($v,array("裁定如下：","判决如下："));
					if ($bool2) {
						$procedureArr[]=$v;
						$flag=7;
					}else if ($bool1) {
						$procedureArr[]=$v;
						$flag=4;
					}else {
						$subjectArr[]=$v;
					}
				}else if ($flag==4){
					//判断事实认定的关键词
					$bool1 = $this->judgeBegin($v,array('经审理查明','本院查明','本院认定','经重审查明','经审理本院查明','经审理本院认定','本院经审理查明','本院经审理认定','经本院审理查明','经本院审理认定','上述事实','以上事实'));
					//判断法院意见的关键词
					$bool2 = $this->judgeExists($v,array('本院认为','本院经审理认为','本院经审查认为','经本院审查认为','经本院审理认为','本院经二审审理后认为','本院经再审审理后认为','本院再审认为','重审认为','本院二审认为','本院经查证','合议庭认为','本院的评判','本院审理认为','本院审查认为','本院经审判委员会讨论认为','逐一分析如下','合议庭评议认为','合议庭综合评判如下','判决结果'));
					//判断裁判结果的关键词（注：裁判结果在这些关键词的下一段）
					//还有一种特殊情况——“判决如上”
					$bool3 = $this->judgeFinish($v,array("判决如下：","裁定如下：","决定如下：","判决如：","裁定如：",'判决以下：','裁定以下：','判处如下：','判决意见如下：','裁定意见如下：'));
					if ($bool3) {
						$reasonArr[]=$v;
						$flag=7;
					}else if ($bool2) {
						$reasonArr[]=$v;
						$flag=6;
					}else if ($bool1) {
						$factArr[]=$v;
						$flag=5;
					}else{
						$subianArr[]=$v;
					}
				}else if ($flag==5){
					//判断法院意见的关键词
					$bool1 = $this->judgeExists($v,array('本院认为','本院经审理认为','本院经审查认为','经本院审查认为','经本院审理认为','本院经二审审理后认为','本院经再审审理后认为','本院再审认为','重审认为','本院二审认为','本院经查证','合议庭认为','本院的评判','本院审理认为','本院审查认为','本院经审判委员会讨论认为','逐一分析如下','合议庭评议认为','合议庭综合评判如下','判决结果'));
					//判断裁判结果的关键词（注：裁判结果在这些关键词的下一段）
					$bool2 = $this->judgeFinish($v,array("判决如下：","裁定如下：","决定如下：","判决如：","裁定如：",'判决以下：','裁定以下：','判处如下：','判决意见如下：','裁定意见如下：'));
					if ($bool2) {
						$reasonArr[]=$v;
						$flag=7;
					}else if($bool1){
						$reasonArr[]=$v;
						$flag=6;
					}else{
						$factArr[]=$v;
					}
				}else if ($flag==6){
					//判断裁判结果的关键词（注：裁判结果在这些关键词的下一段）
					$bool = $this->judgeFinish($v,array("判决如下：","裁定如下：","决定如下：","判决如：","裁定如：",'判决以下：','裁定以下：','判处如下：','判决意见如下：','裁定意见如下：'));
					if ($bool) {
						$reasonArr[]=$v;
						$flag=7;
					}else{
						$reasonArr[]=$v;

					}
				}else if ($flag==7){
					//判断审判人员的关键词
					$bool = $this->judgeBegin($v,array("审判长","审判员","代理审判员","执行员"));
					if ($bool) {
						$judgeArr[]=$v;
						$flag=8;
					} else {
						$resultArr[]=$v;
					}
				}else if ($flag==8){
					//判断裁判日期的关键词
					$bool = $this->judgeBegin($v,array("二"));
					if ($bool) {
						$timeStr=$v;
						$flag=9;
					} else {
						$judgeArr[]=$v;
					}
				}else if ($flag==9){
					//判断书记员的关键词
					$bool = $this->judgeExists($v,array("书记员"));
					if ($bool) {
						$writerArr[]=$v;
						$flag=10;
					}
				}
			}

			// echo "<br>court=";
			// print_r($courtStr);
			// echo "<br>wenshu=";
			// print_r($wenshuStr);
			// echo "<br>casenum=";
			// print_r($casenumStr);
			// echo "<br>subject=";
			// print_r($subjectArr);
			// echo "<br>proceduer=";
			// print_r($procedureArr);
			// echo "<br>subian=";
			// print_r($subianArr);
			// echo "<br>fact=";
			// print_r($factArr);
			// echo "<br>reason=";
			// print_r($reasonArr);
			// echo "<br>result=";
			// print_r($resultArr);
			// echo "<br>judge=";
			// print_r($judgeArr);
			// echo "<br>date=";
			// print_r($dateStr);
			// echo "<br>writer=";
			// print_r($writerArr);
			// echo "<br>";

			$this->case[$key]['court']=$courtStr;
			$this->case[$key]['wenshu']=$wenshuStr;
			$this->case[$key]['casenum']=$casenumStr;
			$this->case[$key]['time']=$timeStr;
			$this->case[$key]['subject']=$subjectArr;
			$this->case[$key]['procedure']=$procedureArr;
			$this->case[$key]['subian']=$subianArr;
			$this->case[$key]['fact']=$factArr;
			$this->case[$key]['reason']=$reasonArr;
			$this->case[$key]['result']=$resultArr;
			$this->case[$key]['judge']=$judgeArr;
			$this->case[$key]['writer']=$writerArr;

			// unset($this->case[$key]['doc']);
			//上传数据库
			// $sarr=array('_id'=>$fid);
			// $darr=array('$set'=>array('court'=>$courtStr,'wenshu'=>$wenshuStr,'casenum'=>$casenumStr,'time'=>$timeStr,'subject'=>$subjectArr,'proceduer'=>$procedureArr,'subian'=>$subianArr,'fact'=>$factArr,'reason'=>$reasonArr,'result'=>$resultArr,'judge'=>$judgeArr,'writer'=>$writerArr));
			// $opts=array(false,true);
			// $this->test->update($sarr,$darr,$opts);
		}
		return $this->case;
	}
	//============================================判断是否存在某些关键词
	protected function judgeExists($para,$arr){
		$res=false;
		foreach ($arr as $key => $val) {
			$res=$res||(mb_strpos($para, $val)!==false);
		}
		return $res;
	}
	//============================================判断是否以某些关键词开头
	protected function judgeBegin($para,$arr){
		$res=false;
		foreach ($arr as $key => $val) {
			$res=$res||(mb_strpos($para, $val)===0);
		}
		return $res;
	}
	//============================================判断是否以某些关键词结尾
	protected function judgeFinish($para,$arr){
		$res=false;
		foreach ($arr as $key => $val) {
			$res=$res||(mb_strpos($para, $val)===mb_strlen($para)-mb_strlen($val));
		}
		return $res;
	}

}
