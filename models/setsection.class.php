<?php

/**
* 
*/
class SetSection
{
	
	function __construct()
	{
		# code...
	}

	//=======================================================截取start
	function setStart($rst,$test){
		foreach ($rst as $val) {
			// echo '<br>H';
			$fid=$val['_id'];
			$key=$val['key'];
			$key=str_replace(' ', '', $key);
			$pos=strpos($key,"号\n");
			$start = substr($key,0,$pos+3);	
			// echo $start;

			$sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('start'=>$start));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);
		}

	}

	//=======================================================截取subject
	function setSubject($rst,$test){
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];

			$pos1=strpos($key, "案号：");

			$minPos1=strpos($key, "审理")?strpos($key, "审理"):strlen($key);
			$minPos2=strpos($key, "受理")?strpos($key, "受理"):strlen($key);
			$minPos3=strpos($key, "本院")?strpos($key, "本院"):strlen($key);

			$minPos=min($minPos1,$minPos2,$minPos3);

			$pos2=$minPos;
			$sub1=substr($key,$pos1,$pos2-$pos1);
			$pos3=strpos($sub1, "\n");
			$pos4=strrpos($sub1, "\n");
			$sub2=substr($sub1,$pos3,$pos4-$pos3);

			$subject=preg_replace('/[\s]{2,}/', "\n", $sub2);

			$sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('subject'=>$subject));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);

		}

	}

	//=======================================================截取procedure
	function setProcedure($rst,$test){
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];
			$key=str_replace(' ', '', $key);

			$minPos1=strpos($key, "审理")?strpos($key, "审理"):strlen($key);
			$minPos2=strpos($key, "受理")?strpos($key, "受理"):strlen($key);
			$minPos3=strpos($key, "本院")?strpos($key, "本院"):strlen($key);

			$minPos=min($minPos1,$minPos2,$minPos3);

			$endpos=strpos($key,"\n",$minPos);   
			$sub=substr($key, 0,$minPos);
			$startpos=strrpos($sub,"\n");   

			$procedure = substr($key,$startpos,$endpos-$startpos);

			// echo $procedure;
			// echo "<hr>";

		  	$sarr=array('_id'=>$fid);
		  	$darr=array('$set'=>array('procedure'=>$procedure));
		  	$opts=array(false,true);
		  	$test->update($sarr,$darr,$opts);
		}

	}


	//=======================================================截取allegation和defence

	function setAllegationDefence($rst,$test){
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];
			$key=str_replace(' ', '', $key);
			$minPos1=strpos($key, "审理")?strpos($key, "审理"):strlen($key);
			$minPos2=strpos($key, "受理")?strpos($key, "受理"):strlen($key);
			$minPos3=strpos($key, "本院")?strpos($key, "本院"):strlen($key);

			$minPos=min($minPos1,$minPos2,$minPos3);

			if ($minPos!=0) {
				$startPos=strpos($key,"\n",$minPos); 
				if (strpos($key,"\n经审理查明")!=false) {
					$endPos=strpos($key,"\n经审理查明"); 
					$sub=substr($key,$startPos,$endPos-$startPos);
					// echo $sub;
					if (strpos($sub,"诉称")) {
						$key1=substr($sub,0,strpos($sub,"诉称"));
						$pos6=strrpos($key1, "\n");
						if(strpos($sub,"辩称")){
							$key2=substr($sub,0,strpos($sub,"辩称"));
							$pos7=strrpos($key2, "。");
							$allegation=substr($sub,$pos6,$pos7-$pos6+3);
							$defence=substr($sub,$pos7+3);
							
							$sarr=array('_id'=>$fid);
							$darr1=array('$set'=>array('allegation'=>$allegation));
							$darr2=array('$set'=>array('defence'=>$defence));
							$opts=array(false,true);
							$test->update($sarr,$darr1,$opts);
							$test->update($sarr,$darr2,$opts);
						}
					}
				}
			}else{
				echo "错误";
			}
		}

	}




	//=======================================================截取fact
	function setFact($rst,$test){
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];
			$key=str_replace(' ', '', $key);

			$pos1=strpos($key,"\n经审理查明");
			$pos2=strpos($key,"\n本院认为");

			$fact = substr($key,$pos1,$pos2-$pos1);

			$sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('fact'=>$fact));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);
		}

	}

	//=======================================================截取reason
	function setReason($rst,$test){
		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];
			$key=str_replace(' ', '', $key);

			$pos1=strpos($key,"\n本院认为");
			$pos2=strpos($key,"判决如下：\n");
			$pos3=strpos($key,"裁定如下：\n");

			if ($pos2!==false) {
			   $reason = substr($key,$pos1,$pos2-$pos1+12);
			}else if ($pos3!==false) {
			   $reason = substr($key,$pos1,$pos3-$pos1+12);
			}
			
			$reason=str_replace('?', '', $reason);
			
			$sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('reason'=>$reason));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);
		}

	}




	//=======================================================截取result
	function setResult($rst,$test){

		foreach ($rst as $val) {
			$fid=$val['_id'];
			$key=$val['key'];

		//获取开始位置
			$maxPos1=strpos($key, "判决如下：")?strrpos($key, "判决如下："):0;
			$maxPos2=strpos($key, "裁定如下：")?strrpos($key, "裁定如下："):0;

			$maxPos=max($maxPos1,$maxPos2);
			// echo $maxPos;
			// echo "<br>";

		//获取结束位置
			$minPos1=strpos($key, "审判长", $maxPos)?strpos($key, "审判长", $maxPos):strlen($key);
			$minPos2=strpos($key, "代理审判员", $maxPos)?strpos($key, "代理审判员", $maxPos):strlen($key);
			$minPos3=strpos($key, "审判员", $maxPos)?strpos($key, "审判员", $maxPos):strlen($key);

			$minPos=min($minPos1,$minPos2,$minPos3);
			// echo $minPos;
			// echo "<br>";

			
			$pos1=$maxPos;
			$pos2=$minPos;
			$sub1=substr($key,$pos1,$pos2-$pos1);

			$result=preg_replace('/[\s]{2,}/', "\n", $sub1);
			// echo $result;
			// echo "<hr>";

			$sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('result'=>$result));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);

		}

	}

	// ===================================================截取end
	function setEnd($rst,$test){
		foreach ($rst as $val) {
			// echo "<br>H";
			$fid=$val['_id'];
			$key=$val['key'];
			$key=preg_replace('/[\s]{2,}/', "\n", $key);
			// echo strrpos($key,"\n审判长")?"成功":"失败";
			// echo "<br>";
			// $pos1=strrpos($key,"书记员");
			if (strrpos($key,"附：")) {
				if(strrpos($key,"附：")<strrpos($key,"书记员")){
					$pos1=strrpos(substr($key, 0,strrpos($key, "附：")),"书记员");
				}else{
					$pos1=strrpos($key,"书记员");
				}
			} else {
				$pos1=strrpos($key,"书记员");
			}

		    $key1=substr($key,0,$pos1);
		    $pos2=strrpos($key1,"\n");
		    $key2=substr($key,$pos1);
		    $pos3=strpos($key2,"\n"); 
		    $end = substr($key,$pos2,$pos1+$pos3-$pos2);
		    // echo $end;

		    $end=strrpos($end,"附：")?substr($end, 0,strrpos($end, "附：")):$end;
		    $flag=strrpos(substr($end, 0,strrpos($end, "书记员")),"。");
		    $end=$flag?substr($end, $flag+3):$end;
		    $end=str_replace("本件与原本核对无异", "", $end);
		    // echo $end;
		    // echo "<br>";
		    $sarr=array('_id'=>$fid);
			$darr=array('$set'=>array('end'=>$end));
			$opts=array(false,true);
			$test->update($sarr,$darr,$opts);
		}

	}

}
