<?php


class Report
{
	public function __construct($case){
		//关闭所有问题提示
		error_reporting(0);
		
		$this->case=$case;

		$this->subName=$_SESSION['subName'];
		$this->subOffice=$_SESSION['subOffice'];
		$this->beginDate=$_SESSION['beginDate'];
		$this->finishDate=$_SESSION['finishDate'];
		$this->reportId=$_SESSION['reportId'];
		$this->totalCase=$_SESSION['totalCase'];
		$this->totalClient=$_SESSION['totalClient'];
		
	}

	public function lawyerReport(){

		$myInfo=new GetInfo($this->case);
		
		$lawyername = $this->subName;
		$officename = $this->subOffice;
		$wenshusource="中国裁判文书网";
		$judgedate=$this->beginDate."-".$this->finishDate;
		$sitescope="全国";
		$casefield="全部";
		$casecount=$this->totalCase;
		$clientcount=$this->totalClient;
		date_default_timezone_set('Asia/Shanghai'); 
		$lastvisit=date('Y年m月d日',time());

		$profile1=$lawyername."律师就职于".$officename."。该律师于".$this->beginDate."到".$this->finishDate."期间共代理".$casecount."件诉讼案件。";
		// $othersource="重庆市司法局网站、西部律师网";
		// $profile1="张晓平律师目前担任重庆碧涛律师事务所主任，于1993年通过全国律师资格统一考试（ 执业证号15001200610271127），执业状态为正常，无律师惩戒记录 。";
		// $profile2="张晓平律师毕业于西政法律系，拥有法律专业本科学历，擅长领域为民事、刑事案件，对法律顾问、合同审查、改制重组、股权纠纷以及兼并收购等均有较深的研究。";
		// $profile3="张晓平律师1994年6月开始于原涪陵地区律师事务所任专职律师；2000年国资所重庆市涪陵律师事务所转制为重庆圣石牛律师事务所后，任该所专职律师、合伙人；2003年6月任重庆元同律师事务所副主任（合伙人）；2012年开始在重庆碧涛律师事务所执业，并担任律所主任。目前，张晓平律师是多家企业、政府机关和其他组织的法律顾问。";
		
		$lawyerArr = array();

		//获取基本信息
		$lawyerArr['lawyername']=$lawyername;
		$lawyerArr['officename']=$officename;
		$lawyerArr['wenshusource']=$wenshusource;
		$lawyerArr['judgedate']=$judgedate;
		$lawyerArr['sitescope']=$sitescope;
		$lawyerArr['casefield']=$casefield;
		$lawyerArr['casecount']=$casecount;
		$lawyerArr['lastvisit']=$lastvisit;
		$lawyerArr['profile1']=$profile1;
		// $lawyerArr['othereource']=$othersource;
		// $lawyerArr['profile2']=$profile2;
		// $lawyerArr['profile3']=$profile3;

		//获取客户
		$clientArr = $myInfo->getLawyerClient();
		// file_put_contents('c:/client.txt', var_export($clientArr,true));
		// exit();

		$clientleixing=$myInfo->get1D('leixing',$clientArr);
		$lawyerArr['04A']=$clientleixing;

		//获取对方当事人
		$reverseArr = $myInfo->getLawyerReverseParty();
		$reverseleixing=$myInfo->get1D('leixing',$reverseArr);
		$lawyerArr['05A']=$reverseleixing;

		//获取案件类型
		$casetypeArr = $myInfo->get1D('casetype');
		$lawyerArr['06A']=$casetypeArr;

		$secondtagArr = $myInfo->get2D('casetype','secondtag');
		//获取民事细分案由
		$lawyerArr['06B']=$myInfo->fillEmpty(@$secondtagArr['民事']);
		//获取刑事细分案由
		$lawyerArr['06C']=$myInfo->fillEmpty(@$secondtagArr['刑事']);
		//获取行政细分案由
		$lawyerArr['06D']=$myInfo->fillEmpty(@$secondtagArr['行政']);
		//获取执行细分案由
		$lawyerArr['06E']=$myInfo->fillEmpty(@$secondtagArr['执行']);

		//获取地域分布
		$siteArr = $myInfo->get1D('site');
		foreach ($siteArr as $k => $v) {
			$siteArr[$k]['selected']=true;
		}
		$lawyerArr['07A']=$siteArr;

		//获取层级分布
		$rankArr = $myInfo->get1D('rank');
		$lawyerArr['07B']=$rankArr;

		//获取诉讼地位

		$statusArr = $myInfo->get1D('status');
		$lawyerArr['08A']=$statusArr;

		//获取诉讼地位2
		$substatusArr=$myInfo->get2D('status','level',$clientArr);
		$lawyerArr['08B']=$myInfo->fillEmpty(@$substatusArr['攻方']);
		$lawyerArr['08C']=$myInfo->fillEmpty(@$substatusArr['第三方']);
		$lawyerArr['08D']=$myInfo->fillEmpty(@$substatusArr['守方']);

		//获取文书类型
		$doctypeArr = $myInfo->get1D('doctype');
		$lawyerArr['09A']=$doctypeArr;

		$resultArr = $myInfo->get2D('doctype','outcome');
		//获取裁定裁判结果
		$lawyerArr['09B']=$myInfo->fillEmpty(@$resultArr['裁定']);
		//获取判决裁判结果
		$lawyerArr['10A']=$myInfo->fillEmpty(@$resultArr['判决']);

		$statusresult=$myInfo->get3D('doctype','status','outcome');

		//获取判决的裁判结果[攻方]
		$lawyerArr['10C']=$myInfo->fillEmpty(@$statusresult['判决']['攻方']);
		
		//获取判决的裁判结果[守方]
		$lawyerArr['10E']=$myInfo->fillEmpty(@$statusresult['判决']['守方']);
		
		$reslevelArr = $myInfo->get2D('doctype','level');
		// //获取裁定审理程序
		// $lawyerArr['08F']=$resultArr['裁定'];
		//获取判决审理程序
		$lawyerArr['10B']=$myInfo->fillEmpty(@$reslevelArr['判决']);

		$statuslevel=$myInfo->get3D('doctype','status','level');

		//获取判决的审理程序[攻方]
		$lawyerArr['10D']=$myInfo->fillEmpty(@$statuslevel['判决']['攻方']);
		
		//获取判决的审理程序[守方]
		$lawyerArr['10F']=$myInfo->fillEmpty(@$statuslevel['判决']['守方']);

		file_put_contents('../views/data/report_lawyer'.$this->reportId.'.txt', var_export($lawyerArr,true));
		file_put_contents('../views/data/report_lawyer'.$this->reportId.'.json', json_encode($lawyerArr));
		// return json_encode($lawyerArr);
	}

	public function companyReport(){

		$myInfo = new GetInfo($this->case);

		$companyname = $this->subName;
		$wenshusource="中国裁判文书网";
		$judgedate=$this->beginDate."-".$this->finishDate;
		$judgedate1="2014年1月1日到2014年12月31日";
		$sitescope="全国范围诉讼案件";
		$casefield="全部";
		$casecount=$this->totalCase;
		$clientcount=$this->totalClient;
		date_default_timezone_set('Asia/Shanghai'); 
		$lastvisit=date('Y年m月d日',time());

		// $profile1="深圳市腾讯计算机系统有限公司（以下简称“腾讯公司”）成立于1998年 11 月，是现今中国最大的互联网综合服务提供商之一，其业务涉及社交网络、互动娱乐、移动互联网、网络媒体、微信、企业发展、技术工程等多个领域。";
		// $gsxx_zch="440301103448669";
		// $gsxx_mc="深圳市腾讯计算机系统有限公司";
		// $gsxx_lx="有限责任公司";
		// $gsxx_fddbr="马化腾";
		// $gsxx_zczb="6500 万元人民币";
		// $gsxx_clrq="1998 年 11 月 11 日";
		// $gsxx_zs="深圳市南山区高新区高新南一路飞亚达大厦 5-10 楼";
		// $gsxx_yyqx="自 1998 年 11 月 11 日 至 2018 年 11 月 11 日";
		// $gsxx_jyfw="计算机软、硬件的设计、技术开发、销售（不含专营、专控、专卖商品及限制项目）；数据库及计算机网络服务、国内商业、物资供销业（不含专营、专控、专卖商品）；第二类增值电信业务中的信息服务业务（不含固定网电话信息服务和互联网信息服务，并按许可证B2-20090028号文办）；信息服务业务（仅限互联网信息服务业务，并按许可证粤B2-20090059号文办）；从事广告业务（法律、行政法规规定应进行广告经营审批等级的，另行办理审批登记后方可经营）；网络游戏出版运营（凭有效的新出网证（粤）字010号互联网出版许可证经营）；货物及技术进出口。";
		

		$companyArr = array();

		//获取基本信息
		$companyArr['companyname']=$companyname;
		$companyArr['wenshusource']=$wenshusource;
		$companyArr['judgedate']=$judgedate;
		$companyArr['sitescope']=$sitescope;
		$companyArr['casefield']=$casefield;
		$companyArr['casecount']=$casecount;
		$companyArr['lastvisit']=$lastvisit;

		// $companyArr['othersource']=$othersource;
		// $companyArr['profile1']=$profile1;
		// $companyArr['gsxx_zch']=$gsxx_zch;
		// $companyArr['gsxx_mc']=$gsxx_mc;
		// $companyArr['gsxx_lx']=$gsxx_lx;
		// $companyArr['gsxx_fddbr']=$gsxx_fddbr;
		// $companyArr['gsxx_zczb']=$gsxx_zczb;
		// $companyArr['gsxx_clrq']=$gsxx_clrq;
		// $companyArr['gsxx_zs']=$gsxx_zs;
		// $companyArr['gsxx_yyqx']=$gsxx_yyqx;
		// $companyArr['gsxx_jyfw']=$gsxx_jyfw;

		//获取案件类型
		$casetypeArr = $myInfo->get1D('casetype');
		$companyArr['04A']=$casetypeArr;
		$companyArr['04A_sub']='总数'.$myInfo->countSum($casetypeArr).'件';

		
		//获取
		$secondtagArr = $myInfo->get2D('casetype','secondtag');
		//获取民事细分案由
		$companyArr['04B']=$myInfo->fillEmpty(@$secondtagArr['民事']);
		$companyArr['04B_sub']='总数'.$myInfo->countSum($companyArr['04B']).'件';
		//获取刑事细分案由
		$companyArr['04C']=$myInfo->fillEmpty(@$secondtagArr['刑事']);
		$companyArr['04C_sub']='总数'.$myInfo->countSum($companyArr['04C']).'件';
		//获取行政细分案由
		$companyArr['04D']=$myInfo->fillEmpty(@$secondtagArr['行政']);
		$companyArr['04D_sub']='总数'.$myInfo->countSum($companyArr['04D']).'件';
		//获取执行细分案由
		$companyArr['04E']=$myInfo->fillEmpty(@$secondtagArr['执行']);
		$companyArr['04E_sub']='总数'.$myInfo->countSum($companyArr['04E']).'件';
		
		//获取月度分布
		$monthArr = $myInfo->getMonth();
		$companyArr['05A1']=array_values($monthArr);
		$companyArr['05A2']=array_keys($monthArr);
		$companyArr['05A_sub']='总数'.array_sum($companyArr['05A1']).'件';

		//获取层级分布
		$rankArr = $myInfo->get1D('rank');
		$companyArr['06A']=$rankArr;
		$companyArr['06A_sub']='总数'.$myInfo->countSum($companyArr['06A']).'件';

		//获取地域分布
		$siteArr = $myInfo->get1D('site');
		foreach ($siteArr as $k => $v) {
			$siteArr[$k]['selected']=true;
		}
		$companyArr['06B']=$siteArr;
		$companyArr['06B_sub']='总数'.$myInfo->countSum($companyArr['06B']).'件';

		//获取前三名法院
		$topCourtArr=$myInfo->get2D('court','secondtag');

		$companyArr['07A1']=key($topCourtArr);
		$companyArr['07A2']=current($topCourtArr);
		$companyArr['07A_sub']='总数'.$myInfo->countSum($companyArr['07A2']).'件';
		next($topCourtArr);
		$companyArr['07B1']=key($topCourtArr);
		$companyArr['07B2']=current($topCourtArr);
		$companyArr['07B_sub']='总数'.$myInfo->countSum($companyArr['07B2']).'件';
		next($topCourtArr);
		$companyArr['07C1']=key($topCourtArr);
		$companyArr['07C2']=current($topCourtArr);
		$companyArr['07C_sub']='总数'.$myInfo->countSum($companyArr['07C2']).'件';
		
		//获取公司的诉讼地位
		$statusArr = $myInfo->get1D('status');
		$companyArr['08A']=$statusArr;

		//获取不同审级攻守的二级案由

		$levelsecondArr = $myInfo->get3D('level','status','secondtag');

		$companyArr['09A']=$myInfo->fillEmpty(@$levelsecondArr['一审']['攻方']);
		$companyArr['09A_sub']='总数'.$myInfo->countSum($companyArr['09A']).'件';
		$companyArr['09B']=$myInfo->fillEmpty(@$levelsecondArr['一审']['守方']);
		$companyArr['09B_sub']='总数'.$myInfo->countSum($companyArr['09B']).'件';
		$companyArr['09C']=$myInfo->fillEmpty(@$levelsecondArr['二审']['攻方']);
		$companyArr['09C_sub']='总数'.$myInfo->countSum($companyArr['09C']).'件';
		$companyArr['09D']=$myInfo->fillEmpty(@$levelsecondArr['二审']['守方']);
		$companyArr['09D_sub']='总数'.$myInfo->countSum($companyArr['09D']).'件';
		$companyArr['09E']=$myInfo->fillEmpty(@$levelsecondArr['再审']['攻方']);
		$companyArr['09E_sub']='总数'.$myInfo->countSum($companyArr['09E']).'件';
		$companyArr['09F']=$myInfo->fillEmpty(@$levelsecondArr['再审']['守方']);
		$companyArr['09F_sub']='总数'.$myInfo->countSum($companyArr['09F']).'件';

		//获取文书类型
		$doctypeArr = $myInfo->get1D('doctype');
		$companyArr['10A']=$doctypeArr;
		$companyArr['10A_sub']='总数'.$myInfo->countSum($companyArr['10A']).'件';

		//获取裁判结果
		$resultArr = $myInfo->get2D('doctype','outcome');
		//获取裁定裁判结果
		$companyArr['10B']=$myInfo->fillEmpty(@$resultArr['判决']);
		$companyArr['10B_sub']='总数'.$myInfo->countSum($companyArr['10B']).'件';
		//获取判决裁判结果
		$companyArr['10C']=$myInfo->fillEmpty(@$resultArr['裁定']);
		$companyArr['10C_sub']='总数'.$myInfo->countSum($companyArr['10C']).'件';

		//获取判决结案案件分析1
		$wholeresultArr=$myInfo->get4D('doctype','level','status','outcome');

		$resultArr1=$myInfo->fillEmpty(@$wholeresultArr['判决']['一审']['攻方']);
		$companyArr['11A']=$resultArr1;
		$companyArr['11A_sub']='总数'.$myInfo->countSum($companyArr['11A']).'件';

		$resultArr2=$myInfo->fillEmpty(@$wholeresultArr['判决']['一审']['守方']);
		$companyArr['11B']=$resultArr2;
		$companyArr['11B_sub']='总数'.$myInfo->countSum($companyArr['11B']).'件';

		$resultArr3=$myInfo->fillEmpty(@$wholeresultArr['判决']['二审']['攻方']);
		$companyArr['11C']=$resultArr3;
		$companyArr['11C_sub']='总数'.$myInfo->countSum($companyArr['11C']).'件';

		$resultArr4=$myInfo->fillEmpty(@$wholeresultArr['判决']['二审']['守方']);
		$companyArr['11D']=$resultArr4;
		$companyArr['11D_sub']='总数'.$myInfo->countSum($companyArr['11D']).'件';

		$resultArr5=$myInfo->fillEmpty(@$wholeresultArr['判决']['再审']['攻方']);
		$companyArr['11E']=$resultArr5;
		$companyArr['11E_sub']='总数'.$myInfo->countSum($companyArr['11E']).'件';

		$resultArr6=$myInfo->fillEmpty(@$wholeresultArr['判决']['再审']['守方']);
		$companyArr['11F']=$resultArr6;
		$companyArr['11F_sub']='总数'.$myInfo->countSum($companyArr['11F']).'件';
		

		//获取前三名律所案由
		$myInfo->getCompanyLawfirm();
		
		$topLawfirmArr=$myInfo->get2D('lawfirm','secondtag');

		$companyArr['12A1']=key($topLawfirmArr);
		$companyArr['12A2']=current($topLawfirmArr);
		$companyArr['12A_sub']='总数'.$myInfo->countSum($companyArr['12A2']).'件';
		next($topLawfirmArr);
		$companyArr['12B1']=key($topLawfirmArr);
		$companyArr['12B2']=current($topLawfirmArr);
		$companyArr['12B_sub']='总数'.$myInfo->countSum($companyArr['12B2']).'件';
		next($topLawfirmArr);
		$companyArr['12C1']=key($topLawfirmArr);
		$companyArr['12C2']=current($topLawfirmArr);
		$companyArr['12C_sub']='总数'.$myInfo->countSum($companyArr['12C2']).'件';

		//获取对方当事人
		$reverseArr = $myInfo->getCompanyReverseParty();

		$reverseleixing=$myInfo->get1D('leixing',$reverseArr);
		$companyArr['13A']=$reverseleixing;
		$companyArr['13A_sub']='总数'.$myInfo->countSum($companyArr['13A']).'名';
		
		//获取前三名对方当事人案由
		$topReverseArr=$myInfo->get2D('name','secondtag',$reverseArr);

		$companyArr['14A1']=key($topReverseArr);
		$companyArr['14A2']=current($topReverseArr);
		$companyArr['14A_sub']='总数'.$myInfo->countSum($companyArr['14A2']).'件';
		next($topReverseArr);
		$companyArr['14B1']=key($topReverseArr);
		$companyArr['14B2']=current($topReverseArr);
		$companyArr['14B_sub']='总数'.$myInfo->countSum($companyArr['14B2']).'件';
		next($topReverseArr);
		$companyArr['14C1']=key($topReverseArr);
		$companyArr['14C2']=current($topReverseArr);
		$companyArr['14C_sub']='总数'.$myInfo->countSum($companyArr['14C2']).'件';

		file_put_contents('../views/data/report_company'.$this->reportId.'.txt', var_export($companyArr,true));
		file_put_contents('../views/data/report_company'.$this->reportId.'.json', json_encode($companyArr));

		// return json_encode($companyArr);
	}
	
	public function lawfirmReport(){
		
		$lawfirmname = '方达律师事务所';
		$wenshusource="中国裁判文书网";
		$scale="全国范围诉讼案件";
		$judgedate="2014年1月1日到2014年12月31日";
		$sitescope="全国";
		$casefield="全部";
		$casecount="74件";
		$othersource="司法局官网、方达律师事务所网站、全国企业信用信息公示系统";
		$lastvisit="2015 年 12 月 23 日";
		$profile1="方达律师事务所（下文简称“方达”）成立于 1993 年，是在商事法律领域处于领先地位的中国律师事务所之一。正如律所的名称所反映的，“方”乃“方正 Integrity”，“达”则要求“通达 Open-mindedness”。方达力求通过提供高质量的专业服务来满足国内外客户广泛和多样的法律服务需求。";
        $profile2="目前方达在北京、上海、深圳和香港四地设有办公室。方达的客户以国际金融机构和跨国公司为主，主要业务范围包括公司并购、私募股权融资、争议解决和知识产权等领域。";
        $name="方达";
		
		$myInfo = new GetInfo();
		$myLawfirm = new ReportLawfirm();
		$lawfirmArr = array();

		//获取基本信息
		$lawfirmArr['lawfirmname']=$lawfirmname;
		$lawfirmArr['wenshusource']=$wenshusource;
		$lawfirmArr['scale']=$scale;
		$lawfirmArr['judgedate']=$judgedate;
		$lawfirmArr['sitescope']=$sitescope;
		$lawfirmArr['casefield']=$casefield;
		$lawfirmArr['casecount']=$casecount;
		$lawfirmArr['othersource']=$othersource;
		$lawfirmArr['lastvisit']=$lastvisit;
        $lawfirmArr['profile1']=$profile1;
		$lawfirmArr['profile2']=$profile2;
        
		//获取律所的律师地域分布
        $lawyersiteArr=array(
        	array('value' =>98,'name'=>'上海','selected'=>true),
        	array('value' =>62 ,'name'=>'北京','selected'=>true),
        	array('value' =>11 ,'name'=>'广东','selected'=>true)
        	);
        $lawfirmArr['03A']=$lawyersiteArr;
                    
		//获取律所客户
		$clientArr = $myLawfirm->getLawfirmClient($this->rst,$name);
        
        //获取律所客户类型
		$totalClientArr=$myInfo->isPerson($clientArr);
		$lawfirmArr['06A']=$totalClientArr;
        
        //获取律所机构客户类型
		$companyClientArr=$myInfo->isCompany($clientArr);
		$lawfirmArr['06B']=$companyClientArr;

		//获取委托数量前三名的客户
        $lawfirmclientnameArr=array_keys($clientArr); 
        $lawfirmclientnumArr=array_values($clientArr);

        $clientTop=array(
        	array('value'=>$lawfirmclientnumArr[0],'name'=>$lawfirmclientnameArr[0]),
        	array('value'=>$lawfirmclientnumArr[1],'name'=>$lawfirmclientnameArr[1]),
        	array('value'=>$lawfirmclientnumArr[2],'name'=>$lawfirmclientnameArr[2])
        	);
        $lawfirmArr['07A']=$clientTop[0];
        $lawfirmArr['07B']=$clientTop[1];
        $lawfirmArr['07C']=$clientTop[2];
        

		//获取对方当事人类型
		$reverseArr=$myLawfirm->getReverseClient($this->rst,$name);

		//获取对方机构当事人细分类型
		$personReverseArr=$myInfo->isPerson($reverseArr);
		$lawfirmArr['08A']=$personReverseArr;

		//获取对方机构当事人细分类型
		$companyReverseArr=$myInfo->isCompany($reverseArr);
		$lawfirmArr['08B']=$companyReverseArr;

		//获取对手律所前三名
		$reverselawfirmArr=$myLawfirm->getReverseAgent($this->rst,$name);

		$reverselawfirmnameArr=array_keys($reverselawfirmArr); 
        $reverselawfirmnumArr=array_values($reverselawfirmArr);
        
        $reverselawfirmTop=array(
        	array('value'=>$reverselawfirmnumArr[0],'name'=>$reverselawfirmnameArr[0]),
        	array('value'=>$reverselawfirmnumArr[1],'name'=>$reverselawfirmnameArr[1]),
        	array('value'=>$reverselawfirmnumArr[2],'name'=>$reverselawfirmnameArr[2])
        	);
        $lawfirmArr['09A']=$reverselawfirmTop[0];
        $lawfirmArr['09B']=$reverselawfirmTop[1];
        $lawfirmArr['09C']=$reverselawfirmTop[2];


		//获取案件类型
		$casetypeArr = $myInfo->getCasetype($this->rst);
		$lawfirmArr['10A']=$casetypeArr;
		
		//获取二级案由
		$secondtagArr = $myInfo->getSecondtag($this->rst);
        //获取民事细分案由
		$lawfirmArr['10B']=$secondtagArr['民事'];
		//获取知识产权细分案由
		$lawfirmArr['10C']=$secondtagArr['知产'];
		//获取行政细分案由
		$lawfirmArr['10D']=$secondtagArr['行政'];
		//获取执行细分案由
		$lawfirmArr['10E']=$secondtagArr['执行'];
		//获取刑事细分案由
		$lawfirmArr['10F']=$secondtagArr['刑事'];

		//获取层级分布
		$rankArr = $myInfo->getRank($this->rst);
		$lawfirmArr['11A']=$rankArr;

		//获取案件审级
		$levelArr = $myInfo->getLevel($this->rst);
		$lawfirmArr['12A']=$levelArr;

		//获取地域分布
		$siteArr = $myInfo->getSite($this->rst);
		$lawfirmArr['12B']=$siteArr;

		//获取诉讼地位
		$statusArr = $myLawfirm->getlawfirmStatus($this->rst,$name);

		$statusArr1=array(
				array('name'=>'攻方','value'=>array_sum($statusArr[0])),
				array('name'=>'守方','value'=>array_sum($statusArr[1])),
				array('name'=>'第三方','value'=>array_sum($statusArr[2]))
				); 
				//因为方达律师可能同时代理攻方/守方和第三方，所以第三方律师会多一些
		$lawfirmArr['13A']=$statusArr1;

		//获得攻方客户的审级分布
		$gongstatusArr=$myLawfirm->getlawfirmgongStatuslevel($this->rst,$name);
		$lawfirmArr['13B']=$gongstatusArr;

		//获得守方客户的审级分布
		$shoustatusArr=$myLawfirm->getlawfirmshouStatuslevel($this->rst,$name);
		$lawfirmArr['13C']=$shoustatusArr;

        //获取文书类型
        $doctypeArr=$myInfo->getDoctype($this->rst,$name);
        $lawfirmArr['14A']=$doctypeArr;

		//获取判决的诉讼地位1
		$panjueanjianstatusArr = $myLawfirm->getpanjueJudgeStatus($this->rst,$name);
		$panjuestatusArr=array(
				array('name'=>'攻方','value'=>array_sum($panjueanjianstatusArr[0])),
				array('name'=>'守方','value'=>array_sum($panjueanjianstatusArr[1])),
				array('name'=>'第三方','value'=>array_sum($panjueanjianstatusArr[2]))
				);
		 $lawfirmArr['14B']=$panjuestatusArr;

		//获取裁定结果
		$caidingResultArr = $myInfo->getCaidingResult($this->rst);
		$lawfirmArr['14C']=$caidingResultArr;

        //获取一审攻方判决结果
         $level1panjuepesultArr=$myLawfirm->getlevel1PanjueResult($this->rst,$name);
         $level1gongfangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level1panjuepesultArr[0]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level1panjuepesultArr[0]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level1panjuepesultArr[0]['驳回攻方全部诉请'])
         	);
 		 $lawfirmArr['15A']=$level1gongfangpanjueArr;
        //获取一审守方判决结果
        $level1shoufangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level1panjuepesultArr[1]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level1panjuepesultArr[1]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level1panjuepesultArr[1]['驳回攻方全部诉请'])
         	);
 		 $lawfirmArr['15D']=$level1shoufangpanjueArr;
        //获取二审攻方判决结果
        $level2panjuepesultArr=$myLawfirm->getlevel2PanjueResult($this->rst,$name);
        $level2gongfangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level2panjuepesultArr[0]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level2panjuepesultArr[0]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level2panjuepesultArr[0]['驳回攻方全部诉请'])
         	);
 		$lawfirmArr['15B']=$level2gongfangpanjueArr;
        //获取二审守方判决结果
        $level2shoufangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level2panjuepesultArr[1]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level2panjuepesultArr[1]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level2panjuepesultArr[1]['驳回攻方全部诉请'])
         	);
 		$lawfirmArr['15E']=$level2shoufangpanjueArr;
        //获取再审攻方判决结果
         $level3panjuepesultArr=$myLawfirm->getlevel3PanjueResult($this->rst,$name);
         $level3gongfangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level3panjuepesultArr[0]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level3panjuepesultArr[0]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level3panjuepesultArr[0]['驳回攻方全部诉请'])
         	);
 		 $lawfirmArr['15C']=$level3gongfangpanjueArr;
        //获取再审守方判决结果
		 $level3shoufangpanjueArr=array(
         	array('name'=>'支持攻方全部诉请','value'=>$level3panjuepesultArr[1]['支持攻方全部诉请']),
         	array('name'=>'支持攻方部分诉请','value'=>$level3panjuepesultArr[1]['支持攻方部分诉请']),
         	array('name'=>'驳回攻方全部诉请','value'=>$level3panjuepesultArr[1]['驳回攻方全部诉请'])
         	);
 		 $lawfirmArr['15F']=$level3shoufangpanjueArr;

         //代理案件前三名律师排行
	     $threelawyernumArr = $myLawfirm->getthreelawyernum($this->rst,$name);
	     //获取代理案件数量第一名的律师及其代理案件案由
         $firstlawyerArr1=$threelawyernumArr[0];
         $firstlawyerArr2=$threelawyernumArr[3];
         $lawfirmArr['17A1']=$firstlawyerArr1;
         $lawfirmArr['17A2']=$firstlawyerArr2;

         //获取代理案件数量第二名的律师及其代理案件案由
         $secondlawyerArr=$threelawyernumArr[1];
         $secondlawyerArr2=$threelawyernumArr[4];
         $lawfirmArr['17B1']=$secondlawyerArr;
         $lawfirmArr['17B2']=$secondlawyerArr2;
         //获取代理案件数量第三名的律师及其代理案件案由
         $thirdlawyerArr=$threelawyernumArr[2]; 
         $thirdlawyerArr2=$threelawyernumArr[5];
         $lawfirmArr['17C1']=$thirdlawyerArr;
         $lawfirmArr['17C2']=$thirdlawyerArr2;
 
		file_put_contents('../views/js/report_lawfirm.txt', var_export($lawfirmArr,true));
		file_put_contents('../views/js/report_lawfirm.json', json_encode($lawfirmArr));
		return json_encode($lawfirmArr);
	}

	public function multiCompanyReport(){

		$multiCompanyArr = array();

		//获取基本信息
		$multiCompanyname = $this->subName;
		$multiCompanyArr['multiCompanyname']=$multiCompanyname;

		$multiCompanyArr['wenshusource']="中国裁判文书网";

		$judgedate=$this->beginDate."-".$this->finishDate;
		$multiCompanyArr['judgedate']=$judgedate;

		$multiCompanyArr['sitescope']="全国范围诉讼案件";

		$multiCompanyArr['casefield']="民事案件、知识产权案件";
		
		date_default_timezone_set('Asia/Shanghai'); 
		$lastvisit=date('Y年m月d日',time());
		$multiCompanyArr['lastvisit']=$lastvisit;

		// $profile1="深圳市腾讯计算机系统有限公司（以下简称“腾讯公司”）成立于1998年 11 月，是现今中国最大的互联网综合服务提供商之一，其业务涉及社交网络、互动娱乐、移动互联网、网络媒体、微信、企业发展、技术工程等多个领域。";

		// $multiCompanyArr['othersource']=$othersource;
		// $multiCompanyArr['profile1']=$profile1;
		
		//整体数据分析



		//每家公司分析
		$companyArr=array('深圳市腾讯计算机系统有限公司','乐视网信息技术（北京）股份有限公司','北京百度网讯科技有限公司','合一信息技术（北京）有限公司','广州网易计算机系统有限公司','北京新浪互联信息服务有限公司','北京搜狐互联网信息服务有限公司','广州市动景计算机科技有限公司','北京猎豹网络科技有限公司','北京一点网聚科技有限公司');

		$n=1;
		foreach ($companyArr as $company) {
			$page=2+7*$n++;

			$myCase=$this->case[$company];
			//获取月度分布
			$myInfo = new GetInfo($myCase);
			$monthArr = $myInfo->getMonth();
			$multiCompanyArr[($page+1).'A1']=array_values($monthArr);
			$multiCompanyArr[($page+1).'A2']=array_keys($monthArr);
			$multiCompanyArr[($page+1).'A_sub']='总数'.array_sum($multiCompanyArr[($page+1).'A1']).'件';

			//获取地域分布
			$siteArr = $myInfo->get1D('site');
			foreach ($siteArr as $k => $v) {
				$siteArr[$k]['selected']=true;
			}
			$multiCompanyArr[($page+2).'A']=$siteArr;
			$multiCompanyArr[($page+2).'A_sub']='总数'.$myInfo->countSum($multiCompanyArr[($page+2).'A']).'件';

			//获取层级分布
			$rankArr = $myInfo->get1D('rank');
			$multiCompanyArr[($page+3).'A']=$rankArr;
			$multiCompanyArr[($page+3).'A_sub']='总数'.$myInfo->countSum($multiCompanyArr[($page+3).'A']).'件';

			//获取审级分布
			$levelArr = $myInfo->get1D('level');
			$multiCompanyArr[($page+3).'B']=$levelArr;
			$multiCompanyArr[($page+3).'B_sub']='总数'.$myInfo->countSum($multiCompanyArr[($page+3).'B']).'件';


			//获取裁判结果
			$resultArr = $myInfo->get2D('doctype','outcome');
			//获取裁定裁判结果
			$multiCompanyArr[($page+4).'A']=$myInfo->fillEmpty(@$resultArr['判决']);
			$multiCompanyArr[($page+4).'A_sub']='总数'.$myInfo->countSum($multiCompanyArr[($page+4).'A']).'件';
			//获取判决裁判结果
			$multiCompanyArr[($page+4).'B']=$myInfo->fillEmpty(@$resultArr['裁定']);
			$multiCompanyArr[($page+4).'B_sub']='总数'.$myInfo->countSum($multiCompanyArr[($page+4).'B']).'件';

			//获取前三名法院
			$topCourtArr=$myInfo->get2D('court','secondtag');

			$multiCompanyArr['07A1']=key($topCourtArr);
			$multiCompanyArr['07A2']=current($topCourtArr);
			$multiCompanyArr['07A_sub']='总数'.$myInfo->countSum($multiCompanyArr['07A2']).'件';
			next($topCourtArr);
			$multiCompanyArr['07B1']=key($topCourtArr);
			$multiCompanyArr['07B2']=current($topCourtArr);
			$multiCompanyArr['07B_sub']='总数'.$myInfo->countSum($multiCompanyArr['07B2']).'件';
			next($topCourtArr);
			$multiCompanyArr['07C1']=key($topCourtArr);
			$multiCompanyArr['07C2']=current($topCourtArr);
			$multiCompanyArr['07C_sub']='总数'.$myInfo->countSum($multiCompanyArr['07C2']).'件';

			//获取前三名法官
			$topCourtArr=$myInfo->get2D('court','secondtag');

			$multiCompanyArr['07A1']=key($topCourtArr);
			$multiCompanyArr['07A2']=current($topCourtArr);
			$multiCompanyArr['07A_sub']='总数'.$myInfo->countSum($multiCompanyArr['07A2']).'件';
			next($topCourtArr);
			$multiCompanyArr['07B1']=key($topCourtArr);
			$multiCompanyArr['07B2']=current($topCourtArr);
			$multiCompanyArr['07B_sub']='总数'.$myInfo->countSum($multiCompanyArr['07B2']).'件';
			next($topCourtArr);
			$multiCompanyArr['07C1']=key($topCourtArr);
			$multiCompanyArr['07C2']=current($topCourtArr);
			$multiCompanyArr['07C_sub']='总数'.$myInfo->countSum($multiCompanyArr['07C2']).'件';


			//获取前三名律所案由
			$myInfo->getCompanyLawfirm();
			
			$topLawfirmArr=$myInfo->get2D('lawfirm','secondtag');

			$multiCompanyArr['12A1']=key($topLawfirmArr);
			$multiCompanyArr['12A2']=current($topLawfirmArr);
			$multiCompanyArr['12A_sub']='总数'.$myInfo->countSum($multiCompanyArr['12A2']).'件';
			next($topLawfirmArr);
			$multiCompanyArr['12B1']=key($topLawfirmArr);
			$multiCompanyArr['12B2']=current($topLawfirmArr);
			$multiCompanyArr['12B_sub']='总数'.$myInfo->countSum($multiCompanyArr['12B2']).'件';
			next($topLawfirmArr);
			$multiCompanyArr['12C1']=key($topLawfirmArr);
			$multiCompanyArr['12C2']=current($topLawfirmArr);
			$multiCompanyArr['12C_sub']='总数'.$myInfo->countSum($multiCompanyArr['12C2']).'件';

			//获取前三名律师案由
			$myInfo->getCompanyLawfirm();
			
			$topLawfirmArr=$myInfo->get2D('lawfirm','secondtag');

			$multiCompanyArr['12A1']=key($topLawfirmArr);
			$multiCompanyArr['12A2']=current($topLawfirmArr);
			$multiCompanyArr['12A_sub']='总数'.$myInfo->countSum($multiCompanyArr['12A2']).'件';
			next($topLawfirmArr);
			$multiCompanyArr['12B1']=key($topLawfirmArr);
			$multiCompanyArr['12B2']=current($topLawfirmArr);
			$multiCompanyArr['12B_sub']='总数'.$myInfo->countSum($multiCompanyArr['12B2']).'件';
			next($topLawfirmArr);
			$multiCompanyArr['12C1']=key($topLawfirmArr);
			$multiCompanyArr['12C2']=current($topLawfirmArr);
			$multiCompanyArr['12C_sub']='总数'.$myInfo->countSum($multiCompanyArr['12C2']).'件';

			//获取前三名对方当事人案由
			$reverseArr = $myInfo->getCompanyReverseParty();
			$topReverseArr=$myInfo->get2D('name','secondtag',$reverseArr);

			$multiCompanyArr['14A1']=key($topReverseArr);
			$multiCompanyArr['14A2']=current($topReverseArr);
			$multiCompanyArr['14A_sub']='总数'.$myInfo->countSum($multiCompanyArr['14A2']).'件';
			next($topReverseArr);
			$multiCompanyArr['14B1']=key($topReverseArr);
			$multiCompanyArr['14B2']=current($topReverseArr);
			$multiCompanyArr['14B_sub']='总数'.$myInfo->countSum($multiCompanyArr['14B2']).'件';
			next($topReverseArr);
			$multiCompanyArr['14C1']=key($topReverseArr);
			$multiCompanyArr['14C2']=current($topReverseArr);
			$multiCompanyArr['14C_sub']='总数'.$myInfo->countSum($multiCompanyArr['14C2']).'件';





		}


		//获取案件类型
		$casetypeArr = $myInfo->get1D('casetype');
		$multiCompanyArr['04A']=$casetypeArr;
		$multiCompanyArr['04A_sub']='总数'.$myInfo->countSum($casetypeArr).'件';

		
		//获取
		$secondtagArr = $myInfo->get2D('casetype','secondtag');
		//获取民事细分案由
		$multiCompanyArr['04B']=$myInfo->fillEmpty(@$secondtagArr['民事']);
		$multiCompanyArr['04B_sub']='总数'.$myInfo->countSum($multiCompanyArr['04B']).'件';
		//获取刑事细分案由
		$multiCompanyArr['04C']=$myInfo->fillEmpty(@$secondtagArr['刑事']);
		$multiCompanyArr['04C_sub']='总数'.$myInfo->countSum($multiCompanyArr['04C']).'件';
		//获取行政细分案由
		$multiCompanyArr['04D']=$myInfo->fillEmpty(@$secondtagArr['行政']);
		$multiCompanyArr['04D_sub']='总数'.$myInfo->countSum($multiCompanyArr['04D']).'件';
		//获取执行细分案由
		$multiCompanyArr['04E']=$myInfo->fillEmpty(@$secondtagArr['执行']);
		$multiCompanyArr['04E_sub']='总数'.$myInfo->countSum($multiCompanyArr['04E']).'件';
		
		
		//获取前三名法院
		$topCourtArr=$myInfo->get2D('court','secondtag');

		$multiCompanyArr['07A1']=key($topCourtArr);
		$multiCompanyArr['07A2']=current($topCourtArr);
		$multiCompanyArr['07A_sub']='总数'.$myInfo->countSum($multiCompanyArr['07A2']).'件';
		next($topCourtArr);
		$multiCompanyArr['07B1']=key($topCourtArr);
		$multiCompanyArr['07B2']=current($topCourtArr);
		$multiCompanyArr['07B_sub']='总数'.$myInfo->countSum($multiCompanyArr['07B2']).'件';
		next($topCourtArr);
		$multiCompanyArr['07C1']=key($topCourtArr);
		$multiCompanyArr['07C2']=current($topCourtArr);
		$multiCompanyArr['07C_sub']='总数'.$myInfo->countSum($multiCompanyArr['07C2']).'件';
		
		//获取公司的诉讼地位
		$statusArr = $myInfo->get1D('status');
		$multiCompanyArr['08A']=$statusArr;

		//获取不同审级攻守的二级案由

		$levelsecondArr = $myInfo->get3D('level','status','secondtag');

		$multiCompanyArr['09A']=$myInfo->fillEmpty(@$levelsecondArr['一审']['攻方']);
		$multiCompanyArr['09A_sub']='总数'.$myInfo->countSum($multiCompanyArr['09A']).'件';
		$multiCompanyArr['09B']=$myInfo->fillEmpty(@$levelsecondArr['一审']['守方']);
		$multiCompanyArr['09B_sub']='总数'.$myInfo->countSum($multiCompanyArr['09B']).'件';
		$multiCompanyArr['09C']=$myInfo->fillEmpty(@$levelsecondArr['二审']['攻方']);
		$multiCompanyArr['09C_sub']='总数'.$myInfo->countSum($multiCompanyArr['09C']).'件';
		$multiCompanyArr['09D']=$myInfo->fillEmpty(@$levelsecondArr['二审']['守方']);
		$multiCompanyArr['09D_sub']='总数'.$myInfo->countSum($multiCompanyArr['09D']).'件';
		$multiCompanyArr['09E']=$myInfo->fillEmpty(@$levelsecondArr['再审']['攻方']);
		$multiCompanyArr['09E_sub']='总数'.$myInfo->countSum($multiCompanyArr['09E']).'件';
		$multiCompanyArr['09F']=$myInfo->fillEmpty(@$levelsecondArr['再审']['守方']);
		$multiCompanyArr['09F_sub']='总数'.$myInfo->countSum($multiCompanyArr['09F']).'件';

		//获取文书类型
		$doctypeArr = $myInfo->get1D('doctype');
		$multiCompanyArr['10A']=$doctypeArr;
		$multiCompanyArr['10A_sub']='总数'.$myInfo->countSum($multiCompanyArr['10A']).'件';

		//获取裁判结果
		$resultArr = $myInfo->get2D('doctype','outcome');
		//获取裁定裁判结果
		$multiCompanyArr['10B']=$myInfo->fillEmpty(@$resultArr['判决']);
		$multiCompanyArr['10B_sub']='总数'.$myInfo->countSum($multiCompanyArr['10B']).'件';
		//获取判决裁判结果
		$multiCompanyArr['10C']=$myInfo->fillEmpty(@$resultArr['裁定']);
		$multiCompanyArr['10C_sub']='总数'.$myInfo->countSum($multiCompanyArr['10C']).'件';

		//获取判决结案案件分析1
		$wholeresultArr=$myInfo->get4D('doctype','level','status','outcome');

		$resultArr1=$myInfo->fillEmpty(@$wholeresultArr['判决']['一审']['攻方']);
		$multiCompanyArr['11A']=$resultArr1;
		$multiCompanyArr['11A_sub']='总数'.$myInfo->countSum($multiCompanyArr['11A']).'件';

		$resultArr2=$myInfo->fillEmpty(@$wholeresultArr['判决']['一审']['守方']);
		$multiCompanyArr['11B']=$resultArr2;
		$multiCompanyArr['11B_sub']='总数'.$myInfo->countSum($multiCompanyArr['11B']).'件';

		$resultArr3=$myInfo->fillEmpty(@$wholeresultArr['判决']['二审']['攻方']);
		$multiCompanyArr['11C']=$resultArr3;
		$multiCompanyArr['11C_sub']='总数'.$myInfo->countSum($multiCompanyArr['11C']).'件';

		$resultArr4=$myInfo->fillEmpty(@$wholeresultArr['判决']['二审']['守方']);
		$multiCompanyArr['11D']=$resultArr4;
		$multiCompanyArr['11D_sub']='总数'.$myInfo->countSum($multiCompanyArr['11D']).'件';

		$resultArr5=$myInfo->fillEmpty(@$wholeresultArr['判决']['再审']['攻方']);
		$multiCompanyArr['11E']=$resultArr5;
		$multiCompanyArr['11E_sub']='总数'.$myInfo->countSum($multiCompanyArr['11E']).'件';

		$resultArr6=$myInfo->fillEmpty(@$wholeresultArr['判决']['再审']['守方']);
		$multiCompanyArr['11F']=$resultArr6;
		$multiCompanyArr['11F_sub']='总数'.$myInfo->countSum($multiCompanyArr['11F']).'件';
		

		//获取前三名律所案由
		$myInfo->getmultiCompanyLawfirm();
		
		$topLawfirmArr=$myInfo->get2D('lawfirm','secondtag');

		$multiCompanyArr['12A1']=key($topLawfirmArr);
		$multiCompanyArr['12A2']=current($topLawfirmArr);
		$multiCompanyArr['12A_sub']='总数'.$myInfo->countSum($multiCompanyArr['12A2']).'件';
		next($topLawfirmArr);
		$multiCompanyArr['12B1']=key($topLawfirmArr);
		$multiCompanyArr['12B2']=current($topLawfirmArr);
		$multiCompanyArr['12B_sub']='总数'.$myInfo->countSum($multiCompanyArr['12B2']).'件';
		next($topLawfirmArr);
		$multiCompanyArr['12C1']=key($topLawfirmArr);
		$multiCompanyArr['12C2']=current($topLawfirmArr);
		$multiCompanyArr['12C_sub']='总数'.$myInfo->countSum($multiCompanyArr['12C2']).'件';

		//获取对方当事人
		$reverseArr = $myInfo->getmultiCompanyReverseParty();

		$reverseleixing=$myInfo->get1D('leixing',$reverseArr);
		$multiCompanyArr['13A']=$reverseleixing;
		$multiCompanyArr['13A_sub']='总数'.$myInfo->countSum($multiCompanyArr['13A']).'名';
		
		//获取前三名对方当事人案由
		$topReverseArr=$myInfo->get2D('name','secondtag',$reverseArr);

		$multiCompanyArr['14A1']=key($topReverseArr);
		$multiCompanyArr['14A2']=current($topReverseArr);
		$multiCompanyArr['14A_sub']='总数'.$myInfo->countSum($multiCompanyArr['14A2']).'件';
		next($topReverseArr);
		$multiCompanyArr['14B1']=key($topReverseArr);
		$multiCompanyArr['14B2']=current($topReverseArr);
		$multiCompanyArr['14B_sub']='总数'.$myInfo->countSum($multiCompanyArr['14B2']).'件';
		next($topReverseArr);
		$multiCompanyArr['14C1']=key($topReverseArr);
		$multiCompanyArr['14C2']=current($topReverseArr);
		$multiCompanyArr['14C_sub']='总数'.$myInfo->countSum($multiCompanyArr['14C2']).'件';

		file_put_contents('../views/data/report_multiCompany'.$this->reportId.'.txt', var_export($multiCompanyArr,true));
		file_put_contents('../views/data/report_multiCompany'.$this->reportId.'.json', json_encode($multiCompanyArr));

		// return json_encode($companyArr);
	}
}