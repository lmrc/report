<?php

/**
* 
*/
class Download
{
	
	function __construct()
	{
		// session_start();
		$this->subType=$_SESSION['subType'];
		$name=$_SESSION['subName'];
		$this->subName=urlencode($name);
		$office=$_SESSION['subOffice'];
		$this->subOffice=urlencode($office);
		$this->beginDate=$_SESSION['beginDate'];
		$this->finishDate=$_SESSION['finishDate'];
	}

	public function getCase(){
		$header = array();
	    $header[] = 'Authorization: Basic dGVzdGxlZ2FsOmxlZ2FsdGVzdDE3ODk=';
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36");
	    curl_setopt($ch, CURLOPT_URL, 'http://pinglu.legalminer.com/rest/_search/download?querySentence=*:*&startDate='.$this->beginDate.'&endDate='.$this->finishDate.'&sortField=judgeDate&caseNum=&jtype=&lawyer='.$this->subName.'&office='.$this->subOffice.'&judge=&litigant=&courtCode=&title=&proceedings=&fact=&courtConsiders=&result=');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
	    
	    $response = curl_exec($ch);
	    if(curl_errno($ch)) echo 'Curl error: '.curl_error($ch);
	    curl_close($ch);

    	$data=gzdecode($response);
		
	    $res=json_decode($data,true);
	    // var_dump($res);
	    // file_put_contents('c:/song.txt', var_export($res,true));
	    return $res;
	}

}