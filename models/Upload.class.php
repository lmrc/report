<?php 

/**
* 
*/
class Upload
{
	
	public function __construct($database_name='report',$collection_name){
		try {
			$conn = new MongoClient();
			$db = $conn ->$database_name;
			$this->test = $db->$collection_name;
		} catch (Exception $e) {
			file_put_contents('c:/dberror.log', $e,FILE_APPEND);			
		}

	}

	public function scanupload($dir){

		// $dir="C:/judges2/";

		$files=scandir($dir);

		foreach ($files as $k => $v) {
			if(!in_array($v,array('.','..'))){

				$filename=$dir.$v;
				// echo $filename;
				// echo "<hr>";

				$handle = fopen($filename, 'rb') or die("文件打开失败"); 

				$contents = "";
				while(!feof($handle)){          //使用feof()判断文件结尾
					$contents .=fread($handle, 1024);        //每次读取1024个字节
				}

				fclose($handle);   

				// var_dump($contents);  
				
				$contents = json_decode($contents,true);
				// var_dump($contents);
				// $contents = json_decode($contents,true);
				$pp=0;
				foreach ($contents as $key => $val) {
				
					// if ($key!=0) {
						$pp++;
						$arr=array("ID"=>$val['id'],"title"=>$val['title'],"content"=>$val['content']);
						if ($this->test->insert($arr)) {
							echo "第".$pp."条数据插入成功<br>";
						} else {
							echo "第".$pp."条数据插入失败<br>";
						}
					// }
				}
			}

		}
	}

	public function uploadOpenlaw(){

		$dir="c:/ms/";

		$files=scandir($dir);

		foreach ($files as $k => $v) {
			if(!in_array($v,array('.','..'))){

				$filename=$dir.$v;
				// echo $filename;

				$handle = fopen($filename, 'rb') or die("文件打开失败");     //以只读的方式，模式加了‘b'
				$contents = "";
				while(!feof($handle)){          //使用feof()判断文件结尾
					$contents .=fread($handle, 1024);        //每次读取1024个字节
				}
				// echo $contents;         //将从文件中读取的全部内容输
				fclose($handle);       //关闭文件资源

				$patterns=array();
				$patterns[0]='/:/';
				$patterns[1]='/字段1[\s]*/';
				$patterns[2]='/允许所有人查看[\s\S]*?new Date\(\)\.getTime\(\);\n		\}[\s]*/';

				$replacements=array();
				$replacements[0]='：';
				$replacements[1]='';
				$replacements[2]='<br>这是中间分隔<br>';

				$contents1=preg_replace($patterns, $replacements, $contents);
				
				$needle = '<br>这是中间分隔<br>';
				$times2 = substr_count($contents1,$needle);
				// echo $times2;

				$exparr=explode($needle, $contents1);
				$company=substr($v, 0, strlen($v)-4);
				// echo $company;
				foreach ($exparr as $key => $val) {
					$arr=array("key"=>$val,"company"=>$company);

					if ($this->test->insert($arr)) {
						echo "插入数据成功<br>";
					} else {
						echo "插入数据失败<br>";
					}
				}
			}

		}
	}

	public function uploadZgcpwsw($dir){

		// $dir="C:/case/";

		$files=scandir($dir);

		foreach ($files as $k => $v) {
			if(!in_array($v,array('.','..'))){

				$filename=$dir.$v;
				echo "<hr>";

				$handle = fopen($filename, 'rb') or die("文件打开失败");     //以只读的方式，模式加了‘b'
				$contents = "";
				while(!feof($handle)){          //使用feof()判断文件结尾
					$contents .=fread($handle, 1024);        //每次读取1024个字节
				}

				fclose($handle);       //关闭文件资源
				// echo $contents;         //将从文件中读取的全部内容输

				
				// $contents = preg_replace("/裁判要旨段原文/", "开始删除位置", $contents);
				// $contents = preg_replace("/DocContent/", "结束位置DocContent", $contents);
				// $contents = preg_replace('/开始删除位置[\s\S]*?结束位置/', '', $contents);

				// $contents = preg_replace('/\}\,\{/', '<br>这是中间分隔<br>', $contents);
				// $contents = preg_replace('/结束位置/', '', $contents);
				
				// $needle = '<br>这是中间分隔<br>';
				// $times2 = substr_count($contents,$needle);

				$contents = json_decode($contents,true);
				// var_dump($contents);
				$contents = json_decode($contents,true);
				// var_dump($contents);

				foreach ($contents as $key => $val) {
						// var_dump($key);
						// var_dump($val);
					if ($key!=0) {
						$arr=array("content"=>$val['DocContent'],"doctype"=>$val['案件类型'],"judgedate"=>$val['裁判日期'],"title"=>$val['案件名称'],"ID"=>$val['文书ID'],"level"=>$val['审判程序'],"casenum"=>$val['案号'],"court"=>$val['法院名称']);
						// $arr=array("ID"=>$val['_id'],"content"=>$val['content'][1],"title"=>$val['content'][0]);
						if ($this->test->insert($arr)) {
							echo "插入数据成功<br>";
						} else {
							echo "插入数据失败<br>";
						}
					}
				}
			}

		}
	}
}



