<?php 

define('BASE_URL', "C:/Myweb/Apache/htdocs/mydata");

define('FILE_LOC', 'C:/mydata');
/**
* 
*/
class BasicExport
{

function __construct()
{
	# code...
}
//*********************************案件报告*****************************
//************************公司报告获取法院层级（03A）***************
function getRank($rst){
	$rankArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$rank=$val['rank'];
		array_push($rankArr, $rank);
	}
	// print_r($rankArr);

	$rankCountValues=array_count_values($rankArr);
	//旧版的操作方法
	// return $rankCountValues;
	//新版的操作方法
	$arr=array();
		if (empty($rankCountValues)) {
			$arr[0]['value']=0;
			$arr[0]['name']="无此类案件";
		}else{
			foreach ($rankCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
			}
		}
	return $arr;
}

//***************************************公司报告获取地域的数据（04A）
function getSite($rst){

	$siteArr=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$court=$val['court'];
		$site=$val['site'];
		if (empty($site)==false) {
			# code...
		$siteArr=array_merge($siteArr,array($site));
		}

	}

	// print_r($siteArr);

	$siteCountValues=array_count_values($siteArr);
	//案件数量排列
	arsort($siteCountValues);
	// print_r($siteCountValues);
	//旧版的操作方法
	// return $siteCountValues;
	//新版的操作方法
	$arr=array();
		foreach ($siteCountValues as $key => $value) {
		array_push($arr, array('value'=>$value,'name'=>$key,'selected'=>true));
		}
	
	// file_put_contents('c:/test.log', var_export($arr,true));
	return $arr;
}



//***************************************公司报告获取日期的数据(04B)

function getMonth($rst){
	$monthArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$date=$val['date'];
		if(preg_match("/^201(1|2|3|4|5)/", $date)==1){
			array_push($monthArr, substr($date, 0, 7));
		}

	}

	// print_r($monthArr);

	$monthCountValues=array_count_values($monthArr);
	//案件数量排列
	ksort($monthCountValues);
	// print_r($monthCountValues);
	return $monthCountValues;
}
//**********************************************************************

//*************************************遍历全部记录获取公司的对方当事人的数据(05A\05B\05C\05D\06A\06B\06C\06D)
function getGSdsrtype($rst){
$L1GdsrArr=array();
$L1SdsrArr=array();
$L2GdsrArr=array();
$L2SdsrArr=array();
	foreach ($rst as $key => $val) {  
		$fid=$val['_id'];
		$party=$val['party'];
		$level=$val['level'];
		if ($level=="一审") {
			foreach ($party as $key1 => $val1) {
				if ($val1['type']=='plaintiff'||$val1['type']=='appellant'||$val1['type']=='proposer'){
					$L1GdsrArr=array_merge($L1GdsrArr,array($val1['name']));	
				}else if ($val1['type']=='defendant'||$val1['type']=='appellee'||$val1['type']=='respondent') {
					$L1SdsrArr=array_merge($L1SdsrArr,array($val1['name']));
            	}
			}
		}else if ($level=="二审") {
			foreach ($party as $key2 => $val2) {
				if ($val2['type']=='plaintiff'||$val2['type']=='appellant'||$val2['type']=='proposer'){
					$L2GdsrArr=array_merge($L2GdsrArr,array($val2['name']));	
				}else if ($val2['type']=='defendant'||$val2['type']=='appellee'||$val2['type']=='respondent') {
					$L2SdsrArr=array_merge($L2SdsrArr,array($val2['name']));
            	}
			}
		}
	}
	$L1GdsrUniqueRaw=array_unique($L1GdsrArr);
	foreach ($L1GdsrUniqueRaw as $key => $value) {
		$L1GdsrUniqueRaw1[]=$value;
	}
	$L1SdsrUniqueRaw=array_unique($L1SdsrArr);
	foreach ($L1SdsrUniqueRaw as $key1 => $value1) {
		$L1SdsrUniqueRaw1[]=$value1;
	}

	$L2GdsrUniqueRaw=array_unique($L2GdsrArr);
	foreach ($L2GdsrUniqueRaw as $key2 => $value2) {
		$L2GdsrUniqueRaw1[]=$value2;
	}
	$L2SdsrUniqueRaw=array_unique($L2SdsrArr);
	foreach ($L2SdsrUniqueRaw as $key3 => $value3) {
		$L2SdsrUniqueRaw1[]=$value3;
	}
     $L1GdsrUniqueRaw2=preg_replace('/（.*?）/', "", $L1GdsrUniqueRaw1);
     $L1SdsrUniqueRaw2=preg_replace('/（.*?）/', "", $L1SdsrUniqueRaw1);
     $L2GdsrUniqueRaw2=preg_replace('/（.*?）/', "", $L2GdsrUniqueRaw1);
     $L2SdsrUniqueRaw2=preg_replace('/（.*?）/', "", $L2SdsrUniqueRaw1);

	$arr=array($L1GdsrUniqueRaw2,$L1SdsrUniqueRaw2,$L2GdsrUniqueRaw2,$L2SdsrUniqueRaw2);
	return $arr;

}


//*************************************案件裁判分析(07A)
function getCasetype($rst){
	$casetypeArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$casetype=$val['casetype'];
		array_push($casetypeArr, $casetype);
	}
	// print_r($rankArr);

	$casetypeCountValues=array_count_values($casetypeArr);
	//案件数量排列
	arsort($casetypeCountValues);
	// print_r($casetypeCountValues);

	//旧版的操作方法
	// return $casetypeCountValues;

	//新版的操作方法
	$arr=array();
	if (empty($casetypeCountValues)) {
		$arr[0]['value']=0;
		$arr[0]['name']="无此类案件";
	}else{
		foreach ($casetypeCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}
	}

	return $arr;
}

//***************************************公司报告获取二级案由（07B）
function getSecondtag($rst){
	$minshiArr=array();
	$xingshiArr=array();
	$xingzhengArr=array();
	$zhichanArr=array();
	$zhixingArr=array();

	foreach ($rst as $val) {
		$fid=$val['_id'];
		$casetype=$val['casetype'];
		$secondtag=$val['secondtag'];
		var_dump($secondtag);
		// array_push($tagArr, $tag);

		switch ($casetype) {
			case '民事':
				$minshiArr=array_merge($minshiArr,array($secondtag));
				break;
			case '刑事':
				$xingshiArr=array_merge($xingshiArr,array($secondtag));
				break;
			case '行政':
				$xingzhengArr=array_merge($xingzhengArr,array($secondtag));
				break;
			case '知识产权':
				$zhichanArr=array_merge($zhichanArr,array($secondtag));
				break;
			case '执行':
				$zhixingArr=array_merge($zhixingArr,array($secondtag));
				break;
			default:
				# code...
				break;
		}
	}
	// print_r($rankArr);

	$minshiCountValues=array_count_values($minshiArr);
	$xingshiCountValues=array_count_values($xingshiArr);
	$xingzhengCountValues=array_count_values($xingzhengArr);
	$zhichanCountValues=array_count_values($zhichanArr);
	$zhixingCountValues=array_count_values($zhixingArr);
	//案件数量排列
	arsort($minshiCountValues);
	arsort($xingshiCountValues);
	arsort($xingzhengCountValues);
	arsort($zhichanCountValues);
	arsort($zhixingCountValues);
	// print_r($tagCountValues);

	//旧版的操作方法
	// return $tagCountValues;

	//新版的操作方法
	$arr_minshi=array();

	if (empty($minshiCountValues)) {
		$arr_minshi[0]['value']=0;
		$arr_minshi[0]['name']="无此类案件";
	}else{
		foreach ($minshiCountValues as $key => $value) {
			array_push($arr_minshi, array('value'=>$value,'name'=>$key));
		}
	}
	$arr_xingshi=array();
	if (empty($xingshiCountValues)) {
		$arr_xingshi[0]['value']=0;
		$arr_xingshi[0]['name']="无此类案件";
	}else{
		foreach ($xingshiCountValues as $key => $value) {
			array_push($arr_xingshi, array('value'=>$value,'name'=>$key));
		}
	}
	$arr_xingzheng=array();
	if (empty($xingzhengCountValues)) {
		$arr_xingzheng[0]['value']=0;
		$arr_xingzheng[0]['name']="无此类案件";
	}else{
		foreach ($xingzhengCountValues as $key => $value) {
			array_push($arr_xingzheng, array('value'=>$value,'name'=>$key));
		}
	}
	$arr_zhichan=array();
	if (empty($zhichanCountValues)) {
		$arr_zhichan[0]['value']=0;
		$arr_zhichan[0]['name']="无此类案件";
	}else{
		foreach ($zhichanCountValues as $key => $value) {
			array_push($arr_zhichan, array('value'=>$value,'name'=>$key));
		}
	}
	$arr_zhixing=array();
	if (empty($zhixingCountValues)) {
		$arr_zhixing[0]['value']=0;
		$arr_zhixing[0]['name']="无此类案件";
	}else{
		foreach ($zhixingCountValues as $key => $value) {
			array_push($arr_zhixing, array('value'=>$value,'name'=>$key));
		}
	}
	$arr=array();
	$arr['民事']=$arr_minshi;
	$arr['刑事']=$arr_xingshi;
	$arr['行政']=$arr_xingzheng;
	$arr['知产']=$arr_zhichan;
	$arr['执行']=$arr_zhixing;


	// foreach ($arr as $key => $value) {
	// 	if (empty($value)) {
	// 		$arr[$key]=array('value'=>0,'name'=>$key);
	// 	}
	// }
	return $arr;
}

//***************************************案件报告（08A）
function getDoctype($rst){
	$doctypeArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$ApplicationDate=$val['ApplicationDate'];
		$Acceptancetime=$val['Acceptancetime'];
		$Processingtimes=$val['Processingtimes'];
		if (empty($ApplicationDate)==false||empty($Acceptancetime)==false||empty($Processingtimes)==false) {
			array_push($doctypeArr, $doctype);
		}
	}
	// print_r($rankArr);

	$doctypeCountValues=array_count_values($doctypeArr);
	//案件数量排列
	arsort($doctypeCountValues);
	// print_r($doctypeCountValues);

	//旧版的操作方法
	// return $doctypeCountValues;

	//新版的操作方法
	$arr=array();
	if (empty($doctypeCountValues)) {
		$arr[0]['value']=0;
		$arr[0]['name']="无此类案件";
	}else{
		foreach ($doctypeCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}
	}

	return $arr;
}



//==========================================案件报告（08B）
function gettimeDoctype($rst){
	$arr=array();
	$time1=array();
	$time2=array();
	$time3=array();
	$time4=array();
	$time5=array();
	$time6=array();
	$i=$j=$k=$e=$f=$g=0;
	foreach ($rst as $val) {
	$fid=$val['_id'];
	$doctype=$val['doctype'];
	$level=$val['level'];
	$secondtime=$val['secondtime'];
		if (empty($secondtime)==false) {
			if($level=="一审"&&$doctype=="判决"){
				$time1[]=$secondtime;
				$i=$i+1;
			}else if ($level=="一审"&&$doctype=="裁定") {
				$j=$j+1;
				$time2[]=$secondtime;
			}else if ($level=="二审"&&$doctype=="判决") {
				$k=$k+1;
				$time3[]=$secondtime;
			}else if ($level=="二审"&&$doctype=="裁定") {
				$e=$e+1;
				$time4[]=$secondtime;
			}else if ($level=="再审"&&$doctype=="判决") {
				$f=$f+1;
				$time5[]=$secondtime;
			}else if ($level=="再审"&&$doctype=="裁定") {
				$g=$g+1;
				$time6[]=$secondtime;
			}
		}
	}
	if (empty($time1)==false) {
		$max1=max($time1);
		$average1=array_sum($time1)/count($time1);
		$min1=min($time1);
	}else{
		$max1=0;
		$average1=0;
		$min1=0;
	}
	if (empty($time2)==false) {
		$max2=max($time2);
		$average2=array_sum($time2)/count($time2);
		$min2=min($time2);
	}else{
		$max2=0;
		$average2=0;
		$min2=0;
	}
	if (empty($time3)==false) {
		$max3=max($time3);
		$average3=array_sum($time3)/count($time3);
		$min3=min($time3);
	}else{
		$max3=0;
		$average3=0;
		$min3=0;
	}
	if (empty($time4)==false) {
		$max4=max($time4);
		$average4=array_sum($time4)/count($time4);
		$min4=min($time4);
	}else{
		$max4=0;
		$average4=0;
		$min4=0;
	}
	if (empty($time5)==false) {
		$max5=max($time5);
		$average5=array_sum($time5)/count($time5);
		$min5=min($time5);
	}else{
		$max5=0;
		$average5=0;
		$min5=0;
	}
	if (empty($time6)==false) {
		$max6=max($time6);
		$average6=array_sum($time6)/count($time6);
		$min6=min($time6);
	}else{
		$max6=0;
		$average6=0;
		$min6=0;
	}
	$Max= array(0=>$max1, 1=>$max2 ,2=>$max3 ,3=>$max4,4=>$max5,5=>$max6);
	$Average= array(0=>$average1, 1=>$average2 ,2=>$average3 ,3=>$average4 ,4=>$average5 ,5=>$average6);
	$min= array(0=>$min1, 1=>$min2 ,2=>$min3 ,3=>$min4,4=>$min5 ,5=>$min6);
	$num=array("一审判决\n(".$i."个案件)","一审裁定\n(".$j."个案件)","二审判决\n(".$k."个案件)","二审裁定\n(".$e."个案件)","再审判决\n(".$f."个案件)","再审裁定\n(".$g."个案件)");	$arr=array($Max,$Average,$min,$num);
	return $arr;
}


//***************************************案件报告（09A）
function getCasetype_Secondtag($rst){
	$levelArr=array();
	$L1mssecondtagArr=array();
	$L2mssecondtagArr=array();
	$L1xssecondtagArr=array();
	$L2xssecondtagArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$level=$val['level'];
		$secondtag=$val['secondtag'];
		$casetype=$val['casetype'];
		array_push($levelArr, $level);
		if ($casetype=="民事"&&$level=="一审") {
			array_push($L1mssecondtagArr, $secondtag);
		}else if ($casetype=="民事"&&$level=="二审") {
			array_push($L2mssecondtagArr, $secondtag);
		}else if ($casetype=="刑事"&&$level=="一审") {
			array_push($L1xssecondtagArr, $secondtag);
		}else if ($casetype=="刑事"&&$level=="二审") {
			array_push($L2xssecondtagArr, $secondtag);
		}
	}
	$levelCountValues=array_count_values($levelArr);
	$L1msCountValues=array_count_values($L1mssecondtagArr);
	$L2msCountValues=array_count_values($L2mssecondtagArr);
	$L1xsCountValues=array_count_values($L1xssecondtagArr);
	$L2xsCountValues=array_count_values($L2xssecondtagArr);
	//案件数量排列
	arsort($levelCountValues);
	arsort($L1msCountValues);
	arsort($L2msCountValues);
	arsort($L1xsCountValues);
	arsort($L2xsCountValues);

	$arr=array();
	$arr1=array();
	if (empty($levelCountValues)) {
		$arr1[0]['value']=0;
		$arr1[0]['name']="无此类案件";
	}else{
		foreach ($levelCountValues as $key => $value) {
			array_push($arr1, array('value'=>$value,'name'=>$key));
		}
	}
	$arr2=array();
	if (empty($L1msCountValues)) {
		$arr2[0]['value']=0;
		$arr2[0]['name']="无此类案件";
	}else{
		foreach ($L1msCountValues as $key => $value) {
			array_push($arr2, array('value'=>$value,'name'=>$key));
		}
	}
	$arr3=array();
	if (empty($L2msCountValues)) {
		$arr3[0]['value']=0;
		$arr3[0]['name']="无此类案件";
	}else{
		foreach ($L2msCountValues as $key => $value) {
			array_push($arr3, array('value'=>$value,'name'=>$key));
		}
	}
	$arr4=array();
	if (empty($L1xsCountValues)) {
		$arr4[0]['value']=0;
		$arr4[0]['name']="无此类案件";
	}else{
		foreach ($L1xsCountValues as $key => $value) {
			array_push($arr4, array('value'=>$value,'name'=>$key));
		}
	}
	$arr5=array();
	if (empty($L2xsCountValues)) {
		$arr5[0]['value']=0;
		$arr5[0]['name']="无此类案件";
	}else{
		foreach ($L2xsCountValues as $key => $value) {
			array_push($arr5, array('value'=>$value,'name'=>$key));
		}
	}
	$arr=array($arr1,$arr2,$arr3,$arr4,$arr5);
	return $arr;
}

//***************************************案件报告获取文书类型（10A）
function getDoctypeResult($rst){
	$doctypeArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		array_push($doctypeArr, $doctype);
	}
	// print_r($rankArr);

	$doctypeCountValues=array_count_values($doctypeArr);
	//案件数量排列
	arsort($doctypeCountValues);
	// print_r($doctypeCountValues);

	//旧版的操作方法
	// return $doctypeCountValues;

	//新版的操作方法
	$arr=array();
	if (empty($doctypeCountValues)) {
		$arr[0]['value']=0;
		$arr[0]['name']="无此类案件";
	}else{
		foreach ($doctypeCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}
	}

	return $arr;
}
//***************************************案件报告获取判决结果（10B）
function getPanjueResult($rst){

	$resultArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['juegeresult'];
		if ($doctype=='判决') {
			array_push($resultArr, $result);
		}
	}
	// print_r($rankArr);

	$resultCountValues=array_count_values($resultArr);
	//案件数量排列
	arsort($resultCountValues);
	// print_r($resultCountValues);

	//旧版的操作方法
	// return $resultCountValues;

	//新版的操作方法
	$arr=array();
	if (empty($resultCountValues)) {
		$arr[0]['value']=0;
		$arr[0]['name']="无此类案件";
	}else{
		foreach ($resultCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}
	}

	return $arr;
}
//***************************************案件报告获取裁定结果（10C）
function getCaidingResult($rst){
	$resultArr=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$doctype=$val['doctype'];
		$result=$val['juegeresult'];
		if ($doctype=='裁定') {
			array_push($resultArr, $result);
		}
	}
	// print_r($rankArr);

	$resultCountValues=array_count_values($resultArr);
	//案件数量排列
	arsort($resultCountValues);
	// print_r($resultCountValues);

	//旧版的操作方法
	// return $resultCountValues;

	//新版的操作方法
	$arr=array();
	if (empty($resultCountValues)) {
		$arr[0]['value']=0;
		$arr[0]['name']="无此类案件";
	}else{
		foreach ($resultCountValues as $key => $value) {
			array_push($arr, array('value'=>$value,'name'=>$key));
		}
	}

	return $arr;
}



function getPanjuesecondtag($rst){
$allgongsecondtagArr1 = array();
$allshousecondtagArr1 = array();
$gongsecondtagArr1 = array();
$shousecondtagArr1 = array();
$gongsecondtagArr2 = array();
$shousecondtagArr2 = array();
$gongsecondtagArr3 = array();
$shousecondtagArr3 = array();
$arr=array();
foreach ($rst as $val) {
	$fid=$val['_id'];
	$doctype=$val['doctype'];
	$secondtag=$val['secondtag'];
	$party=$val['party'];
	$level=$val['level'];
	$winner=$val['winner'];

	if ($winner=="攻方") {
		array_push($allgongsecondtagArr1, $secondtag);

	}else if ($winner=="守方") {
		array_push($allshousecondtagArr1, $secondtag);

	}

	if ($level=='一审'&&$winner=="攻方") {
		array_push($gongsecondtagArr1, $secondtag);

	}else if ($level=='一审'&&$winner=="守方") {
		array_push($shousecondtagArr1, $secondtag);

	}else if($level=="二审"&&$winner=="攻方"){
		array_push($gongsecondtagArr2, $secondtag);

	} else if ($level=="二审"&&$winner=="守方") {
		array_push($shousecondtagArr2, $secondtag);

	}else if($level=="再审"&&$winner=="攻方"){
		array_push($gongsecondtagArr3, $secondtag);

	}else if ($level=="再审"&&$winner=="守方") {
		array_push($shousecondtagArr3, $secondtag);

	}
}

$allgongsecondtagValues1=array_count_values($allgongsecondtagArr1);
$allshousecondtagValues1=array_count_values($allshousecondtagArr1);
$gongsecondtagValues1=array_count_values($gongsecondtagArr1);
$shousecondtagValues1=array_count_values($shousecondtagArr1);
$gongsecondtagValues2=array_count_values($gongsecondtagArr2);
$shousecondtagValues2=array_count_values($shousecondtagArr2);
$gongsecondtagValues3=array_count_values($gongsecondtagArr3);
$shousecondtagValues3=array_count_values($shousecondtagArr3);

arsort($allgongsecondtagValues1);
arsort($allshousecondtagValues1);
arsort($gongsecondtagValues1);
arsort($shousecondtagValues1);
arsort($gongsecondtagValues2);
arsort($shousecondtagValues2);
arsort($gongsecondtagValues3);
arsort($shousecondtagValues3);


	$arr1=array();
	if (empty($allgongsecondtagValues1)) {
		$arr1[0]['value']=0;
		$arr1[0]['name']="无此类案件";
	}else{
		foreach ($allgongsecondtagValues1 as $k1 => $v1) {
		array_push($arr1, array('value'=>$v1,'name'=>$k1));
		}
	}



	$arr2=array();
	if (empty($allshousecondtagValues1)) {
		$arr2[0]['value']=0;
		$arr2[0]['name']="无此类案件";
	}else{
		foreach ($allshousecondtagValues1 as $k2 => $v2) {
		array_push($arr2, array('value'=>$v2,'name'=>$k2));
		}
	}


	$arr3=array();
	if (empty($gongsecondtagValues1)) {
		$arr3[0]['value']=0;
		$arr3[0]['name']="无此类案件";
	}else{
		foreach ($gongsecondtagValues1 as $k3 => $v3) {
		array_push($arr3, array('value'=>$v3,'name'=>$k3));
		}
	}
	$arr4=array();
	if (empty($shousecondtagValues1)) {
		$arr4[0]['value']=0;
		$arr4[0]['name']="无此类案件";
	}else{
		foreach ($shousecondtagValues1 as $k4 => $v4) {
		array_push($arr4, array('value'=>$v4,'name'=>$k4));
		}
	}
	$arr5=array();
	if (empty($gongsecondtagValues2)) {
		$arr5[0]['value']=0;
		$arr5[0]['name']="无此类案件";
	}else{
		foreach ($gongsecondtagValues2 as $k5 => $v5) {
			array_push($arr5, array('value'=>$v5,'name'=>$k5));
		}
	}
	$arr6=array();
	if (empty($shousecondtagValues2)) {
		$arr6[0]['value']=0;
		$arr6[0]['name']="无此类案件";
	}else{
		foreach ($shousecondtagValues2 as $k6 => $v6) {
			array_push($arr6, array('value'=>$v6,'name'=>$k6));
		}
	}
	$arr7=array();
	if (empty($gongsecondtagValues3)) {
			$arr7[0]['value']=0;
			$arr7[0]['name']="无此类案件";
	}else{
		foreach ($gongsecondtagValues3 as $k7 => $v7) {
			array_push($arr7, array('value'=>$v7,'name'=>$k7));
		}
	}
	$arr8=array();
		if (empty($shousecondtagValues3)) {
			$arr8[0]['value']=0;
			$arr8[0]['name']="无此类案件";

		}else{
			foreach ($shousecondtagValues3 as $k8 => $v8) {
				array_push($arr8, array('value'=>$v8,'name'=>$k8));
			}
		}
	$arr=array($arr1,$arr2,$arr3,$arr4,$arr5,$arr6,$arr7,$arr8);
	return $arr;
}


function getmoney($rst){
	$allmoney=array();
	foreach ($rst as $val) {
	$fid=$val['_id'];
	$money=$val['money'];
		if ($money!=="裁定或者二审再审") {
			$money=$money/10000;
			array_push($allmoney, $money);
		}
	}
	$nameType=array(
		array('name'=>'0——10万元','value'=>0),
		array('name'=>'10——50万元','value'=>0),
		array('name'=>'50——100万元','value'=>0),
		array('name'=>'100——500万元','value'=>0),
		array('name'=>'500万元以上','value'=>0)
		);
		foreach ($allmoney as $key => $value) {
			if ($value<10) {
				$nameType[0]['value']++;
			}else if($value>10&$value<=50){
				$nameType[1]['value']++;
			}else if ($value>50&$value<=100) {
				$nameType[2]['value']++;
			}else if ($value>100&$value<=500) {
				$nameType[3]['value']++;
			}else if ($value>500) {
				$nameType[4]['value']++;
			}
		}
		// print_r($nameType);
		return $nameType;
	}

//============================================法律的引用15A
function getlaw($rst){
	$lawnameArr1=array();
	foreach ($rst as $val) {
	$fid=$val['_id'];
	$law=$val['law'];
	$lawnew=array();
	$lawnum=array();
		foreach ($law as $key1 => $value1) {
			$num=count($value1);
			$lawnum[$key1]=$num;
		}
		foreach ($lawnum as $key2 => $value2) {
			if (array_key_exists($key2, $lawnameArr1)) {
				$lawnameArr1[$key2]+=$value2;
			}else{
				$lawnameArr1[$key2]=$value2;
			}
		}
	}
	$lawArr1=array();
	$lawArr2=array();
	$lawArr3=array();
	$lawArr1_1=array();
	$lawArr2_1=array();
	$lawArr3_1=array();
	$lawnameArr111=array();
	foreach ($lawnameArr1 as $key => $value) {
		if (mb_strpos($key, "民事诉讼")==false) {
			if (mb_strrpos($key, "法")==mb_strlen($key)-1||mb_strrpos($key, "民法通则")==mb_strlen($key)-4) {
					$lawArr1[$key]=$value;
			}else if(mb_strpos($key, "最高人民法院")!==false||mb_strpos($key, "最高人民检察院")!==false){
					$lawArr2[$key]=$value;
			}else{
					$lawArr3[$key]=$value;
			}
		}
	}
	if (empty($lawArr1)) {
		$lawArr1_1[0]['value']=0;
		$lawArr1_1[0]['name']="未适用法律";
	}else{
		foreach ($lawArr1 as $k1 => $v1) {
		array_push($lawArr1_1, array('value'=>$v1,'name'=>$k1));
		}
	}
	if(empty($lawArr2)){
		$lawArr2_1[0]['value']=0;
		$lawArr2_1[0]['name']="未适用司法解释";
	}else{
		foreach ($lawArr2 as $k2 => $v2) {
		array_push($lawArr2_1, array('value'=>$v2,'name'=>$k2));
		}
	}
	if(empty($lawArr3)){
		$lawArr3_1[0]['value']=0;
		$lawArr3_1[0]['name']="未适用行政法规";
	}else{
		foreach ($lawArr3 as $k3 => $v3) {
		array_push($lawArr3_1, array('value'=>$v3,'name'=>$k3));
		}
	}
	

	arsort($lawArr1);
	arsort($lawArr2);
	arsort($lawArr3);
	$lawmax1=array_keys($lawArr1);
	$lawmax2=array_keys($lawArr2);
	$lawmax3=array_keys($lawArr3);
	$lawT0=array();
	$lawT0_1=array();
	$lawT1=array();
	$lawT1_1=array();
	$lawT2=array();
	$lawT2_1=array();
	$lawT3=array();
	$lawT3_1=array();
	$lawT4=array();
	$lawT4_1=array();
	$lawT5=array();
	$lawT5_1=array();
	foreach ($rst as $val) {
		$fid=$val['_id'];
		$law1=$val['law'];
		foreach ($law1 as $key3 => $value3) {
			if ($lawmax1[0]==$key3) {
				$lawT0=array_merge($lawT0, $value3);
			}else if ($lawmax1[1]==$key3) {
				$lawT1=array_merge($lawT1, $value3);
			}else if ($lawmax2[0]==$key3) {
				$lawT2=array_merge($lawT2, $value3);
			}else if ($lawmax2[1]==$key3) {
				$lawT3=array_merge($lawT3, $value3);
			}else if ($lawmax3[0]==$key3) {
				$lawT4=array_merge($lawT4, $value3);
			}else if ($lawmax3[1]==$key3) {
				$lawT5=array_merge($lawT5, $value3);
			}
		}
	}
	$lawT0=array_count_values($lawT0);
	$lawT1=array_count_values($lawT1);
	$lawT2=array_count_values($lawT2);
	$lawT3=array_count_values($lawT3);
	$lawT4=array_count_values($lawT4);
	$lawT5=array_count_values($lawT5);
	arsort($lawT0);
	arsort($lawT1);
	arsort($lawT2);
	arsort($lawT3);
	arsort($lawT4);
	arsort($lawT5);
	if (empty($lawT0)) {
		$lawT0_1[0]['value']=0;
		$lawT0_1[0]['name']="无此类案件";
	}else{
		foreach ($lawT0 as $k0 => $v0) {
		array_push($lawT0_1, array('value'=>$v0,'name'=>$k0));
		}
	}
	if(empty($lawT1)){
		$lawT1_1[0]['value']=0;
		$lawT1_1[0]['name']="无此类hhah案件";
	}else{
		foreach ($lawT1 as $k1 => $v1) {
		array_push($lawT1_1, array('value'=>$v1,'name'=>$k1));
		}
	}
	if(empty($lawT2)){
		$lawT2_1[0]['value']=0;
		$lawT2_1[0]['name']="无此类案件";
	}else{
		foreach ($lawT2 as $k2 => $v2) {
		array_push($lawT2_1, array('value'=>$v2,'name'=>$k2));
		}
	}
	if(empty($lawT3)){
		$lawT3_1[0]['value']=0;
		$lawT3_1[0]['name']="无此类案件";
	}else{
		foreach ($lawT3 as $k3 => $v3) {
		array_push($lawT3_1, array('value'=>$v3,'name'=>$k3));
		}
	}
	if(empty($lawT4)){
		$lawT4_1[0]['value']=0;
		$lawT4_1[0]['name']="无此类案件";
	}else{
		foreach ($lawT4 as $k4 => $v4) {
		array_push($lawT4_1, array('value'=>$v4,'name'=>$k4));
		}
	}
	if(empty($lawT5)){
		$lawT5_1[0]['value']=0;
		$lawT5_1[0]['name']="无此类案件";
	}else{
		foreach ($lawT5 as $k5 => $v5) {
		array_push($lawT5_1, array('value'=>$v5,'name'=>$k5));
		}
	}
	$Arr=array($lawArr1_1,$lawArr2_1,$lawArr3_1,$lawT0_1,$lawT1_1,$lawT2_1,$lawT3_1,$lawT4_1,$lawT5_1,$lawmax1[0],$lawmax1[1],$lawmax2[0],$lawmax2[1],$lawmax3[0],$lawmax3[1]);
	return $Arr;

}







































	//===============================================判断对方当事人类型【机构、个人】
	function isPerson($nameUnique){
		// file_put_contents('c:/test.log', var_export($nameUnique,true));
		$nameType=array(
			array('name'=>'机构','value'=>0),
			array('name'=>'个人','value'=>0)
			);

		foreach ($nameUnique as $key => $value) {

			// $pos4=strlen($value);
			if (strlen($value)>9) {
				$nameType[0]['value']++;
			}else{
				$nameType[1]['value']++;
			}
		}
		// print_r($nameType);
		return $nameType;
	}

	//===============================================判断对方当事人类型【企业、其他】
	function isCompany($nameUnique){

		$nameType=array(
			array('name'=>'企业','value'=>0),
			array('name'=>'其他机构','value'=>0)
			);

		foreach ($nameUnique as $key => $value) {
			if (strlen($value)>9) {
			    $pos1=strpos($value,"公司");
				$pos2=strpos($value,"企业");
				$pos3=strpos($value,"厂");
			    $pos4=strpos($value,"店");
			    $pos5=strpos($value,"超市");
				if($pos1!==false||$pos2!==false||$pos3!==false||$pos4!==false||$pos5!==false){
					$nameType[0]['value']++;
				}else {
					$nameType[1]['value']++;
				}
			}
		}
		// print_r($nameType);
		return $nameType;
	}























}
